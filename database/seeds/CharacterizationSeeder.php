<?php

use App\Models\Characterization;
use Illuminate\Database\Seeder;

class CharacterizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Characterization::create([
            'name' => 'Técnicos',
            
        ]);

        Characterization::create([
            'name' => 'Afrodescendientes',
        ]);

        Characterization::create([
            'name' => 'Desplazados',
        ]);
        Characterization::create([
            'name' => 'Producción de centro',
        ]);
    }
}
