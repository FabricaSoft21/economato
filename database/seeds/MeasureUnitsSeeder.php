<?php

use Illuminate\Database\Seeder;
use App\Models\MeasureUnits;

class MeasureUnitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MeasureUnits::create([
            'name'=>'Libra'
        ]);
        MeasureUnits::create([
            'name'=>'Kilogramo'
        ]);
        MeasureUnits::create([
            'name'=>'Litro'
        ]);
        MeasureUnits::create([
            'name'=>'Unidad'
        ]);
        MeasureUnits::create([
            'name'=>'Gramo'
        ]);
        MeasureUnits::create([
            'name'=>'Militro'
        ]);
    }
}
