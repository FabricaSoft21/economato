<?php

use App\Models\Tax;
use Illuminate\Database\Seeder;

class TaxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tax::create([
            'tax' => 19
        ]);
        Tax::create([
            'tax' => 5
        ]);
        Tax::create([
            'tax' => 0
        ]);
    }
}
