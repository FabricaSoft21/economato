<?php

use App\Models\Complex;
use App\Models\Region;
use Illuminate\Database\Seeder;

class ComplexSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Complex::create([
            'name'=>'Central',
            'region_id'=>1
        ]);

        Complex::create([
            'name'=>'Oriental',
            'region_id'=>2
        ]);
       
    }
}
