<?php

use App\Models\Center;
use Illuminate\Database\Seeder;


class FormationCenterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Center::create([
            'name' => 'Centro de servicios y gestión empresarial',
            'complex_id' => 1,            
            'address' => 'Cra. 57 #51-83, Medellín, Antioquia',
        ]);
        
        Center::create([
            'name' => 'Centro de gestión administrativa',
            'complex_id' => 2,            
            'address' => 'Av. Caracas #No. 13-80, Bogotá, Cundinamarca',
        ]);

        Center::create([
            'name' => 'Centro de formación en actividad física.',
            'complex_id' => 2,            
            'address' => 'Av. Caracas #No. 13-80, Bogotá, Cundinamarca',
        ]);
        
    }
}
