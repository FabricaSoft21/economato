<?php

use App\Models\StatusOrder;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatusOrder::create([
            'name'=>'Por aprobar'
        ]);
        StatusOrder::create([
            'name'=>'Aprobado'
        ]);
        StatusOrder::create([
            'name'=>'Denegado'
        ]);
        StatusOrder::create([
            'name'=>'Entregado'
        ]);
    }
}
