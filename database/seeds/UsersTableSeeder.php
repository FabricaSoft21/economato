<?php

use Illuminate\Database\Seeder;
use App\User;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //usuario con el rol instructor
        $instructor = User::create([
            'name' => 'Instructor',
            'lastname' => 'Carvajal',
            'email' => 'instructor@gmail.com',
            'password' => bcrypt('123456'),
            'foto' => 'default.png',
            'identification' => '123456789',
            'phone' => '300588558',
        ]);

        $instructor->assignRole('Instructores');

        //usuario con el rol super-admin
        $super = User::create([
            'name' => 'Super',
            'lastname' => 'Arenas',
            'email' => 'super@gmail.com',
            'password' => bcrypt('123456'),
            'foto' => 'default.png',
            'identification' => '123456789',
            'phone' => '300588558',

        ]);

        $super->assignRole('Super-Admin');

        $coordinacion = User::create([
            'name' => 'Coordinación',
            'lastname' => 'gomez',
            'email' => 'coord@gmail.com',
            'password' => bcrypt('123456'),
            'foto' => 'default.png',
            'identification' => '123456789',
            'phone' => '300588558',

        ]);

        $coordinacion->assignRole('Coordinación');


        $bodega = User::create([
            'name' => 'bodega',
            'lastname' => 'Arenas',
            'email' => 'bodega@gmail.com',
            'password' => bcrypt('123456'),
            'foto' => 'default.png',
            'identification' => '123456789',
            'phone' => '300588558',
        ]);
        $bodega->assignRole('Bodega');


        $Presupuestal = User::create([
            'name' => 'Presupuestal',
            'lastname' => 'Arenas',
            'email' => 'Presupuestal@gmail.com',
            'password' => bcrypt('123456'),
            'foto' => 'default.png',
            'identification' => '123456789',
            'phone' => '300588558',
        ]);

        $Presupuestal->assignRole('Presupuestal');

        $LiderProduccion = User::create([
            'name' => 'LiderProduccion',
            'lastname' => 'Arenas',
            'email' => 'LiderProduccion@gmail.com',
            'password' => bcrypt('123456'),
            'foto' => 'default.png',
            'identification' => '123456789',
            'phone' => '300588558',
        ]);

        $LiderProduccion->assignRole('Lider-Produccion-centro');

        $Administrador = User::create([
            'name' => 'Admnistrador',
            'lastname' => 'Arenas',
            'email' => 'Admnistrador@gmail.com',
            'password' => bcrypt('123456'),
            'foto' => 'default.png',
            'identification' => '123456789',
            'phone' => '300588558',
        ]);

        $Administrador->assignRole('Administrador');
    }
}
