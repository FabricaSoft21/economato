<?php

use App\Models\Provider;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(RolesAndPermissions::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TypeProductsSeeder::class);
        $this->call(PresentationSeeder::class);
        $this->call(MeasureUnitsSeeder::class);
        $this->call(CharacterizationSeeder::class);
        $this->call(StatusSeeder::class);

        $this->call(RegionSeeder::class);
        $this->call(ComplexSeeder::class);
        $this->call(FormationProgramSeeder::class);
        $this->call(FormationCenterSeeder::class);



        $this->call(TaxSeeder::class);

        $this->call(ProviderSeeder::class);
        $this->call(ProductSeeder::class);
    }
}
