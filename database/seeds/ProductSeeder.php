<?php

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'Leche Deslactosada',
            'type_product_id' =>1,
            'presentation_id'=>1,
            'measure_unit_id'=>3,
            'tax_id'=>2
        ]);
        Product::create([
            'name' => 'Queso',
            'type_product_id' =>1,
            'presentation_id'=>1,
            'measure_unit_id'=>4,
            'tax_id'=>3
        ]);
        Product::create([
            'name' => 'Yogurt Artesanal',
            'type_product_id' =>1,
            'presentation_id'=>1,
            'measure_unit_id'=>3,
            'tax_id'=>2
        ]);
        Product::create([
            'name' => 'Carne de cerdo',
            'type_product_id' =>2,
            'presentation_id'=>1,
            'measure_unit_id'=>1,
            'tax_id'=>1
        ]);
        Product::create([
            'name' => 'Carne de res',
            'type_product_id' =>2,
            'presentation_id'=>1,
            'measure_unit_id'=>1,
            'tax_id'=>1
        ]);
        Product::create([
            'name' => 'Pollo',
            'type_product_id' =>2,
            'presentation_id'=>1,
            'measure_unit_id'=>1,
            'tax_id'=>2
        ]);
        Product::create([
            'name' => 'Whisky Irlandés',
            'type_product_id' =>4,
            'presentation_id'=>3,
            'measure_unit_id'=>3,
            'tax_id'=>1
        ]);
        Product::create([
            'name' => 'Chimeneaud',
            'type_product_id' =>4,
            'presentation_id'=>3,
            'measure_unit_id'=>3,
            'tax_id'=>1
        ]);
        Product::create([
            'name' => 'Ron Viejo de Caldas',
            'type_product_id' =>4,
            'presentation_id'=>3,
            'measure_unit_id'=>3,
            'tax_id'=>1
        ]);
        Product::create([
            'name' => 'Aguardiente Antioqueño',
            'type_product_id' =>4,
            'presentation_id'=>3,
            'measure_unit_id'=>3,
            'tax_id'=>1
        ]);
        Product::create([
            'name' => 'Avichuela',
            'type_product_id' =>5,
            'presentation_id'=>1,
            'measure_unit_id'=>2,
            'tax_id'=>3
        ]);
        Product::create([
            'name' => 'Frijóles',
            'type_product_id' =>5,
            'presentation_id'=>1,
            'measure_unit_id'=>2,
            'tax_id'=>2
        ]);
        Product::create([
            'name' => 'Lentejas',
            'type_product_id' =>2,
            'presentation_id'=>1,
            'measure_unit_id'=>1,
            'tax_id'=>3
        ]);
        Product::create([
            'name' => 'Garbanso',
            'type_product_id' =>2,
            'presentation_id'=>1,
            'measure_unit_id'=>1,
            'tax_id'=>3
        ]);
        Product::create([
            'name' => 'Arroz',
            'type_product_id' =>2,
            'presentation_id'=>1,
            'measure_unit_id'=>1,
            'tax_id'=>2
        ]);
        Product::create([
            'name' => 'Papa',
            'type_product_id' =>5,
            'presentation_id'=>1,
            'measure_unit_id'=>2,
            'tax_id'=>1
        ]);
        Product::create([
            'name' => 'Tomate chonto',
            'type_product_id' =>5,
            'presentation_id'=>1,
            'measure_unit_id'=>2,
            'tax_id'=>1
        ]);
        Product::create([
            'name' => 'Pimentón',
            'type_product_id' =>5,
            'presentation_id'=>1,
            'measure_unit_id'=>2,
            'tax_id'=>2
        ]);
    }
}
