<?php

use App\Models\Region;
use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Region::create([
            'name' => 'Antioquia'
        ]);

        Region::create([
            'name' => 'Boyacá'
        ]);

        Region::create([
            'name' => 'Caldas'
        ]);

        Region::create([
            'name' => 'Cundinamarca'
        ]);

        Region::create([
            'name' => 'Zona Andina'
        ]);

        Region::create([
            'name' => 'Quindío'
        ]);

        Region::create([
            'name' => 'Risaralda'
        ]);

        Region::create([
            'name' => 'Santander'
        ]);

        Region::create([
            'name' => 'Tolima'
        ]);

        Region::create([
            'name' => 'Bolívar'
        ]);

        Region::create([
            'name' => 'César'
        ]);

        Region::create([
            'name' => 'Córdoba'
        ]);
    }
}
