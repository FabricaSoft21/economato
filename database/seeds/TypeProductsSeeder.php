<?php

use Illuminate\Database\Seeder;
use App\Models\TypeProduct;

class TypeProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TypeProduct::create([
            'name'=>'Lacteos'
        ]);
        TypeProduct::create([
            'name'=>'Carnes'
        ]);
        TypeProduct::create([
            'name'=>'Granos'
        ]);
        TypeProduct::create([
            'name'=>'Licores'
        ]);
        TypeProduct::create([
            'name'=>'Abarrotes'
        ]);
    }
}
