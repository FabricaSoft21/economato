<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissions extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // Reset cached roles and permissions
    app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

    // permissions products 
    Permission::create(['name' => 'all products']);
    Permission::create(['name' => 'delete products']);
    Permission::create(['name' => 'create products']);
    Permission::create(['name' => 'update products']);

    //permissions providers
    Permission::create(['name' => 'all providers']);
    Permission::create(['name' => 'delete providers']);
    Permission::create(['name' => 'create providers']);
    Permission::create(['name' => 'update providers']);

    //permissions contract
    Permission::create(['name' => 'all contracts']);
    Permission::create(['name' => 'create contracts']);

    //permissions regionales
    Permission::create(['name' => 'all regionales']);
    Permission::create(['name' => 'create regionales']);
    Permission::create(['name' => 'update regionales']);
    Permission::create(['name' => 'delete regionales']);

    //permissions complejos
    Permission::create(['name' => 'all complejos']);
    Permission::create(['name' => 'create complejos']);
    Permission::create(['name' => 'update complejos']);
    Permission::create(['name' => 'delete complejos']);

    //permissions centros
    Permission::create(['name' => 'all centros']);
    Permission::create(['name' => 'create centros']);
    Permission::create(['name' => 'update centros']);
    Permission::create(['name' => 'delete centros']);

    //permissions programas formación
    Permission::create(['name' => 'all formacion']);
    Permission::create(['name' => 'create formacion']);
    Permission::create(['name' => 'update formacion']);
    Permission::create(['name' => 'delete formacion']);

    //permissions instructores
    Permission::create(['name' => 'all instructores']);
    Permission::create(['name' => 'create instructores']);
    Permission::create(['name' => 'update instructores']);
    Permission::create(['name' => 'delete instructores']);

    //permissions fichas
    Permission::create(['name' => 'all fichas']);
    Permission::create(['name' => 'create fichas']);
    Permission::create(['name' => 'update fichas']);
    Permission::create(['name' => 'delete fichas']);

    //permissions recipes
    Permission::create(['name' => 'all recipes']);
    Permission::create(['name' => 'create recipes']);
    Permission::create(['name' => 'update recipes']);
    Permission::create(['name' => 'delete recipes']);

    //permissions budget
    Permission::create(['name' => 'all budget']);
    Permission::create(['name' => 'create budget']);
    Permission::create(['name' => 'update budget']);
    Permission::create(['name' => 'delete budget']);
    Permission::create(['name' => 'details budget']);

    //permissions  orders
    Permission::create(['name' => 'all orders']);
    Permission::create(['name' => 'create orders']);
    Permission::create(['name' => 'update orders']);
    Permission::create(['name' => 'delete orders']);



    //permissions general
    Permission::create(['name' => 'view dashboard']);
    // create roles and assign created permissions

    $role = Role::create(['name' => 'Super-Admin']);
    $role->givePermissionTo(Permission::all());

    $role = Role::create(['name' => 'Instructores']);
    $role->givePermissionTo([
      'create orders',
      'all recipes', 'create recipes', 'update recipes', 'delete recipes',
      'all products'
    ]);


    $role = Role::create(['name' => 'Administrador']);
    $role->givePermissionTo([
      'all products', 'delete products', 'create products', 'update products',
      'all regionales', 'create regionales', 'update regionales', 'delete regionales',
      'all complejos', 'create complejos', 'update complejos', 'delete complejos',
      'all centros', 'create centros', 'update centros', 'delete centros',
      'all formacion', 'create formacion', 'update formacion', 'delete formacion',
      'all complejos', 'create complejos', 'update complejos', 'delete complejos',
      'all instructores', 'create instructores', 'update instructores', 'delete instructores',
      'all fichas', 'create fichas', 'update fichas', 'delete fichas',
      'all providers', 'create providers', 'update providers', 'delete providers'
    ]);


    $role = Role::create(['name' => 'Bodega']);
    $role->givePermissionTo([
      'view dashboard',
      'all contracts', 'create contracts',
      'all products', 'delete products', 'create products', 'update products',
      'all providers', 'create providers', 'update providers', 'delete providers',
      'details budget'
    ]);


    $role = Role::create(['name' => 'Coordinación']);
    $role->givePermissionTo([
      'view dashboard', 'all products'
    ]);

    $role = Role::create(['name' => 'Lider-Produccion-centro']);
    $role->givePermissionTo([
      'view dashboard', 'all products'
    ]);

    $role = Role::create(['name' => 'Presupuestal']);
    $role->givePermissionTo([
      'view dashboard',
      'all budget', 'create budget', 'update budget', 'delete budget'
    ]);
  }
}
