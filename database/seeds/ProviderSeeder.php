<?php

use App\Models\Provider;
use Illuminate\Database\Seeder;

class ProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Provider::create([
            'NIT'=>'258741-6',
            'name'=>'Fruver Full',
            'phone'=>'4265879',
            'email'=>'fruverful@gmail.com',
            'contact_name'=>'Andrés',
            'last_name'=>'Gutiérrez',
            'phone_contact'=>'312784955',
        ]);
    }
}
