<?php

use App\Models\Program;
use Illuminate\Database\Seeder;

class FormationProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Program::create([
            'name' => 'Técnico Profesional en cocina',
            'version' => '2019-01',
            'description' => 'Técnico Profesional en cocina, de tyodas la sedes de formación.',
            'status' => '0',
        ]);

        Program::create([
            'name' => 'Técnico Profesional en Gastronomía',
            'version' => '2019-01',
            'description' => 'Técnico Profesional en Gastronomía, de tyodas la sedes de formación.',
            'status' => '0',
        ]);
    }
}
