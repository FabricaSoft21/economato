<?php

use Illuminate\Database\Seeder;
use App\Models\PresentationProduct;

class PresentationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PresentationProduct::create([
            'name'=>'Bolsa'
        ]);
        PresentationProduct::create([
            'name'=>'Caja'
        ]);
        PresentationProduct::create([
            'name'=>'Botella'
        ]);
    }
}
