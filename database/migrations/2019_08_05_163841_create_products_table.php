<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('status')->default(0);
            $table->bigInteger('tax_id')->unsigned();
            $table->bigInteger('type_product_id')->unsigned();
            $table->bigInteger('presentation_id')->unsigned();
            $table->bigInteger('measure_unit_id')->unsigned();

            $table->foreign('type_product_id')->references('id')->on('tbl_type_products');
            $table->foreign('presentation_id')->references('id')->on('tbl_presentation_products');
            $table->foreign('measure_unit_id')->references('id')->on('tbl_measure_units');
            $table->foreign('tax_id')->references('id')->on('tbl_taxes');




            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_products');
    }
}
