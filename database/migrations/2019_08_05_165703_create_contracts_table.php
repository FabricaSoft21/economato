<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_contracts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('number');
            $table->date('start_date');
            $table->date('finis_date');
            $table->string('contracts_url');
            $table->boolean('status')->default(0);
            $table->bigInteger('provider_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();

            
            $table->foreign('provider_id')->references('id')->on('tbl_providers');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_contracts');
    }
}
