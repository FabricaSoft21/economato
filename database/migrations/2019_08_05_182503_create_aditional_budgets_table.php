<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAditionalBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_aditional_budgets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('budget_id')->unsigned();
            $table->integer('aditional_budget');
            $table->string('aditional_budget_code');
            $table->date('aditional_begin_date');
            $table->date('aditional_finish_date');
            $table->integer('status')->default(0);
            
            $table->foreign('budget_id')->references('id')->on('tbl_budgets');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_aditional_budgets');
    }
}
