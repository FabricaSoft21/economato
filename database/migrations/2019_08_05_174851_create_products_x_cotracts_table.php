<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsXCotractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_productsXcontracts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pruduct_id')->unsigned();
            $table->bigInteger('contract_id')->unsigned();
            $table->integer('unit_price');
            $table->integer('total_with_tax');
            $table->integer('tax_value');
            $table->integer('status')->default(0);
            $table->timestamps();

            $table->foreign('pruduct_id')->references('id')->on('tbl_products');
            $table->foreign('contract_id')->references('id')->on('tbl_contracts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_productsXcontracts');
    }
}
