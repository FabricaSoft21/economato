<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('number');
            $table->integer('apprentices');
            $table->date('start_date');
            $table->date('finish_date');
            $table->integer('status')->default(0);
            $table->bigInteger('program_id')->unsigned();
            $table->bigInteger('characterization_id')->unsigned();
            $table->bigInteger('center_id')->unsigned();

            $table->foreign('program_id')->references('id')->on('tbl_programs');
            $table->foreign('characterization_id')->references('id')->on('tbl_characterizations');
            $table->foreign('center_id')->references('id')->on('tbl_centers');
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_files');
    }
}
