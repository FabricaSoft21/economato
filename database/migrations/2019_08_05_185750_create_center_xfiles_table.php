<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCenterXfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_centerXfiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('center_production_order_id')->unsigned();
            $table->bigInteger('file_id')->unsigned();
            $table->timestamps();
            $table->foreign('center_production_order_id')->references('id')->on('tbl_center_production_orders');
            $table->foreign('file_id')->references('id')->on('tbl_files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_centerXfiles');
    }
}
