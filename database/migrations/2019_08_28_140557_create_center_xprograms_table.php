<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCenterXprogramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_centerXprogram', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('center_id')->unsigned();
            $table->bigInteger('program_id')->unsigned();
            $table->timestamps();

            $table->foreign('center_id')->references('id')->on('tbl_centers');
            $table->foreign('program_id')->references('id')->on('tbl_programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_centerXprogram');
    }
}
