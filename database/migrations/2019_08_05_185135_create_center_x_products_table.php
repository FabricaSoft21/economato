<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCenterXProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_centerXproducts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('center_production_order_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->float('quantity',8,2);
            $table->timestamps();
            $table->foreign('center_production_order_id')->references('id')->on('tbl_center_production_orders');
            $table->foreign('product_id')->references('id')->on('tbl_productsXcontracts');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_centerXproducts');
    }
}
