<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_budgets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('initial_budget');
            $table->integer('remaining');
            $table->string('budget_code');
            $table->date('budget_begin');
            $table->date('budget_finish');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_budgets');
    }
}
