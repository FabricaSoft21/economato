<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipesXproductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_recipes_xproducts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('recipe_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->float('quantity', 8, 2);
            
            $table->foreign('recipe_id')->references('id')->on('tbl_recipes');
            $table->foreign('product_id')->references('id')->on('tbl_products');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_recipes_xproducts');
    }
}
