<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetXCharacterizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_budgetXcharacterizations', function (Blueprint $table) {
            $table->bigInteger('budget_id')->unsigned();
            $table->bigInteger('characterization_id')->unsigned();
            $table->double('porcentage');
            $table->double('value')->nullable();
            $table->timestamps();
            $table->foreign('budget_id')->references('id')->on('tbl_budgets');
            $table->foreign('characterization_id')->references('id')->on('tbl_characterizations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_budgetXcharacterizations');
    }
}
