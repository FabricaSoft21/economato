<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCenterProductionOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_center_production_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion',250);
            $table->integer('quantity_people');
            $table->date('order_date');
            $table->string('title',100);
            $table->integer('cost');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('center_id')->unsigned();
            $table->bigInteger('status_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('center_id')->references('id')->on('tbl_centers');
            $table->foreign('status_id')->references('id')->on('tbl_status_orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_center_production_orders');
    }
}
