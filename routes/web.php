<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(['register' => false]);
Route::middleware(['prevent-back-history', 'auth'])->group(function () {

    Route::group(['middleware' => ['permission:view dashboard']], function () {
        //solo los que tengan el permiso 
        Route::get('/home', 'HomeController@index')->name('home');
    });

    //fichas y Usuarios
    Route::group(['middleware' => ['role:Super-Admin|Administrador']], function () {

        Route::get('/file', 'FileController@index')->name('file.index');
        Route::post('/file', 'FileController@store');
        Route::get('/file/data', 'FileController@data');
        Route::get('/file/{id}', 'FileController@show');
        Route::get('/file/get_pro/{id}', 'FileController@get_program');
        Route::get('/infoFile/{id}', 'FileController@infoFile');

        Route::get('/users/manage', 'UserController@manage')->name('users.manage');
        Route::post('/user/save', 'UserController@saveUser')->name('user.save');
        Route::get('/user/data', 'UserController@data');
        Route::post('/user/delete', 'UserController@destroy');
    });
    //fichas 

    //productos 
    Route::group(['middleware' => ['role_or_permission:Super-Admin|Bodega|Administrador|all products']], function () {
        Route::get('/product/data', 'ProductController@data');
        Route::get('/product/show', 'ProductController@show');
        Route::post('/product/update', 'ProductController@update');
        Route::get('/product/delete', 'ProductController@destroy');
        Route::get('/search/product/{value}', 'ProductController@search');
        Route::get('/product', 'ProductController@index');
        //Productos
        Route::resource('/product', 'ProductController');
        //Productos
    });
    //productos 

    //presupuesto
    Route::group(['middleware' => ['role_or_permission:Super-Admin|Presupuestal|details budget']], function () {
        //Budget
        Route::get('budget/data', 'BudgetController@data');
        Route::post('/budget/update', 'BudgetController@update');
        Route::post('/budget', 'BudgetController@store');
        Route::get('/budget/delete', 'BudgetController@destroy');
        Route::get('/budget', 'BudgetController@index')->name('budgets');
        Route::get('/budget/{id}', 'BudgetController@show');
        Route::get('/charact', 'BudgetController@charact');
        Route::get('/charts/{id}', 'BudgetController@charts');
        Route::get('/budget_detail/{id}', 'BudgetController@detail_bud')->name('budget');

        //Aditional Budget
        Route::get('adibudget/data', 'AditionalBudgetController@data');
        Route::post('/adibudget/update', 'AditionalBudgetController@update');
        Route::post('/adibudget', 'AditionalBudgetController@store');
        Route::get('/adibudget/delete', 'AditionalBudgetController@destroy');
        Route::get('/adibudget', 'AditionalBudgetController@index')->name('adibudgets');
        Route::get('/adibudget/{id}', 'AditionalBudgetController@show');
    });
    //presupuesto 

    Route::group(['middleware' => ['role:Super-Admin|Administrador']], function () {

        //Region
        Route::get('/region', 'RegionController@index')->name('region');
        Route::get('region/data', 'RegionController@data');
        Route::post('/region', 'RegionController@store');
        Route::get('/region/{id}', 'RegionController@show');
        Route::post('/region/update', 'RegionController@update');
        Route::get('/region/delete', 'RegionController@destroy');
        //Region

        //Instructor
        Route::get('teacher/data', 'TeacherController@data');
        Route::post('/teacher/update', 'TeacherController@update');
        Route::post('/teacher', 'TeacherController@store');
        Route::get('/teacher/delete', 'TeacherController@destroy');
        Route::get('/teacher', 'TeacherController@index')->name('teacher');
        Route::get('/teacher/{id}', 'TeacherController@show');
        //Instructor

        //characterizations
        Route::get('/characterization', 'CharacterizationController@index')->name('characterization');
        Route::get('characterization/data', 'CharacterizationController@data');
        Route::post('/characterization', 'CharacterizationController@store');
        Route::post('/characterization/update', 'CharacterizationController@update');
        Route::get('/characterization/delete', 'CharacterizationController@destroy');
        Route::get('/characterization/{id}', 'CharacterizationController@show');
        //characterizations

        //Program
        Route::get('/program/data', 'ProgramController@data');
        Route::get('/search/program/{value}', 'ProgramController@search');
        Route::resource('/program', 'ProgramController')->except(['create', 'edit']);
        //Progra,

        //Complexes
        Route::get('/complex', 'ComplexController@index')->name('complex');
        Route::get('complex/data', 'ComplexController@data');
        Route::post('/complex', 'ComplexController@store');
        Route::get('/complex/{id}', 'ComplexController@show');
        Route::post('/complex/update', 'ComplexController@update');
        Route::get('/complex/delete', 'ComplexController@destroy');
        //Complexes
        //centros 
        Route::get('/center', 'CenterController@index')->name('center.index');
        Route::post('/center', 'CenterController@store');
        Route::get('/center/data', 'CenterController@data');
        Route::delete('/center/{id}', 'CenterController@destroy');
        Route::get('/center/{id}', 'CenterController@show');
        Route::patch('/center/{id}', 'CenterController@update');
        //centros

    });

    Route::group(['middleware' => ['role:Super-Admin|Administrador|Bodega']], function () {
        //Proveedores
        Route::get('/provider/data', 'ProviderController@data');
        Route::resource('/provider', 'ProviderController');
        //Proveedores
    });





    Route::group(['middleware' => ['role:Super-Admin|Bodega']], function () {

        //contract 
        Route::get('/contracts', 'ContractController@index')->name('contracts');
        Route::get('/contracts/all', 'ContractController@getContracts');
        Route::get('/contracts/providers/actives', 'ProviderController@getProvidersActives');
        Route::get('/contracts/create', 'ContractController@create')->name('contracts.create');
        Route::post('/contracts/create', 'ContractController@store');
        Route::get('/contracts/products/actives', 'ProductController@GetProductsActives');
        Route::get('/contracts/vigente/Validate', 'ContractController@changeStatus');
        Route::get('/contracts/products/view/{id}', 'ContractController@showProducts');
        //contract end

    });

    Route::group(['middleware' => ['role:Super-Admin|Instructores|Coordinación|Bodega']], function () {
        //Recetas
        Route::get('/recipe', 'RecipeController@index')->name('recipes.index');
        Route::get('/recipe/search/{search?}', 'RecipeController@dataRecipes')->name('recipes.recipes');
        Route::post('/recipe/save-recipe', 'RecipeController@store')->name('recipes.save');
        Route::get('/recipe/edit-recipe/{id}/', 'RecipeController@edit')->name('recipes.edit');
        Route::get('/recipe/product/{search?}', 'RecipeController@products')->name('recipe.product.search');
        Route::post('/recipe/update-recipe', 'RecipeController@update')->name('recipes.update');
        Route::post('/recipe/addproduct/', 'RecipeController@addProduct')->name('recipes.addProduct');
        Route::post('/recipe/removeproduct/', 'RecipeController@removeProduct')->name('recipes.removeProduct');
        Route::get('/products-of-recipe/{idRecipe}', 'RecipeController@listProducts');
        Route::get('/recipe/costProduction/{idRecipe}', 'RecipeController@costProduction')->name('recipes.costProduction');
        Route::get('/recipe/delete/{id}', 'RecipeController@recipeDelete')->name('recipes.delete');
        Route::get('/contracted_products_of_recipe/{idRecipe}', 'RecipeController@recipeProducts');
        //Recetas

        //Solicitudes
        Route::get('/request//history/{id}', 'RequestController@showHistoryRequest');
        Route::get('/request/data/history', 'RequestController@dataHistoryRequest');
        Route::get('/listRequest', 'RequestController@listRequest');
        Route::get('/request/data', 'RequestController@data');
        Route::get('/request/approve/{id}', 'RequestController@approve')->name('request.approve');
        Route::post('/request/setState/{id}', 'RequestController@setState');
        Route::get('/requestInformation/{id}', 'RequestController@requestInformation')->name('request.information');
        Route::resource('/request', 'RequestController');


        //Solicitudes

    });

    Route::group(['middleware' => ['role:Super-Admin|Lider-Produccion-centro|Coordinación']], function () {
        //producción de centro
        Route::get('/produccioncenter/approved', 'CenterProductionOrderController@requestapproved')->name('produccioncenter.approved');
        Route::get('/approved/data', 'CenterProductionOrderController@dataRequest');
        Route::get('/approved/history/data/{id}', 'CenterProductionOrderController@showDataHistory');
        Route::get('/produccioncenter/data', 'CenterProductionOrderController@data')->name('produccioncenter.data');
        Route::post('/produccioncenter/setState/{id}', 'CenterProductionOrderController@setState');
        Route::resource('/produccioncenter', 'CenterProductionOrderController')->except(['create', 'edit']);

        //producción de centro
    });


    //usuario
    Route::get('/perfil', 'UserController@index')->name('perfil');
    Route::post('/perfil', 'UserController@store')->name('perfil');
    Route::post('/user/changePassword', 'UserController@changePassword')->name('user.changePassword');
    Route::get('/measureunit/{id}', 'MeasureUnitsController@show');
    Route::get('/get/notifications', 'UserController@getNotifications');
    Route::get('/view/all/notifications', 'UserController@viewAllNotifications');
    Route::view('/config', 'modules.setting.config');

    //usuario





});
