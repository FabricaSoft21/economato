<?php

namespace App\Http\Controllers;

use App\Models\AditionalBudget;
use App\Models\Budget;
use Illuminate\Http\Request;
use Validator;

class AditionalBudgetController extends Controller
{

    public function index()
    {
        $adibudget = AditionalBudget::all();
        $budget = Budget::all();
        return view('modules.adibudget.index', compact('adibudget', 'budget'));
    }


    public function data()
    {
        $adibudget = AditionalBudget::select('tbl_aditional_budgets.*', 'tbl_budgets.budget_code as budgetss')
            ->join('tbl_budgets', 'tbl_aditional_budgets.budget_id', '=', 'tbl_budgets.id')
            ->get();


        return response()->json([
            "ok" => true,
            "data" => $adibudget
        ]);
    }



    public function store(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($input, [

            'budget_id' => 'required',
            'aditional_budget' => 'required',
            'aditional_budget_code' => 'required|unique:tbl_aditional_budgets,aditional_budget_code',
            'aditional_begin_date' => 'required',
            'aditional_finish_date' => 'required',

        ])->setAttributeNames([

            'budget_id' => 'Código del presupuesto',
            'aditional_budget' => 'Presupuesto adicional',
            'aditional_budget_code' => 'Código del presupuesto adicional',
            'aditional_begin_date' => 'Fecha inicio adicional',
            'aditional_finish_date' => 'Fecha fin adicional',

        ]);

        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages()
            ]);
        }

        try {


            AditionalBudget::create($input);

            return response()->json([
                "ok" => true,
                "message" => "Presupuesto adicional registrado con éxito."
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "ok" => false,
                "error" => $ex->getMessage()
            ]);
        }
    }


    public function show($id)
    {
        $budget = AditionalBudget::with('Budget')->where("id", $id)->first();

        if ($budget == null) {
            return response()->json([
                "ok" => false,
                "message" => "No se ha encontrado nungún presupuesto adicional."
            ]);
        } else {

            return response()->json([
                "ok" => true,
                "data" => $budget,
            ]);
        }
    }


    public function update(Request $request)
    {

        $input = $request->all();


        $validator = Validator::make($input, [

            'initial_budget' => 'required',
            'budget_code' => 'required',
            'budget_begin' => 'required',
            'budget_finish' => 'required',

        ]);
        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages()
            ]);
        }

        try {
            $id = $request->id;
            $budget = AditionalBudget::find($id);
            $budget->update($input);

            return response()->json([
                "ok" => true,
                "message" => "Presupuesto Actualizado con éxito."
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "ok" => false,
                "error" => $ex->getMessage()
            ]);
        }
    }


    public function destroy(Request $request)
    {
        $id = $request->id;
        $budget = AditionalBudget::find($id);

        $budget->update([
            'status' => $budget->status == 1 ? 0 : 1
        ]);

        return response()->json([
            'ok' => true,
            'message' => 'se cambio el estado'
        ]);
    }
}
