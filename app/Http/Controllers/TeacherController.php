<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Validator;
use Hash;


class TeacherController extends Controller
{



    public function index()
    {
        $teacher = User::select("users.id as id", "users.name as name", "users.lastname as lastname", "users.identification as identification", "users.foto as foto", "users.phone as phone", "users.status as status", "users.email as email")
            ->join("model_has_roles", "model_has_roles.model_id", "=", "users.id")
            ->where("model_has_roles.role_id", 1)
            ->get();

        return view("modules.teacher.index", compact("teacher"));
        //  dd($teacher);
    }



    public function data()
    {

        $teacher = User::select("users.id as id", "users.name as name", "users.lastname as lastname", "users.identification as identification", "users.foto as foto", "users.phone as phone", "users.status as status", "users.email as email")
            ->join("model_has_roles", "model_has_roles.model_id", "=", "users.id")
            ->where("model_has_roles.role_id", 2)
            ->get();

        if ($teacher == null) {
            return response()->json([
                "ok" => false,
                "message" => "No existen instructores registrados.",
            ]);
        } else {

            return response()->json([
                "ok" => true,
                "data" => $teacher,
            ]);
        }
    }



    public function store(Request $request)
    {

        $input = $request->all();


        $validator = Validator::make($input, [
            "name" => "required",
            "lastname" => "required",
            "email" => "required|email|unique:users",
            "phone" => "required",
            "identification" => "required|unique:users",
            "password" => "required",
        ])->setAttributeNames([
            "name" => "nombre",
            "lastname" => "apellidos",
            "email" => "correo electrónico",
            "phone" => "teléfono",
            "identification" => "número de identificación",
            "password" => "contraseña",
        ]);

        if ($validator->fails()) {
            return response()->json([
                "ok" => false,
                "message" => $validator->messages()
            ]);
        }

        try {

            $user = User::create([
                "name" => $input["name"],
                "lastname" => $input["lastname"],
                "email" => strtolower($input["email"]),
                "password" => Hash::make($input["password"]),
                "identification" => $input["identification"],
                "phone" => $input["phone"],
            ]);

            $user->assignRole("Instructores");

            return response()->json([
                "ok" => true,
                "message" => "Instructor registrado con éxito."
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "ok" => false,
                "error" => $ex->getMessage()
            ]);
        }
    }





    public function show($id)
    {
        $teacher = User::find($id);

        if ($teacher == null) {
            return response()->json([
                "ok" => false,
                "message" => "No se han encontrado instructores registrados."
            ]);
        } else {
            return response()->json([
                "ok" => true,
                "data" => $teacher,
            ]);
        }
    }




    public function update(Request $request)
    {


        $input = $request->all();
        $id = $request->id;

        $validator = Validator::make($input, [
            "name" => "required",
            "lastname" => "required",
            "email" => "required|unique:users,email," . $id,
            "phone" => "required",
            "identification" => "required|unique:users,identification," . $id


        ])->setAttributeNames([
            "name" => "nombre",
            "lastname" => "apellidos",
            "email" => "correo electrónico",
            "phone" => "teléfono",
            "identification" => "número de identificación",
        ]);
        if ($validator->fails()) {
            return response()->json([
                "ok" => false,
                "message" => $validator->messages()
            ]);
        }


        $teacher = User::find($id);
        $teacher->update($input);

        return response()->json([
            "ok" => true,
            "message" => "Información del instructor actualizada con éxito."
        ]);
    }




    public function destroy(Request $request)
    {
        $id = $request->id;
        $teacher = User::find($id);

        try {
            $teacher->update([
                "status" => $teacher->status == 1 ? 0 : 1
            ]);

            return response()->json([
                "ok" => true,
                "message" => "se cambio el estado"
            ]);
        } catch (\Throwable $th) {

            return response()->json([
                "ok" => false,
                "error" =>  $th
            ]);
        }
    }
}
