<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Provider;
use Illuminate\Support\Facades\Validator;

class ProviderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(['permission:all providers'], ['only' => ['index', 'data', 'create', 'store', 'show', 'edit', 'update', 'delete']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modules.provider.index');
    }
    public function data()
    {
        $providers = Provider::all();

        if (count($providers) == 0) {
            return response()->json([
                "ok" => false,
                "message" => 'No hay proveedores registrados.'
            ]);
        } else {
            return response()->json([
                "ok" => true,
                "data" => $providers
            ]);
        }
    }

    protected function getProvidersActives()
    {
        try {
            $providers = Provider::where('status', 0)->get();
            return response()->json([
                'ok' => true,
                'data' => $providers
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'ok' => false
            ]);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'NIT' => ['required', 'unique:tbl_providers'],
            'name' => ['required', 'string', 'unique:tbl_providers', 'max:45'],
            'phone' => ['required', 'string', 'max:20'],
            'email' => ['required', 'string', 'email', 'max:50', 'unique:tbl_providers'],
            'contact_name' => ['required', 'string', 'max:45'],
            'last_name' => ['required', 'string', 'max:45'],
            'phone_contact' => ['required', 'string', 'max:60'],
        ])->setAttributeNames([
            'name' => 'nombre',
            'phone' => 'teléfono',
            'email' => 'correo',
            'contact_name' => 'nombre del contacto',
            'last_name' => 'apellido del contacto',
            'phone_contact' => 'teléfono del contacto'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages()
            ]);
        }

        try {


            Provider::create($input);

            return response()->json([
                "ok" => true,
                "message" => "Proveedor registrado con exito."
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "ok" => false,
                "error" => $ex->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $provider = Provider::find($id);

        if ($provider == null) {
            return response()->json([
                'ok' => false,
                'message' => 'No se encontro ningún proveedor.'
            ]);
        } else {
            return response()->json([
                'ok' => true,
                'data' => $provider
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
