<?php

namespace App\Http\Controllers;

use App\Models\MeasureUnits;
use Illuminate\Http\Request;

class MeasureUnitsController extends Controller
{
    public function show($id)
    {
        $measureUnit = MeasureUnits::find($id);
        
        return response()->json([
            'data'=>$measureUnit
        ]);
    }
}
