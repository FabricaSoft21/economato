<?php

namespace App\Http\Controllers;

use App\Mail\NuevoPedido;
use App\Models\Center;
use App\Models\CenterProductionOrder;
use App\Models\CenterXProduct;
use App\Models\Email;
use App\Models\Notification;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Auth;

class CenterProductionOrderController extends Controller
{


    public function index()
    {

        $centers = Center::all();
        return view('modules.centerProduction.index', compact('centers'));
    }


    public function data()
    {
        $rol = DB::table('model_has_roles')
            ->select('roles.name')
            ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
            ->join('users', 'users.id', '=', 'model_has_roles.model_id')
            ->where('users.id', Auth::user()->id)
            ->first();

        if ($rol->name == 'Lider-Produccion-centro') {
            $request = CenterProductionOrder::select(
                'tbl_center_production_orders.id as id',
                'tbl_center_production_orders.quantity_people as quantity_people',
                'tbl_center_production_orders.descripcion as descripcion',
                'tbl_center_production_orders.order_date as order_date',
                'tbl_center_production_orders.title as title',
                'tbl_center_production_orders.cost as cost',
                'tbl_centers.name as centerName',
                'tbl_center_production_orders.status_id as status_id',
                'tbl_status_orders.name as statusName'
            )
                ->join('tbl_centers', 'tbl_center_production_orders.center_id', 'tbl_centers.id')
                ->join('tbl_status_orders', 'tbl_center_production_orders.status_id', 'tbl_status_orders.id')
                ->join('users', 'users.id', '=', 'tbl_center_production_orders.user_id')
                ->where('users.id', Auth::user()->id)
                ->get();
            if (count($request) > 0) {
                return response()->json([
                    'ok' => true,
                    'data' => $request
                ]);
            } else {
                return response()->json([
                    'ok' => false,
                    'message' => 'No tienes solicitudes registradas.'
                ]);
            }
        } else if ($rol->name == 'Coordinación') {
            $request = CenterProductionOrder::select(
                'tbl_center_production_orders.id as id',
                'tbl_center_production_orders.quantity_people as quantity_people',
                'tbl_center_production_orders.descripcion as descripcion',
                'tbl_center_production_orders.order_date as order_date',
                'tbl_center_production_orders.title as title',
                'tbl_center_production_orders.cost as cost',
                'tbl_centers.name as centerName',
                'tbl_center_production_orders.status_id as status_id',
                'tbl_status_orders.name as statusName'
            )
                ->join('tbl_centers', 'tbl_center_production_orders.center_id', 'tbl_centers.id')
                ->join('tbl_status_orders', 'tbl_center_production_orders.status_id', 'tbl_status_orders.id')
                ->where('tbl_status_orders.id', 1)
                ->get();

            if (count($request) > 0) {
                return response()->json([
                    'ok' => true,
                    'data' => $request
                ]);
            } else {
                return response()->json([
                    'ok' => false,
                    'message' => 'No hay nuevas solicitudes.'
                ]);
            }
        } else if ($rol->name == 'Bodega') {
            $request = CenterProductionOrder::select(
                'tbl_center_production_orders.id as id',
                'tbl_center_production_orders.quantity_people as quantity_people',
                'tbl_center_production_orders.descripcion as descripcion',
                'tbl_center_production_orders.order_date as order_date',
                'tbl_center_production_orders.title as title',
                'tbl_center_production_orders.cost as cost',
                'tbl_centers.name as centerName',
                'tbl_center_production_orders.status_id as status_id',
                'tbl_status_orders.name as statusName'
            )
                ->join('tbl_centers', 'tbl_center_production_orders.center_id', 'tbl_centers.id')
                ->join('tbl_status_orders', 'tbl_center_production_orders.status_id', 'tbl_status_orders.id')
                ->where('tbl_status_orders.id', 2)
                ->get();

            if (count($request) > 0) {
                return response()->json([
                    'ok' => true,
                    'data' => $request
                ]);
            } else {
                return response()->json([
                    'ok' => false,
                    'message' => 'No hay nuevas solicitudes.'
                ]);
            }
        }
    }


    public function create()
    {
        //
    }


    public function requestapproved()
    {
        return view('modules.centerProduction.approved');
    }

    public function dataRequest()
    {
        $approved = CenterProductionOrder::select('tbl_center_production_orders.*', 'tbl_centers.name as nombreCentro', 'users.name as username')
            ->join('tbl_centers', 'tbl_center_production_orders.center_id', '=', 'tbl_centers.id')
            ->join('users', 'tbl_center_production_orders.user_id', '=', 'users.id')
            ->get();

        return response()->json([
            "ok" => true,
            "data" => $approved
        ]);
    }

    public function showDataHistory($id)
    {
        try {

            $history = CenterProductionOrder::select('tbl_center_production_orders.*', 'tbl_centers.name as nombreCentro', 'users.name as username')
                ->join('tbl_centers', 'tbl_center_production_orders.center_id', '=', 'tbl_centers.id')
                ->join('users', 'tbl_center_production_orders.user_id', '=', 'users.id')
                ->where('tbl_center_production_orders.id', $id)
                ->first();

            return response()->json([
                "ok" => true,
                "data" => $history
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "ok" => false,
                "message" => $th
            ]);
        }
    }





    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'order_date' => 'required',
            'quantity_people' => 'required|integer',
            'title' => 'required|string|max:100',
            'descripcion' => 'required|string|max:250',
            'cost' => 'required',
            'center_id' => 'required'
        ])->setAttributeNames([
            'order_date' => 'fecha',
            'quantity_people' => 'cantidad de personas',
            'title' => 'título',
            'descripcion' => 'descripción',
            'cost' => 'costo',
            'center_id' => 'centro'
        ]);
        $input['user_id'] = Auth::user()->id;
        $input['status_id'] = 1;

        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages()
            ]);
        } else {
            try {
                $order = CenterProductionOrder::create($input);
                $products = $input['products'];

                foreach ($products as $p) {
                    $centerXproduct = new CenterXProduct();
                    $centerXproduct->center_production_order_id = $order->id;
                    $centerXproduct->product_id = $p['product_id'];
                    $centerXproduct->quantity = $p['quantity'];
                    $centerXproduct->save();
                }

                $from = DB::table('model_has_roles')
                    ->select('users.email', 'users.id')
                    ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                    ->join('users', 'users.id', '=', 'model_has_roles.model_id')
                    ->where('roles.name', 'Coordinación')
                    ->first();


                $notify = new Notification();
                $notify->user_id = $from->id;
                $notify->title = 'Nueva solicitud';
                $notify->description = 'El usuario ' . $order->user->name . ' acaba de crear una nueva solicitud para produccion de centro.';
                $notify->save();


                return response()->json([
                    'ok' => true,
                    'message' => 'Orden de producción registrada correctamente',
                ]);
            } catch (\Throwable $ex) {
                return response()->json([
                    'ok' => false,
                    "error" => $ex->getMessage()
                ]);
            }
        }
    }



    public function show($id)
    {
        $head = CenterProductionOrder::where('id', $id)->with(['centersXproducts', 'center', 'statusOrder', 'user'])->first();
        // $products = CenterXProduct::where('center_production_order_id',$id)->with('product')->get();

        $products = CenterXProduct::select(
            'tbl_products.name as name',
            'tbl_productsXcontracts.total_with_tax as unitPrice',
            'tbl_centerXproducts.quantity as quantity',
            DB::raw('(tbl_centerXproducts.quantity*tbl_productsXcontracts.total_with_tax) as subtotal')
        )
            ->join('tbl_products', 'tbl_products.id', 'tbl_centerXproducts.product_id')
            ->join('tbl_productsXcontracts', 'tbl_productsXcontracts.id', 'tbl_products.id')
            ->where('tbl_centerXproducts.center_production_order_id', $id)
            ->get();

        $order = [
            'head' => $head,
            'products' => $products
        ];
        if ($head == null) {
            return response()->json([
                'ok' => false,
                'message' => 'No se encontró la orden.'
            ]);
        } else {
            return response()->json([
                'ok' => true,
                'data' => $order,
            ]);
        }
    }




    public function setState(Request $request, $id)
    {

        try {
            $newState = $request['newState'];
            $centerProductionOrder = CenterProductionOrder::find($id);
            $centerProductionOrder->update([
                'status_id' => $newState
            ]);
            if ($centerProductionOrder->status_id == 2) {
                // Crear la notificación
                $notify = new Notification();
                $notify->user_id = $centerProductionOrder->user_id;
                $notify->title = 'Se aprobó tu pedido';
                $notify->description = 'La coordinación ha aprobado tu produccion de centro, espera a que llegue el pedido.';
                $notify->save();

                // Enviar correo
                $email = new Email();
                $email->order_id = $centerProductionOrder->id;
                $email->user = $centerProductionOrder->user->name;
                $email->date = Carbon::now();
                $email->cost = $centerProductionOrder->cost;

                $from = DB::table('model_has_roles')
                    ->select('users.email', 'users.id')
                    ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                    ->join('users', 'users.id', '=', 'model_has_roles.model_id')
                    ->where('roles.name', 'Bodega')
                    ->first();

                $notify = new Notification();
                $notify->user_id = $from->id;
                $notify->title = 'Nuevo pedido';
                $notify->description = 'La coordinación ha aprobado la produccion de centro del usuario ' . $centerProductionOrder->user->name . '.';
                $notify->save();

                Mail::to($from->email)->send(new NuevoPedido($email));
                return response()->json([
                    'ok' => true,
                    'message' => 'Se aprobó la solicitud con éxito'
                ]);
            } else if ($centerProductionOrder->status_id == 3) {
                // Crear la notificación
                $notify = new Notification();
                $notify->user_id = $centerProductionOrder->user_id;
                $notify->title = 'Se anuló tu pedido';
                $notify->description = 'La coordinación ha anulado tu produccion de centro (' . $centerProductionOrder->center->name . ')';
                $notify->save();

                return response()->json([
                    'ok' => true,
                    'message' => 'Se anuló la solicitud con éxito'
                ]);
            } else if ($centerProductionOrder->status_id == 4) {
                // 
                $budget = Budget::where('status', 0)->first();
                $budget->update([
                    'remaining' => $budget->remaining - $centerProductionOrder->cost
                ]);


                return response()->json([
                    'ok' => true,
                    'message' => 'Se cambio el estado con éxito'
                ]);
            }
        } catch (\Exepcion $ex) {

            $from = DB::table('model_has_roles')
                ->select('users.email', 'users.id')
                ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                ->join('users', 'users.id', '=', 'model_has_roles.model_id')
                ->where('roles.name', 'Coordinación')
                ->first();

            $notify = new Notification();
            $notify->user_id = $from->id;
            $notify->title = 'Pedido entregado';
            $notify->description = 'Se acaba de entregar el pedido de ' . $centerProductionOrder->user->name . '.';
            $notify->save();

            return response()->json([
                'ok' => false,
                'message' => $ex->getMessage()
            ]);
        }
    }
}
