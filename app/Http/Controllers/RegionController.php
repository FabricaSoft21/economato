<?php

namespace App\Http\Controllers;

use App\Models\Region;
use Illuminate\Http\Request;
use Validator;

class RegionController extends Controller
{

    public function index()
    {
        $region = Region::all();
        return view('modules.region.index', compact('region'));
    }


    public function data()
    {

        $region = Region::all();
        return response()->json([
            "ok" => true,
            "data" => $region,
        ]);
    }


    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => ['required','unique:tbl_regions,name'],
        ])->setAttributeNames([
            'name' => 'nombre',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages(),
            ]);
        } else {
            try {
                Region::create($input);
                return response()->json([
                    'ok' => true,
                    'message' => 'Regional registrada con éxito',
                ]);
            } catch (\Throwable $ex) {
                return response()->json([
                    'ok' => false,
                    "error" => $ex->getMessage()
                ]);
            }
        }
    }



    public function show($id)
    {
        $region = Region::find($id);
        if ($region == null) {
            return response()->json([
                'ok' => true,
                'message' => "No hay Regionales para mostrar"
            ]);
        } else {
            return response()->json([
                'ok' => true,
                'data' => $region,
            ]);
        }
    }



    public function update(Request $request)
    {
        $id = $request->id;
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required|unique:tbl_regions,name,' . $id,
        ])->setAttributeNames([
            'name' => 'nombre',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages()
            ]);
        } else {
            try {


                $region = Region::find($id);
                $region->update($input);

                return response()->json([
                    'ok' => true,
                    'message' => 'Regional actualizada con éxito',
                ]);
            } catch (\Throwable $ex) {
                return response()->json([
                    'ok' => false,
                    "error" => $ex->getMessage()
                ]);
            }
        }
    }


    public function destroy($request)
    {
        $id = $request->id;
        $region = Region::find($id);

        $region->update([
            'status' => $region->status == 1 ? 0 : 1
        ]);

        return response()->json([
            'ok' => true,
            'message' => 'se cambio el estado'
        ]);
    }
}
