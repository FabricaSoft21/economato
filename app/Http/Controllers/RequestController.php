<?php

namespace App\Http\Controllers;

use App\Models\Center;
use App\Models\File;
use App\Models\Order;
use App\Models\UserXfile;
use Illuminate\Http\Request;
use App\Models\OrdersXproduct;
use App\Models\ProductsXContract;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Mail\NuevoPedido;
use App\Models\Budget;
use App\Models\CenterProductionOrder;
use Illuminate\Support\Facades\Mail;
use App\Models\Email;
use App\Models\Notification;
use DB;
use Illuminate\Support\Carbon;

class RequestController extends Controller
{


    public function index()
    {
        $request = Order::all();

        $id = \Auth::user()->id;
        $files = UserXfile::where('user_id', $id)->get();

        return view('modules.request.index', compact('request', 'files'));
    }

    public function data()
    {
        $rol = DB::table('model_has_roles')
            ->select('roles.name')
            ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
            ->join('users', 'users.id', '=', 'model_has_roles.model_id')
            ->where('users.id', Auth::user()->id)
            ->first();

        if ($rol->name == 'Instructores') {
            $request = Order::select(
                'tbl_orders.id as id',
                'tbl_files.number as number',
                'tbl_orders.created_at as date',
                'tbl_orders.description as description',
                'tbl_files.number as file',
                'tbl_status_orders.name as status',
                'tbl_status_orders.id as status_id'
            )
                ->join('tbl_files', 'tbl_orders.file_id', 'tbl_files.id')
                ->join('tbl_status_orders', 'tbl_orders.status_id', 'tbl_status_orders.id')
                ->join('users', 'users.id', '=', 'tbl_orders.user_id')
                ->where('users.id', Auth::user()->id)
                ->get();
            if (count($request) > 0) {
                return response()->json([
                    'ok' => true,
                    'data' => $request
                ]);
            } else {
                return response()->json([
                    'ok' => false,
                    'message' => 'No tienes solicitudes registradas.'
                ]);
            }
        } else if ($rol->name == 'Coordinación') {
            $request = Order::select(
                'tbl_orders.id as id',
                'tbl_files.number as number',
                'tbl_orders.created_at as date',
                'tbl_orders.description as description',
                'tbl_files.number as file',
                'tbl_status_orders.name as status',
                'tbl_status_orders.id as status_id',
                'users.name as userName'
            )
                ->join('tbl_files', 'tbl_orders.file_id', 'tbl_files.id')
                ->join('tbl_status_orders', 'tbl_orders.status_id', 'tbl_status_orders.id')
                ->join('users', 'users.id', '=', 'tbl_orders.user_id')
                ->where('tbl_orders.status_id', 1)
                ->get();

            if (count($request) > 0) {
                return response()->json([
                    'ok' => true,
                    'data' => $request
                ]);
            } else {
                return response()->json([
                    'ok' => false,
                    'message' => 'No hay nuevas solicitudes.'
                ]);
            }
        } else if ($rol->name == 'Bodega') {
            $request = Order::select(
                'tbl_orders.id as id',
                'tbl_files.number as number',
                'tbl_orders.created_at as date',
                'tbl_orders.description as description',
                'tbl_files.number as file',
                'tbl_status_orders.name as status',
                'tbl_status_orders.id as status_id',
                'users.name as userName'
            )
                ->join('tbl_files', 'tbl_orders.file_id', 'tbl_files.id')
                ->join('tbl_status_orders', 'tbl_orders.status_id', 'tbl_status_orders.id')
                ->join('users', 'users.id', '=', 'tbl_orders.user_id')
                ->where('tbl_orders.status_id', 2)
                ->get();

            if (count($request) > 0) {
                return response()->json([
                    'ok' => true,
                    'data' => $request
                ]);
            } else {
                return response()->json([
                    'ok' => false,
                    'message' => 'No hay nuevas solicitudes.'
                ]);
            }
        }
    }


    public function store(Request $request)
    {
        $input = $request->all();

        $product = $request->products;
        $validator = Validator::make($input, [
            'file_id' => 'required',
        ])->setAttributeNames([
            'file_id' => 'ficha',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages()
            ]);
        } else {
            $id =  \Auth::user()->id;
            try {
                $order = Order::create([
                    'user_id' => $id,
                    'description' => $input['description'],
                    'file_id' => $input['file_id'],
                    'cost' => $input['cost'],
                ]);

                foreach ($product as $pr) {
                    OrdersXproduct::create([
                        'order_id' => $order->id,
                        'product_id' =>  $pr['product_id'],
                        'quantity' => $pr['quantity'],
                        'subtotal' => $pr['subtotal'],
                    ]);
                }
                $from = DB::table('model_has_roles')
                    ->select('users.email', 'users.id')
                    ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                    ->join('users', 'users.id', '=', 'model_has_roles.model_id')
                    ->where('roles.name', 'Coordinación')
                    ->first();

                $notify = new Notification();
                $notify->user_id = $from->id;
                $notify->title = 'Nueva solicitud';
                $notify->description = 'El usuario ' . $order->user->name . ' acaba de crear una nueva solicitud de pedido.';
                $notify->save();
                return response()->json([
                    'ok' => true,
                    'message' => 'Solicitud registrada correctamente',
                ]);
            } catch (\Throwable $ex) {
                return response()->json([
                    'ok' => false,
                    "error" => $ex->getMessage()
                ]);
            }
        }
    }


    public function show($id)
    {
        //
    }



    public function edit($id)
    {
        //
    }



    public function update(Request $request, $id)
    {
        $input = $request->all();

        $order = Order::find($id);

        try {
            // dd($input['cost']);
            $order->update([
                'cost' => $input['cost']
            ]);

            OrdersXproduct::where('order_id', $id)->delete();

            foreach ($request->products as $pr) {
                OrdersXproduct::create([
                    'order_id' => $id,
                    'product_id' =>  $pr['product_id'],
                    'quantity' => $pr['quantity'],
                    'subtotal' => $pr['subtotal'],
                ]);
            }

            return response()->json([
                'ok' => true,
                'message' => 'Orden actualizada con éxito.'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'ok' => false,
                'message' => $th->getMessage()
            ]);
        }
    }



    public function setState(Request $request, $id)
    {
        try {
            $newState = $request['newState'];
            $order = Order::find($id);
            $order->update([
                'status_id' => $newState
            ]);
            if ($order->status_id == 2) {
                // Crear la notificación
                $notify = new Notification();
                $notify->user_id = $order->user_id;
                $notify->title = 'Se aprobó tu pedido';
                $notify->description = 'La coordinación ha aprobado tu pedido, espera a que llegue.';
                $notify->save();

                // Enviar correo
                $email = new Email();
                $email->order_id = $order->id;
                $email->user = $order->user->name;
                $email->date = Carbon::now();
                $email->cost = $order->cost;

                $from = DB::table('model_has_roles')
                    ->select('users.email', 'users.id')
                    ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                    ->join('users', 'users.id', '=', 'model_has_roles.model_id')
                    ->where('roles.name', 'Bodega')
                    ->first();

                $notify = new Notification();
                $notify->user_id = $from->id;
                $notify->title = 'Nuevo pedido';
                $notify->description = 'La coordinación ha aprobado el pedido del instructor ' . $order->user->name . '.';
                $notify->save();

                Mail::to($from->email)->send(new NuevoPedido($email));
                return response()->json([
                    'ok' => true,
                    'message' => 'Se aprobó la solicitud con éxito'
                ]);
            } else if ($order->status_id == 3) {
                // Crear la notificación
                $notify = new Notification();
                $notify->user_id = $order->user_id;
                $notify->title = 'Se anuló tu pedido';
                $notify->description = 'La coordinación ha anulado tu pedido.';
                $notify->save();

                return response()->json([
                    'ok' => true,
                    'message' => 'Se anuló la solicitud con éxito'
                ]);
            } else if ($order->status_id == 4) {
                // 
                $budget = Budget::where('status', 0)->first();
                $budget->update([
                    'remaining' => $budget->remaining - $order->cost
                ]);


                return response()->json([
                    'ok' => true,
                    'message' => 'Se cambio el estado con éxito'
                ]);
            }
        } catch (\Exepcion $ex) {

            $from = DB::table('model_has_roles')
                ->select('users.email', 'users.id')
                ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                ->join('users', 'users.id', '=', 'model_has_roles.model_id')
                ->where('roles.name', 'Coordinación')
                ->first();

            $notify = new Notification();
            $notify->user_id = $from->id;
            $notify->title = 'Pedido entregado';
            $notify->description = 'Se acaba de entregar el pedido de ' . $order->user->name . '.';
            $notify->save();

            return response()->json([
                'ok' => false,
                'message' => $ex->getMessage()
            ]);
        }
    }

    public function listRequest()
    {

        $orders = Order::all();


        return view('modules.request.listRequest', ['orders' => $orders]);
    }


    public function requestInformation($id)
    {

        $request = Order::where('id', $id)->with('statusOrder', 'user')->first();
        $OrderXProducts = OrdersXproduct::where('order_id', $id)->with('order', 'productXcontract')->get();
        $file = File::find($request->file_id)->with('characterization')->first();


        $data = ['request' => $request, 'file' => $file, 'orderXProducts' => $OrderXProducts];

        return response()->json([
            'ok' => true,
            'data' => $data
        ]);
    }


    public function dataHistoryRequest()
    {

        try {
            $request = Order::with("file", "user")->get();
            return response()->json([
                "ok" => true,
                "data" => $request
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "ok" => false,
                "message" => $th
            ]);
        }
    }


    public function showHistoryRequest($id)
    {
        $request = Order::select(
            "tbl_orders.id as id",
            "tbl_orders.cost as costt",
            "tbl_orders.description as descriptions",
            "tbl_orders.status_id as status_ids",
            "users.name as usernames",
            "tbl_files.number as file"
        )

            ->join("users", "users.id", "=", "tbl_orders.user_id")
            ->join("tbl_files", "tbl_files.id", "=", "tbl_orders.file_id")
            ->where("tbl_orders.id", $id)
            ->first();

        try {
            return response()->json([
                "ok" => true,
                "data" => $request
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "ok" => false,
                "message" => $th
            ]);
        }
    }
}
