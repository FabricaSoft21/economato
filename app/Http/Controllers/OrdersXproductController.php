<?php

namespace App\Http\Controllers;

use App\Models\OrdersXproduct;
use Illuminate\Http\Request;

class OrdersXproductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrdersXproduct  $ordersXproduct
     * @return \Illuminate\Http\Response
     */
    public function show(OrdersXproduct $ordersXproduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrdersXproduct  $ordersXproduct
     * @return \Illuminate\Http\Response
     */
    public function edit(OrdersXproduct $ordersXproduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OrdersXproduct  $ordersXproduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrdersXproduct $ordersXproduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrdersXproduct  $ordersXproduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrdersXproduct $ordersXproduct)
    {
        //
    }
}
