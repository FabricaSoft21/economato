<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Budget;
use App\Models\BudgetXCharacterization;
use App\Models\Characterization;
use Validator;
use Carbon\Carbon;
use DB;

class BudgetController extends Controller
{

    public function index()
    {
        $budget = Budget::all();
        $charact = Characterization::all();

        $this->validate_date();
        $countVigente  = Budget::where('status', 0)->count();
        return view('modules.budget.index', compact('budget', 'charact', 'countVigente'));
    }

    public function validate_date()
    {
        $date = Carbon::now()->toDateString();

        DB::table("tbl_budgets")
            ->where("budget_finish", "<=", $date)
            ->update(["tbl_budgets.status" => 1]);
    }


    public function data()
    {

        $butget = Budget::all();


        return response()->json([
            "ok" => true,
            "data" => $butget,
        ]);
    }


    public function store(Request $request)
    {

        $input = $request->all();

        $budgetXcharact =  $request->budgetXcharact;


        $validator = Validator::make($input, [
            'initial_budget' => 'required',
            'budget_code' => 'required|unique:tbl_budgets',
            'budget_begin' => 'required',
            'budget_finish' => 'required',
        ])->setAttributeNames([
            'initial_budget' => 'Presupuesto inicial',
            'budget_code' => 'Código del presupuesto',
            'budget_begin' => 'Fecha inicio',
            'budget_finish' => 'Fecha fin',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages()
            ]);
        }
        
        try {

            $bud = Budget::create($input);
            $this->validate_date();

            $total = $bud->initial_budget;

            foreach ($budgetXcharact as $bdc) {
                BudgetXCharacterization::create([
                    'budget_id' => $bud->id,
                    'characterization_id' => $bdc['id'],
                    'porcentage' => $bdc['porcentage'],
                    'value' => $bdc['value'],
                ]);
            }

            return response()->json([
                "ok" => true,
                "message" => "Presupuesto registrado con éxito."
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "ok" => false,
                "error" => $ex->getMessage()
            ]);
        }
    }


    public function show($id)
    {
        $butget = Budget::with('budgetsXcharacterizations')->find($id);
        // $char = BudgetXCharacterization::where('budget_id', $id)->get();
        if ($butget == null) {
            return response()->json([
                'ok' => false,
                'message' => 'No se ha encontrado nungún presupuesto.'
            ]);
        } else {
            $this->validate_date();

            return response()->json([
                'ok' => true,
                'data' => $butget,
            ]);
        }
    }



    public function charact()
    {
        $charact = Characterization::all();

        return response()->json($charact);
    }




    public function update(Request $request)
    {

        $input = $request->all();


        $validator = Validator::make($input, [

            'initial_budget' => 'required',
            'budget_code' => 'required',
            'budget_begin' => 'required',
            'budget_finish' => 'required',

        ]);
        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages()
            ]);
        }

        try {
            $id = $request->id;
            $budget = Budget::find($id);
            $budget->update($input);

            return response()->json([
                "ok" => true,
                "message" => "Presupuesto Actualizado con éxito."
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "ok" => false,
                "error" => $ex->getMessage()
            ]);
        }
    }


    public function destroy(Request $request)
    {
        $id = $request->id;
        $butget = Budget::find($id);

        $butget->update([
            'status' => $butget->status == 1 ? 0 : 1
        ]);

        return response()->json([
            'ok' => true,
            'message' => 'se cambio el estado'
        ]);
    }
    public function detail_bud($id)
    {
        $budget = Budget::find($id);
        $charact = BudgetXCharacterization::where('budget_id', $id)->get();
        $one = $budget->initial_budget/100;
        $porcetege_round = intval($budget->remaining/$one);

        $porcentage_exact = \round($budget->remaining/$one);

       if($porcetege_round % 5 != 0){
            $second = \substr($porcetege_round, 1);

            $a = $porcetege_round-$second;

            
            if($second > 5){
               $a = $a+5;
            }
            $porcetege_round = $a;
       }
       ;
        return view('modules.budget.detail', compact('budget', 'charact', 'porcetege_round', 'porcentage_exact'));
    }

    public function charts($id)
    {

        $moneyXchart = DB::table('tbl_budgetxcharacterizations')
            ->select('tbl_characterizations.name', 'tbl_budgetxcharacterizations.porcentage as porcentage')
            ->join('tbl_characterizations', 'tbl_budgetxcharacterizations.characterization_id', '=', 'tbl_characterizations.id')
            ->join('tbl_budgets', 'tbl_budgetxcharacterizations.budget_id', '=', 'tbl_budgets.id')
            ->where('tbl_budgetxcharacterizations.budget_id', $id)
            ->get();

        $spent_charact = DB::table('tbl_orders')
            ->select(DB::raw('SUM(tbl_orders.cost) as total'), 'tbl_characterizations.name as characterization')
            ->join('tbl_files', 'tbl_orders.file_id', '=', 'tbl_files.id')
            ->join('tbl_characterizations', 'tbl_files.characterization_id', '=', 'tbl_characterizations.id')
            ->groupBy('tbl_characterizations.name')
            ->where('tbl_orders.status_id', 4)
            ->get();

        $prodution = DB::table('tbl_center_production_orders')
                      ->select(DB::raw('SUM(tbl_center_production_orders.cost) as produc_total' ))
                         ->get();

        $bud = Budget::find($id);
        $one = $bud->initial_budget/100;
        $porcenta_spent = $bud->remaining/$one;

        return response()->json([
            'ok' => true,
            'moneyXchart' => $moneyXchart,
            'spent_charact' => $spent_charact,
            'production' => $prodution,
        ]);
    }
}
