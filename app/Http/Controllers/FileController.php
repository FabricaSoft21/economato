<?php

namespace App\Http\Controllers;

use App\Models\Center;
use App\Models\Characterization;
use App\Models\File;
use App\Models\Program;
use App\User;
use Illuminate\Http\Request;
use App\Models\UserXfile;
use Carbon\Carbon;
use Validator;
use DB;

class FileController extends Controller
{



    public function index()
    {
        $center = Center::all();
        $program = Program::all();
        $charac = Characterization::all();
        


        $teacher = User::select("users.name", "users.id")
            ->join("model_has_roles", "users.id", "=", "model_has_roles.model_id")
            ->join("roles", "model_has_roles.role_id", "roles.id")
            ->where("model_has_roles.role_id",  "2")
            ->where("users.status", "0")
            ->get();


        $this->valdate_date();

        return view("modules.files.index", compact("center", "charac", "program", "teacher"));
    }



    public function valdate_date()
    {
        $date = Carbon::now()->toDateString();

        DB::table("tbl_files")
            ->where("finish_date", "<=", $date)
            ->update(["tbl_files.status" => 1]);
    }



    public function data()
    {
        $this->valdate_date();

        $file = File::select(
            "tbl_files.id as id",
            "tbl_programs.name as program",
            "tbl_characterizations.name as charact",
            "tbl_files.number as number",
            "tbl_files.apprentices as people",
            "tbl_files.status as status"
        )
            ->join("tbl_centers", "tbl_files.center_id", "=", "tbl_centers.id")
            ->join("tbl_programs", "tbl_files.program_id", "=", "tbl_programs.id")
            ->join("tbl_characterizations", "tbl_files.characterization_id", "=", "tbl_characterizations.id")
            ->get();



        if (count($file) > 0) {
            return response()->json([
                "ok" => true,
                "data" => $file,
            ]);
        } else {
            return response()->json([
                "ok" => false,
                "message" => "No hay fichas registradas",
            ]);
        }
    }



    public function get_program($id)
    {
        $program = Program::select("tbl_programs.id as id", "tbl_programs.name as name", "tbl_programs.version as version")
            ->join("tbl_centerxprogram", "tbl_centerxprogram.program_id", "tbl_programs.id")
            ->where("tbl_centerxprogram.center_id", "=", $id)
            ->get();


        return response()->json($program);
    }




    public function store(Request $request)
    {
        $input = $request->all();

      
        $teacher = $request->teachers;

        $validator = Validator::make($input, [
            "number" => "required|numeric",
            "apprentices" => "required",
            "start_date" => "required",
            "finish_date" => "required",
            "program_id" => "required",
            "characterization_id" => "required",
            "center_id" => "required"
        ])->setAttributeNames([
            "number" => "numero de ficha",
            "apprentices" => "numero de aprendices",
            "start_date" => "fecha de inicio",
            "finish_date" => "fecha de terminacion",
            "program_id" => "programa de formacion",
            "characterization_id" => "caracterizacion",
            "center_id" => "centro de formacion"
        ]);

        if ($validator->fails()) {
            return response()->json([
                "ok" => false,
                "message" => $validator->messages()
            ]);
        } else {
            try {


                $file = File::create($input);

                foreach ($teacher as $th) {
                    UserXfile::create([
                        "user_id" => $th["id"],
                        "file_id" => $file->id
                    ]);
                }


                return response()->json([
                    "ok" => true,
                    "message" => "se registró con exito",
                ]);
            } catch (\Throwable $ex) {
                return response()->json([
                    "ok" => false,
                    "error" => $ex->getMessage()
                ]);
            }
        }
    }



    public function show($id)
    {
        $file = File::where("id", $id)->with("characterization", "program", "center")->first();

       

        if ($file == null) {
            return response()->json([
                "ok" => false,
                "message" => "No logramos encontrar información sobre esta ficha."
            ]);
        } else {
            return response()->json([
                "ok" => true,
                "data" => $file
            ]);
        }
    }




    public function update(Request $request, File $file)
    {
        //
    }


    public function destroy(File $file)
    {
        //
    }
}
