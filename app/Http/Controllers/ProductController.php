<?php

namespace App\Http\Controllers;

use App\Models\Contract;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\MeasureUnits;
use App\Models\PresentationProduct;
use App\Models\ProductsXContract;
use App\Models\Tax;
use App\Models\TypeProduct;
use Validator;
use DB;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        //especificar el permiso que solo va tener acceso y a que metodos en especifico
        $this->middleware(['permission:all products'], ['only' => 'index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unit = MeasureUnits::all();
        $presentation = PresentationProduct::all();
        $type = TypeProduct::all();
        $tax = Tax::all();

        return view('modules.product.index', compact('unit', 'presentation', 'type', 'tax'));
    }

    protected function GetProductsActives()
    {

        try {

            $products = Product::Select(
                'tbl_products.id',
                'tbl_products.name',
                'tbl_type_products.name as category',
                'tbl_presentation_products.name as presentation',
                'tbl_measure_units.name as measure',
                'tbl_taxes.tax as iva'
            )
                ->join('tbl_type_products', 'tbl_type_products.id', '=', 'tbl_products.type_product_id')
                ->join('tbl_presentation_products', 'tbl_presentation_products.id', '=', 'tbl_products.presentation_id')
                ->join('tbl_measure_units', 'tbl_measure_units.id', '=', 'tbl_products.measure_unit_id')
                ->join('tbl_taxes', 'tbl_taxes.id', '=', 'tbl_products.tax_id')
                ->where('tbl_products.status', 0)->get();

            return response()->json([
                'ok' => true,
                'data' => $products
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'ok' => false,
                'error' => $th->getMessage()
            ]);
        }
        //throw $th;

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();


        $validator = Validator::make($input, [
            'name' => 'required|unique:tbl_products|max:50',
            'type_product_id' => 'required',
            'presentation_id' => 'required',
            'measure_unit_id' => 'required',
            'tax_id' => 'required'
        ])->setAttributeNames([
            'name' => 'nombre',
            'type_product_id' => 'tipo de producto',
            'presentation_id' => 'presentacion',
            'measure_unit_id' => 'unidad de medida',
            'tax_id' => 'IVA'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages()
            ]);
        } else {
            try {
                Product::create($input);
                return response()->json([
                    'ok' => true,
                    'message' => 'Se registró correctamente',
                ]);
            } catch (\Throwable $ex) {
                return response()->json([
                    'ok' => false,
                    "error" => $ex->getMessage()
                ]);
            }
        }



        return response()->json([
            'ok' => true,
            'message' => 'Se registró correctamente',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $id = $request->id;

        $products = Product::find($id);

        return response()->json($products);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'type_product_id' => 'required',
            'presentation_id' => 'required',
            'measure_unit_id' => 'required',
        ])->setAttributeNames([
            'name' => 'nombre',
            'type_product_id' => 'tipo de producto',
            'presentation_id' => 'presentacion',
            'measure_unit_id' => 'unidad de medida',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages()
            ]);
        } else {
            try {
                $id = $request->id;
                $product = Product::find($id);
                $product->update($input);

                return response()->json([
                    'ok' => true,
                    'message' => 'Se Actualizó correctamente',
                ]);
            } catch (\Throwable $ex) {
                return response()->json([
                    'ok' => false,
                    "error" => $ex->getMessage()
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;

        $product = Product::find($id);


        $product->update([
            'status' => $product->status == 1 ? 0 : 1
        ]);

        return response()->json([
            'ok' => true,
            'message' => 'se cambio el estado'
        ]);
    }
    public function data()
    {
        //$products = Product::all();

        $products = DB::table('tbl_products')
            ->select(
                'tbl_type_products.name as type_product_name',
                'tbl_products.status as status',
                'tbl_presentation_products.name as presentation_name',
                'tbl_products.id as id',
                'tbl_measure_units.name as measure_name',
                'tbl_products.name as name',
                'tbl_taxes.tax as tax'
            )
            ->join('tbl_type_products', 'tbl_products.type_product_id', '=', 'tbl_type_products.id')
            ->join('tbl_presentation_products', 'tbl_products.presentation_id', 'tbl_presentation_products.id')
            ->join('tbl_measure_units', 'tbl_products.measure_unit_id', '=', 'tbl_measure_units.id')
            ->join('tbl_taxes', 'tbl_products.tax_id', '=', 'tbl_taxes.id')
            ->get();

        if (count($products) == 0) {
            return response()->json([
                "ok" => false,
                "message" => 'No hay proveedores registrados.'
            ]);
        } else {
            return response()->json([
                "ok" => true,
                "data" => $products,
            ]);
        }
    }
    public function search($value)
    {
        // $products = ProductsXContract::where('name','LIKE','%'.$value.'%')->with('product')->get();

        $products = ProductsXContract::select('tbl_products.name as name', 'tbl_productsXcontracts.id as product_id', 'tbl_productsXcontracts.total_with_tax as total_with_tax', 'tbl_products.measure_unit_id as measure_unit_id')
            ->join('tbl_products', 'tbl_products.id', '=', 'tbl_productsXcontracts.pruduct_id')
            ->join('tbl_contracts', 'tbl_contracts.id', '=', 'tbl_productsXcontracts.contract_id')
            // ->join('tbl_measure_units','tbl_measure_units.id','tbl_products.measure_unit_id')
            ->where('tbl_products.name', 'LIKE', '%' . $value . '%')
            ->where('tbl_contracts.status', 0)
            ->take(5)
            ->get();

        if (count($products) == 0) {
            return response()->json([
                'ok' => false,
                'message' => 'No se encontraron productos que coincidan'
            ]);
        } else {
            return response()->json([
                'ok' => true,
                'data' => $products
            ]);
        }
    }


    public function objProduct($id)
    {

        $product = product::find($id)->with('tax', 'typeProduct', 'productsXcontracts')->first();
        $ProductContract = ProductsXContract::where('pruduct_id', $id)->where('status', 0)->first();


        return response()->json([
            'ok' => true,
            'data' => $product,
            'contract' => $ProductContract
        ]);
    }
}
