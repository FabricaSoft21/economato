<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Image;
use File;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        return view('users.perfil', compact('user'));
    }


    public function data()
    {
        try {
            $user = User::select("users.*", "roles.id as role_id", "roles.name as role_name")
                ->join("model_has_roles", "users.id", "=", "model_has_roles.model_id")
                ->join("roles", "roles.id", "=", "model_has_roles.role_id")
                ->get();

            return response()->json([
                "ok" => true,
                "data" => $user
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "ok" => false,
                "data" => $th
            ]);
        }
    }

    public function manage()
    {


        return view('users.index');
    }


    public function saveUser(Request $request)
    {




        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'lastname' => 'required',
            'rol_id' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
            'identification' => 'required|unique:users|max:15',
            'phone' => 'max:15'
        ])->setAttributeNames([
            'rol_id' => 'rol',
            'name' => 'nombres',
            'lastname' => 'apellidos',
            'email' => 'correo electrónico',
            'password' => 'contraseña',
            'identification' => 'número de documento',
            'phone' => 'número de teléfono'

        ]);


        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages()
            ]);
        } else {
            try {


                $input['password'] =   bcrypt($input['password']);

                $user = User::create($input);

                $user->assignRole($input['rol_id']);
                return response()->json([
                    'ok' => true,
                    'message' => 'Se registró el usuario correctamente',
                ]);
            } catch (\Throwable $ex) {
                return response()->json([
                    'ok' => false,
                    "error" => $ex->getMessage()
                ]);
            }
        }
    }




    // Guardar foto de perfil del usuario
    public function store(Request $request)
    {
        $campos = [

            "foto" => "required|mimes:jpg,jpeg,png,svg,gif"
        ];

        $this->validate($request, $campos);

        if ($request->hasFile('foto')) {

            $foto = $request->file('foto');
            $filename = time() . '.' . $foto->getClientOriginalExtension();
            Image::make($foto)->resize(300, 300)->save(public_path('/uploads/perfil/' . $filename));
            $user = Auth::user();
            File::delete('uploads/perfil/' . $user->foto);

            $user->foto = $filename;
            $user->save();
        }
        return redirect('perfil');
    }




    public function update(Request $request, $id)
    {
        //
    }


    public function destroy(Request $request)
    {
        $id = $request->id;
        $user = User::find($id);

        $user->update([
            'status' => $user->status == 1 ? 0 : 1
        ]);

        return response()->json([
            'ok' => true,
            'message' => 'se cambio el estado'
        ]);
    }

    public function changePassword(Request $request)
    {


        $user = \Auth::user();

        $this->validate($request, [
            'contraseña_actual' => 'required',
            'nueva_contraseña'  => 'required|min:8',
            'confirmacion_contraseña' => 'required|same:nueva_contraseña'
        ]);


        $contraseña_actual = $user->password;
        $contraseña_actual_input = $request->input('contraseña_actual');

        if (Hash::check($contraseña_actual_input, $contraseña_actual)) {

            $user->password = Hash::make($request->input('nueva_contraseña'));
            $user->update();

            // return view('modules.setting.config');  
            return back();
        } else {

            // return view('modules.setting.config'); 
            return back();
        }
    }

    public function getNotifications()
    {
        $id = Auth::user()->id;
        $notifications = Notification::where('user_id', $id)->where('state', 0)->get();

        if (count($notifications) > 0) {
            return response()->json([
                'ok' => true,
                'data' => $notifications
            ]);
        } else {
            return response()->json([
                'ok' => false,
                'data' => 'No hay notificaciones'
            ]);
        }
    }

    public function viewAllNotifications()
    {
        $id = Auth::user()->id;
        Notification::where('user_id', $id)->where('state', 0)->update(['state' => 1]);
    }
}
