<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Characterization;
use Validator;

class CharacterizationController extends Controller
{

    public function index()
    {

        $charact = Characterization::all();

        return view('modules.characterization.index', compact('charact'));
    }



    public function data()
    {
        $charact = Characterization::all();

        if ($charact == null) {
            return response()->json([
                "ok" => false,
                "message" => "No se ha encontrado ninguna caracterizacion registrada."
            ]);
        } else {
            return response()->json([
                "ok" => true,
                "data" => $charact,
            ]);
        }
    }




    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required|unique:tbl_characterizations',
        ])->setAttributeNames([
            'name' => 'nombre',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages()
            ]);
        } else {
            try {
                Characterization::create($input);
                return response()->json([
                    'ok' => true,
                    'message' => 'caracterizacion registrada correctamente',
                ]);
            } catch (\Throwable $ex) {
                return response()->json([
                    'ok' => false,
                    "error" => $ex->getMessage()
                ]);
            }
        }
    }


    public function show($id)
    {
        $charact = Characterization::find($id);

        if ($charact == null) {
            return response()->json([
                "ok" => false,
                "message" => "No se encontrado información de la caracterización.",
            ]);
        } else {
            return response()->json([
                "ok" => true,
                "data" => $charact,
            ]);
        }
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request)
    {
        $input = $request->all();
        $id = $request->id;

        $validator = Validator::make($input, [
            'name' => 'required|unique:tbl_characterizations,name,'.$id,
        ])->setAttributeNames([
            'name' => 'nombre',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages()
            ]);
        } else {
            try {
               $charact = Characterization::find($id);
               $charact->update($input);
                return response()->json([
                    'ok' => true,
                    'message' => 'caracterizacion actualizada correctamente',
                ]);
            } catch (\Throwable $ex) {
                return response()->json([
                    'ok' => false,
                    "error" => $ex->getMessage()
                ]);
            }
        }
    }


    public function destroy(Request $request)
    {
        $id = $request->id;
        $charact = Characterization::find($id);

        try {
            $charact->update([
                "status" => $charact->status == 1 ? 0 : 1
            ]);

            return response()->json([
                "ok" => true,
                "message" => "se cambio el estado"
            ]);
        } catch (\Throwable $th) {

            return response()->json([
                "ok" => false,
                "error" =>  $th
            ]);
        }
    }
}
