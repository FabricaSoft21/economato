<?php

namespace App\Http\Controllers;

use App\Models\Location;
use Illuminate\Http\Request;
use App\Models\Center;
use App\Models\CenterXprogram;
use App\Models\Complex;
use DB;
use Validator;

class CenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $complex = Complex::all();
        $center = Center::all();
        return view('modules.center.index', compact('center', 'complex'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'complex_id' => 'required',
            'address' => 'required'
        ])->setAttributeNames([
            'name' => 'nombre',
            'complex_id' => 'complejo',
            'address' => 'dirección',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages()
            ]);
        } else {
            try {
                $center = Center::create($input);
                $programs = $input['programs'];

                foreach ($programs as $p) {
                    $centerXprogram = new CenterXprogram();
                    $centerXprogram->center_id=$center->id;
                    $centerXprogram->program_id=$p['id'];
                    $centerXprogram->save();
                }
                return response()->json([
                    'ok' => true,
                    'message' => 'centro registrado correctamente',
                ]);
            } catch (\Throwable $ex) {
                return response()->json([
                    'ok' => false,
                    "error" => $ex->getMessage()
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $center = Center::where('id',$id)->with('complex')->first();
        $programs = CenterXprogram::where('center_id',$id)->with('program')->get();
        $complex = Complex::with('region')->get();
        if($center==null){
            return response()->json([
                'ok'=>false,
                'message'=>'No se encontró el centro'
            ]);
        }else {
            return response()->json([
                'ok'=>true,
                'data'=>$center,
                'complex'=>$complex,
                'programs'=>$programs
            ]);
        }
    }
    public function data()
    {
        $center = Center::all();

        $center = DB::table('tbl_centers')
            ->select(
                'tbl_centers.id as id',
                'tbl_centers.name as name_center',
                'tbl_centers.address as address',
                'tbl_regions.name as name_region',
                'tbl_complexes.name as name_complex',
                'tbl_centers.status as status'
            )
            ->join('tbl_complexes',  'tbl_centers.complex_id', '=', 'tbl_complexes.id')
            ->join('tbl_regions', 'tbl_complexes.region_id', '=', 'tbl_regions.id')
            ->get();

        if (count($center) == 0) {
            return response()->json([
                'ok' => false,
                'message' => 'No hay centros registrados'
            ]);
        } else {
            return response()->json([
                'ok' => true,
                'data' => $center,
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'complex_id' => 'required',
            'address' => 'required'
        ])->setAttributeNames([
            'name' => 'nombre',
            'complex_id' => 'complejo',
            'address' => 'dirección',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages()
            ]);
        }else {
            try {
                $center = Center::find($id);
                $center->update($input);

                $programs = $input['programs'];
                $programsLasts = CenterXprogram::where('center_id',$id)->get();
                foreach ($programsLasts as $p) {
                    $p->delete();
                }
                
                foreach ($programs as $p) {
                    $centerXprogram = new CenterXprogram();
                    $centerXprogram->center_id=$center->id;
                    $centerXprogram->program_id=$p['id'];
                    $centerXprogram->save();
                }
                return response()->json([
                    'ok' => true,
                    'message' => 'Programa actualizado',
                ]);
            } catch (\Throwable $ex) {
                return response()->json([
                    'ok' => false,
                    "error" => $ex->getMessage()
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $center = Center::find($id);
        $center->update([
            'status' => $center->status == 1 ? 0 : 1
        ]);

        return response()->json([
            'ok' => true,
            'message' => 'Se cambio el estado con éxito'
        ]);
    }
}
