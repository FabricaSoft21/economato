<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    public function logout(Request $request)
    {

        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('login');
    }
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        //consultamos el usuario
        $user = User::where($this->username(), $request->email)->first();
        //validamos si existe el usuario
        if ($user != null) {
            //validamos el estado
            if ($user->status == 1) {
                // si el estado es inactivo retornamos lo valores modificados para generar el error de logueo y entrar en el método  sendFailedLoginResponse
                return [$this->username() => 'inactive', 'password' => 'Tu cuenta esta actualmente inactiva, comunicate con el administrador'];
            } else {
                return [$this->username() => $request->email, 'password' => $request->password, 'status' => 0];
            }
        }
        return $request->only($this->username(), 'password');
    }
    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */





    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        //obtenemos los valores del método credencials y validamos el tipo de error
        $fields = $this->credentials($request);

        if ($fields[$this->username()] == 'inactive') {
            //mostramos el mensaje de cuenta inactiva 
            throw ValidationException::withMessages([
                $this->username() => $fields['password'],
            ]);
        } else {
            //errores de logueo por defecto 
            throw ValidationException::withMessages([
                $this->username() => [trans('auth.failed')],
            ]);
        }
    }
}
