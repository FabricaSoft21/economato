<?php

namespace App\Http\Controllers;

use App\Models\Contract;
use Illuminate\Http\Request;
use App\Models\ProductsXContract;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ContractController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        //especificar el permiso que solo va tener acceso y a que metodos en especifico
        $this->middleware(['permission:all contracts'], ['only' => 'index']);
        $this->middleware(['permission:create contracts'], ['only' => 'create']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contracts = Contract::select('tbl_contracts.*', 'tbl_providers.name as name_provider', 'tbl_providers.last_name')
            ->join('tbl_providers', 'tbl_providers.id', '=', 'tbl_contracts.provider_id')->get();

        $countVigente =  Contract::where('status', 0)->count();

        return view('modules.contract.index')->with(compact('contracts', 'countVigente'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countVigente =  Contract::where('status', 0)->count();
        if ($countVigente > 0) {

            return back()->with(compact('countVigente'));
        } else {
            return view('modules.contract.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $provider = json_decode($request->get('Provider'));
        $products = json_decode($request->get('Products'));
        try {
            //subimos el archivo 
            $file = $request->file('file');
            $ruta = public_path() . '/uploads/contracts/';
            $fileName = sha1(date('YmdHis')) . "." . $file->getClientOriginalExtension();
            $file->move($ruta, $fileName);
            //agregamos el contrato
            $contract =  Contract::create([
                'number' => intval($request->get('num_contract')),
                'start_date' =>  $request->get('startDate'),
                'finis_date' =>  $request->get('endDate'),
                'contracts_url' => $fileName,
                'provider_id' => $provider->id,
                'user_id' => \Auth::user()->id
            ]);
            //agregamos el detalle
            foreach ($products as $product) {
                ProductsXContract::create([
                    'pruduct_id' => $product->id,
                    'contract_id' => $contract->id,
                    'unit_price' => $product->unit_price,
                    'total_with_tax' => $product->unit_price + $product->totalIva,
                    'tax_value' => $product->totalIva
                ]);
            }
            return response()->json([
                'status' => true
            ]);
        } catch (\Throwable $th) {

            return response()->json([
                'status' => false,
                'error' => $th->getMessage()
            ]);
        }
    }


    public function showProducts($id)
    {
        try {
            $products =  ProductsXContract::Select(
                'tbl_products.name as product_name',
                'tbl_productsxcontracts.unit_price',
                'tbl_productsxcontracts.tax_value',
                'tbl_productsxcontracts.total_with_tax',
                'tbl_taxes.tax as IVA',
                'tbl_type_products.name as category',
                'tbl_presentation_products.name as presentation',
                'tbl_measure_units.name as measure'
            )
                ->join('tbl_products', 'tbl_products.id', '=', 'tbl_productsxcontracts.pruduct_id')
                ->join('tbl_taxes', 'tbl_taxes.id', '=', 'tbl_products.tax_id')
                ->join('tbl_type_products', 'tbl_products.type_product_id', '=', 'tbl_type_products.id')
                ->join('tbl_presentation_products', 'tbl_products.presentation_id', '=', 'tbl_presentation_products.id')
                ->join('tbl_measure_units', 'tbl_products.measure_unit_id', '=', 'tbl_measure_units.id')
                ->where('tbl_productsxcontracts.contract_id', $id)->get();

            return response()->json([
                'data' => $products
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function changeStatus()
    {
        try {
            $contract = Contract::where('status', 0)->first();

            $date = Carbon::now()->toDateString();
            $result =   DB::table('tbl_contracts')
                ->where('finis_date', '<=', $date)
                ->update(['tbl_contracts.status' => 1]);

                if ($result > 0) {
                    $productsXcontract = ($contract->productsXcontracts);
                
                    foreach($productsXcontract as $productContract){

                        $productContract->status = 1;
                        $productContract->update();

                    }

                }

            return response()->json([
                'status' =>  true,
                'data' => $result
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' =>  false,
                'error' => $th->getMessage()
            ]);
        }
    }
}
