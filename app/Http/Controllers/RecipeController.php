<?php

namespace App\Http\Controllers;

use App\Models\Recipe;
use Illuminate\Http\Request;
use Image;
use App\Models\Product;
use App\Models\RecipesXproduct;
use File;
use Validator;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $n_recetas = count(Recipe::all());
        $recipes = Recipe::orderBy('id', 'desc')->get();
            return view("modules.recipes.index",['n_recetas' => $n_recetas, 'recipes' => $recipes]);

    }


    public function dataRecipes($search = null){

        
        if(!empty($search)){

            $recipes = Recipe::where('recipe_name', 'LIKE', '%'.$search.'%')
            ->orWhere('description', 'LIKE', '%'.$search.'%')
            ->orderBy('id', 'desc')
            ->with('recipesXproducts')
            ->get();

        }else{

            $recipes = Recipe::orderBy('id', 'desc')->with('recipesXproducts')->get();
           
        }

        return response()->json([
            'ok' => true,
            'data' => $recipes,
        ]);

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $atributes = ['recipe_name' => 'nombre de la receta', 'description' => 'descripcion', 'recipe_image' => 'imagen'];

        $validate = $this->validate($request,[
            'recipe_name' => ['required', 'string', 'max:70', 'min:2'],
            'description' => ['string', 'max:200'],
            'recipe_image' => ['image']
        ], [], $atributes); 

        $recipe_name = $request->input('recipe_name');
        $description = $request->input('description');
        $recipe_image = $request->file('recipe_image');



        $recipe = new Recipe();

        $recipe->recipe_name = $recipe_name;
        $recipe->description = $description;
        $recipe->status = 0;

        if($recipe_image){
            // Poner nombre unico.
            $image_recipe_name = time().$recipe_image->getClientOriginalName();
            // Guardar en la carpeta storage (stoarage/app/users)
            Image::make($recipe_image)->resize(300,300)->save(public_path('/uploads/recipes/'.$image_recipe_name));
        
            // Seteo el nombre de la imagen en el objeto
            $recipe->image = $image_recipe_name;
        }


        $recipe->save();


        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function show(Recipe $recipe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $recipe = Recipe::find($id);

        $products = $this->products();

        return view('modules.recipes.edit', ['recipe' => $recipe, 'products' => $products]);
    }


    public function products($product_search = null){


       

        if(!empty($product_search)){

            $products = Product::where('name', 'LIKE', '%'.$product_search.'%')
            ->orderBy('id', 'desc')
            ->with('typeProduct', 'presentationProduct', 'measureUnit')
            ->get();
        }else{

            $products = Product::orderBy('id', 'desc')->with('typeProduct', 'presentationProduct', 'measureUnit')->get();
           
        }

        return response()->json([
            'ok' => true,
            'data' => $products,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //

    
        $atributes = ['recipe_name' => 'nombre de la receta', 'description' => 'descripcion', 'recipe_image' => 'imagen'];


        $validate = $this->validate($request,[
            'recipe_name' => ['required', 'string', 'max:70', 'min:2'],
            'description' => ['string', 'max:200'],
            'recipe_image' => ['image'],
            'recipe' => ['required']
        ], [], $atributes); 


            $recipe_id = $request->input('recipe');
            $recipe_name = $request->input('recipe_name');
            $description = $request->input('description');
            $recipe_image = $request->file('recipe_image');

            $recipe = Recipe::find($recipe_id);

            $recipe->recipe_name = $recipe_name;
            $recipe->description = $description;
            $recipe->status = 0;
    
            if($recipe_image){
                // Poner nombre unico.
                $image_recipe_name = time().$recipe_image->getClientOriginalName();
                // Guardar en la carpeta storage (stoarage/app/users)
                Image::make($recipe_image)->resize(300,300)->save(public_path('/uploads/recipes/'.$image_recipe_name));
            
                // Seteo el nombre de la imagen en el objeto
                $recipe->image = $image_recipe_name;
            }
    
    
            $recipe->update();

            return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recipe $recipe)
    {
        //
    }

    public function addProduct(Request $request){


        $input = $request->all();


        $validator = Validator::make($input, [
            'idProduct' => 'required',
            'idRecipe' => 'required',
            'product_quantity' => 'required',
        ])->setAttributeNames([
            'idProduct' => 'producto',
            'idRecipe' => 'receta',
            'product_quantity' => 'cantidad'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages()
            ]);
        } else {
            try {
                
                $recipeXproduct = new RecipesXproduct();

                $recipeXproduct->recipe_id = $input['idRecipe'];
                $recipeXproduct->product_id = $input['idProduct'];
                $recipeXproduct->quantity = $input['product_quantity'];

                $recipeXproduct->save();

                return response()->json([
                    'ok' => true,
                    'message' => 'Producto añadido a la receta correctamente.',
                ]);
            } catch (\Throwable $ex) {
                return response()->json([
                    'ok' => false,
                    "error" => $ex->getMessage()
                ]);
            }
        }

        return response()->json([
            'ok' => true,
            'message' => 'Se registró correctamente',
        ]);
       
    }

    public function removeProduct(Request $request){

        $input = $request->all();
        
            $validator = Validator::make($input, [
                'product_quantity_remove' => 'required',
                'idRecipeXProduct' => 'required',
            ])->setAttributeNames([
                'product_quantity_remove' => 'cantidad a remover',
                'idRecipeXProduct' => 'producto de receta',
            ]);
            if ($validator->fails()) {
                return response()->json([
                    'ok' => false,
                    'message' => $validator->messages()
                ]);
            } else {
                try {
                    
                    $product_quantity_remove = $input['product_quantity_remove'];
                    $idRecipeXProduct = $input['idRecipeXProduct'];
    
                    $recipeXproduct = RecipesXproduct::find($idRecipeXProduct);

                        if($recipeXproduct->quantity == $product_quantity_remove){
                                $recipeXproduct->delete();
                        }else{
                                $recipeXproduct->quantity =  ($recipeXproduct->quantity-$product_quantity_remove);
                                $recipeXproduct->update();
                        }

                
                    return response()->json([
                        'ok' => true,
                        'message' => 'Se realizo el ajuste correctamente.',
                    ]);

                } catch (\Throwable $ex) {
                    return response()->json([
                        'ok' => false,
                        "error" => $ex->getMessage()
                    ]);
                }
            }

    }

    public function listProducts($idRecipe){

            $recipe = Recipe::find($idRecipe);

            $products = [];
            
            foreach($recipe->recipesXproducts as $recipeXproduct){


                $products[] = RecipesXproduct::where('id', $recipeXproduct->id)->with('product')->first();

            }
            
            

        return response()->json([
            'ok' => true,
            'data' => $products,
        ]);

    }


    public function costProduction($idRecipe){


            $recipe = Recipe::find($idRecipe);

              $costProduction = null;


                foreach($recipe->recipesXproducts as $recipeXproduct){
                    if($recipeXproduct->productContract){
                        $costProduction =  $costProduction+($recipeXproduct->productContract->unit_price);
                    }
                }

                return response()->json([
                    'ok' => true,
                    'data' => $costProduction,
                ]);

    }


    public function recipeDelete($id){


            $recipe = Recipe::find($id);

                    foreach($recipe->recipesXproducts as $recipeXproduct){

                        $recipeXproduct->delete();

                    }

            /* $recipe->recipesXproducts()->delete(); */

            $recipe->delete();

            return redirect()->route('recipes.index');
    }


    public function recipeProducts($idRecipe){

        $products = RecipesXproduct::where('recipe_id', $idRecipe)->with('productContract', 'product')->get();


        return response()->json([
            'ok' => true,
            'data' => $products,
        ]);

    }


}
