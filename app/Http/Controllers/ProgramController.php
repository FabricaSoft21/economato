<?php

namespace App\Http\Controllers;

use App\Models\Program;
use Illuminate\Http\Request;
use Validator;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');

    }
    public function index()
    {
        return view('modules.program.index');
    }

    public function data(){
        $programs = Program::all();

        if (count($programs) == 0) {
            return response()->json([
                "ok" => false,
                "message" => 'No hay programas registrados.'
            ]);
        } else {
            return response()->json([
                "ok" => true,
                "data" => $programs
            ]);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => ['required', 'string', 'unique:tbl_providers','max:150'],            
            'version' => ['required', 'string', 'max:20'],
            'description' => ['required', 'string', 'max:600'],
        ])->setAttributeNames([
            'name' => 'nombre',
            'version' => 'versión',
            'description' => 'descripción',    
        ]);

        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages()
            ]);
        }

        try {
            Program::create($input);

            return response()->json([
                "ok" => true,
                "message" => "Programa registrado con exito."
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "ok" => false,
                "error" => $ex->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $program = Program::find($id);

        if ($program==null) {
            return response()->json([
                'ok'=>true,
                'message'=>'No se encontro ningún programa.'
            ]);
        } else {
            return response()->json([
                'ok'=>true,
                'data'=>$program
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function edit(Program $program)
    {
        //
    }
    public function search($value)
    {
        // dd($value);
        $programs = Program::where('name','LIKE','%'.$value.'%')->get();

        if(count($programs)==0){
            return response()->json([
                'ok'=>false,
                'message'=>'No se encontraron programas que coincidan'
            ]);
        }else{
            return response()->json([
                'ok'=>true,
                'data'=>$programs
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => ['required', 'string','max:150'],            
            'version' => ['required', 'string', 'max:20'],
            'description' => ['required', 'string', 'max:600'],
        ])->setAttributeNames([
            'name' => 'nombre',
            'version' => 'versión',
            'description' => 'descripción',   
        ]);
        if ($validator->fails()) {
            return response()->json([
                'ok' => false,
                'message' => $validator->messages()
            ]);
        }else {
            try {
                $program = Program::find($id);
                $program->update($input);

                return response()->json([
                    'ok' => true,
                    'message' => 'Programa actualizado',
                ]);
            } catch (\Throwable $ex) {
                return response()->json([
                    'ok' => false,
                    "error" => $ex->getMessage()
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $program = Program::find($id);

        
        $program->update([
            'status' => $program->status == 1 ? 0 : 1
        ]);

        return response()->json([
            'ok' => true,
            'message' => 'Se cambio el estado con éxito'
        ]);
    }
}
