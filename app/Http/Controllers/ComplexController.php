<?php

namespace App\Http\Controllers;

use App\Models\Complex;
use App\Models\Region;
use Illuminate\Http\Request;
use Validator;

class ComplexController extends Controller
{

    public function index()
    {
        $complex =  Complex::with("region")->get();
        $region = Region::all();

        return view("modules.complex.index", compact("complex", "region"));
    }




    public function data()
    {
        $complex =  Complex::with("region")->get();


        if ($complex == null) {
            return response()->json([
                "ok" => false,
                "message" => "No se encuentra o no existe información de los complejos registrados"
            ]);
        } else {
            return response()->json([

                "ok" => true,
                "data" => $complex,

            ]);
        }
    }



    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            "name" => "required",
            "region_id" => "required"
        ])->setAttributeNames([
            "name" => "nombre del complejo",
            "region_id" => "región"
        ]);


        if ($validator->fails()) {
            return response()->json([
                "ok" => false,
                "message" => $validator->messages()
            ]);
        } else {
            $validate = Complex::where('region_id', $input['region_id'])->get();

            foreach ($validate as $v) {
                if (strtoupper($v->name) == strtoupper($input['name'])) {
                    return response()->json([
                        "ok" => false,
                        "exist" => 'La regional ' . $v->region->name . ' ya tiene un complejo con este nombre'
                    ]);
                }
            }

            try {
                Complex::create($input);
                return response()->json([
                    "ok" => true,
                    "message" => "Complejo registrado correctamente",
                ]);
            } catch (\Throwable $ex) {
                return response()->json([
                    "ok" => false,
                    "error" => $ex->getMessage()
                ]);
            }
        }
    }




    public function show($id)
    {
        $complex = Complex::with('region')->where('id', $id)->first();

        if ($complex == null) {

            return response()->json([
                "ok" => false,
                "message" => "No se encontrado información del complejo."
            ]);
        } else {
            return response()->json([
                "ok" => true,
                "data" => $complex,

            ]);
        }
    }




    public function edit(Complex $complex)
    {
        //
    }




    public function update(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            "name" => "required",
            "region_id" => "required"
        ])->setAttributeNames([
            "name" => "nombre del complejo",
            "region_id" => "región"
        ]);


        if ($validator->fails()) {
            return response()->json([
                "ok" => false,
                "message" => $validator->messages()
            ]);
        } else {
            $validate = Complex::where('region_id', $input['region_id'])->get();

           
            try {
                $id = $request->id;
                $complex = Complex::find($id);
                $complex->update($input);

                return response()->json([
                    "ok" => true,
                    "message" => "Complejo actualizado correctamente",
                ]);
            } catch (\Throwable $ex) {
                return response()->json([
                    "ok" => false,
                    "error" => $ex->getMessage()
                ]);
            }
        }
    }




    public function destroy(Complex $complex)
    {
        //
    }
}
