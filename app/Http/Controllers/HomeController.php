<?php

namespace App\Http\Controllers;

use App\Models\Budget;
use DB;
use App\Models\Contract;
use App\Models\Product;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //especificar el permiso que solo va tener acceso y a que metodos en especifico
        // $this->middleware(['permission::all products'],['only' => 'index']);
        // $this->middleware(['permission::create products'],['only' => ['create','store']]);

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {


        $nUsers = DB::table('users')->count();
        $nRecipes = DB::table('tbl_recipes')->count();
        $nContracts = DB::table('tbl_contracts')->count();
        $nComplexes = DB::table('tbl_complexes')->count();
        $contractA = Contract::where('status', 0)->first();
        $products = Product::where('status', 1)->orderBy('updated_at', 'asc')->get();
        $budget = Budget::where('status', 0)->first();
        

        return view('dashboard2', [
            'nUsers' => $nUsers,
            'nRecipes' => $nRecipes,
            'nContracts' => $nContracts,
            'nComplexes' => $nComplexes,
            'contractA' => $contractA,
            'products' => $products,
            'budget' => $budget,
        ]);
    }
}
