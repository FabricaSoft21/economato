<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Characterization extends Model
{
    
    public $table = "tbl_characterizations";

    protected $fillable = [
        'name',
        'status',
    ];


    public function files(){
        return $this->hasMany('App\Models\File');
    }


    public function budgetsXcharacterizations(){
        return $this->hasMany('App\Models\BudgetXCharacterization');
    }
  
}
