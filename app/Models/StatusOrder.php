<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusOrder extends Model
{
    
    public $table = "tbl_status_orders";

    protected $fillable = [
        'name',
    ];

    public function centersProductionOders(){
        return $this->hasMany('App\Models\CenterProductionOrder', 'status_order_id');
    }

    public function oders(){
        return $this->hasMany('App\Models\Order', 'status_order_id');
    }


}
