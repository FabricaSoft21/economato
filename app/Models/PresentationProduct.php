<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PresentationProduct extends Model
{
    public $table = "tbl_presentation_products";

    protected $fillable = [
        'name',
    ];

    public function products(){
      return $this->hasMany('App\Models\Product');  
    }

}
