<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Center extends Model
{
    public $table = "tbl_centers";

    protected $fillable = [
        'name',
        'complex_id',
        'status',
        'address',
    ];

    public function complex(){
        return $this->belongsTo('App\Models\Complex', 'complex_id');
    }

    public function centerXprograms(){
        return $this->hasMany('App\Models\CenterXprogram');
    }

    public function centersProductionsOrders(){
        return $this->hasMany('App\Models\CenterProductionOrder');
    }

    public function files(){
        return $this->hasMany('App\Models\File');
    }

}
