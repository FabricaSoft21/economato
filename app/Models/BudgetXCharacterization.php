<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BudgetXCharacterization extends Model
{
    public $table = "tbl_budgetXcharacterizations";

    protected $fillable  =[
      'budget_id',
      'characterization_id',
      'porcentage',
      'value',
    ];

    public function budget(){
      return $this->belongsTo('App\Models\Budget', 'budget_id');
    }

    public function characterization(){
      return $this->belongsTo('App\Models\Characterization', 'characterization_id');
    }

}
