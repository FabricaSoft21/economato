<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    public $table = "tbl_files";

    protected $fillable = [
        'number',
        'apprentices',
        'start_date',
        'finish_date',
        'status',
        'program_id',
        'characterization_id',
        'center_id'
    ];


    public function characterization()
    {
        return $this->belongsTo('App\Models\Characterization', 'characterization_id');
    }

    public function program()
    {
        return $this->belongsTo('App\Models\Program');
    }

    public function center()
    {
        return $this->belongsTo('App\Models\Center');
    }

    public function centersXfiles()
    {
        return $this->hasMany('App\Models\CenterXfiles');
    }

    public function usersXfiles()
    {
        return $this->hasMany('App\Models\UserXfile');
    }

    public function order()
    {
        return $this->hasMany('App\Models\Order');
    }
}
