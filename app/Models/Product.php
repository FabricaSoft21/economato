<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $table = "tbl_products";

    protected $fillable = [
        'name',
        'status',
        'type_product_id',
        'presentation_id',
        'measure_unit_id',
        'tax_id',
    ];


    public function ordersXproducts(){
        return $this->hasMany('App\Models\OrdersXProduct');
    }

    public function recipesXproducts(){
        return $this->hasMany('App\Models\RecipesXproduct');
    }


    public function productsXcontracts(){
        return $this->hasMany('App\Models\ProductsXContract', 'pruduct_id');
    }

    public function centersXproducts(){
        return $this->hasMany('App\Models\CenterXProduct');
    }


    public function measureUnit(){
        return $this->belongsTo('App\Models\MeasureUnits', 'measure_unit_id');
    }

    public function presentationProduct(){
        return $this->belongsTo('App\Models\PresentationProduct', 'presentation_id');
    }

    public function typeProduct(){
        return $this->belongsTo('App\Models\TypeProduct');
    }
    public function tax()
    {
        return $this->belongsTo('App\Models\Tax');
    }

   



}
