<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    public $table = "tbl_taxes";

    protected $fillable = [
        'tax',
    ];

    public function productsXcontracts(){
        return $this->hasMany('App\Models\ProductsXContracts');
    }
    public function products(){
        return $this->hasMany('App\Models\Product');
    }


}
