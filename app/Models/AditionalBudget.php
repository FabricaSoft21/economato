<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AditionalBudget extends Model
{
    public $table = "tbl_aditional_budgets";

    protected $fillable  =[
      'budget_id',
      'aditional_budget',
      'aditional_budget_code',
      'aditional_begin_date',
      'aditional_finish_date',
      'status',
    ];


    public function budget(){
      return $this->belongsTo('App\Models\Budget', 'budget_id');
    }

   
}
