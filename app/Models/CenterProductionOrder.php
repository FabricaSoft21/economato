<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CenterProductionOrder extends Model
{
    public $table = "tbl_center_production_orders";

    protected $fillable  =[
      'descripcion',
      'quantity_people',
      'order_date',
      'title',
      'cost',
      'user_id',
      'center_id',
      'status_id',
    ];

    public function centersXproducts(){
      return $this->hasMany('App\Models\CenterXProduct');
    }

    public function centersXfiles(){
      return $this->hasMany('App\Models\CenterXfiles');
    }

    public function user(){
      return $this->belongsTo('App\User', 'user_id');
    }

    public function statusOrder(){
      return $this->belongsTo('App\Models\StatusOrder', 'status_id');
    }

    public function center(){
      return $this->belongsTo('App\Models\center', 'center_id');
    }


}
