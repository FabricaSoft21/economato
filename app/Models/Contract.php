<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    public $table = "tbl_contracts";

    protected $fillable = [
        'number',
        'price',
        'start_date',
        'finis_date',
        'contracts_url',
        'provider_id',
        'user_id'
    ];


    public function productsXcontracts()
    {
        return $this->hasMany('App\Models\ProductsXContract');
    }

    public function provider()
    {
        return $this->belongsTo('App\Models\Provider');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function getUrlPdfAttribute()
    {
        return  \Storage::url($this->contracts_url);
    }
}
