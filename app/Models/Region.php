<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public $table = "tbl_regions";

    protected $fillable = [
        'name',
    ];

    public function complexes(){
        return $this->hasMany('App\Models\Complex');
    }

}
