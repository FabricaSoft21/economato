<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CenterXfiles extends Model
{
   
    public $table = "tbl_centerXfiles";

    protected $fillable = [
        'center_production_order_id',
        'file_id'
    ];

    public function centerProductionOrder(){
        return $this->belongsTo('App\Models\CenterProduction', 'center_production_order_id');
    }

    public function file(){
        return $this->belongsTo('App\Models\File', 'file_id');
    }

}
