<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CenterXProduct extends Model
{
    public $table = "tbl_centerXproducts";

    protected $fillable = [
        'center_production_order_id',
        'product_id',
        'quantity',
    ];

    public function centerProductionOrder(){
        return $this->belongsTo('App\Models\CenterProductionOrder', 'center_production_order_id');
    }

    public function product(){
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

}
