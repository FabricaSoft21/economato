<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecipesXproduct extends Model
{
    public $table = "tbl_recipes_xproducts";

    protected $fillable = [
        'recipe_id',
        'product_id',
        'quantity',
    ];


    public function product(){
        return $this->belongsTo('App\Models\Product', 'product_id')->with('measureUnit');
    }

    public function recipe(){
        return $this->belongsTo('App\Models\Recipe', 'recipe_id');
    }

    public function productContract(){
        return $this->belongsTo('App\Models\ProductsXContract', 'product_id', 'pruduct_id')->where('status',0)->with('product');
    }

}
