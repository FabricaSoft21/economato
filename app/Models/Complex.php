<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Complex extends Model
{
    public $table = "tbl_complexes";

    protected $fillable = [
        'name',
        'region_id',
    ];

    public function locations(){
        return $this->hasMany('App\Models\Location');
    }

    public function region(){
        return $this->belongsTo('App\Models\Region', 'region_id');
    }

}
