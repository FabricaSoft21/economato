<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    public $table = "tbl_orders";

    protected $fillable = [
           'file_id',
           'user_id',
           'description',
           'status_id',
           'cost',
    ];
    
        
    public function ordersXproducts(){
        return $this->hasMany('App\Models\OrdersXProduct');
    }
    
    public function statusOrder(){
        return $this->belongsTo('App\Models\StatusOrder', 'status_order_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function file(){
        return $this->belongsTo('App\Models\File','file_id');
    }

}
