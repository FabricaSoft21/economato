<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeDocument extends Model
{
    public $table = 'tbl_type_documents';

    protected $fillable = [
        'name'
    ];
}
