<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MeasureUnits extends Model
{
    
    public $table = "tbl_measure_units";

    protected $fillable = [
        'name',
    ];

    public function products(){
        return $this->hasMany('App\Models\Product');
    }

    


}
