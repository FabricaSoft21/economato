<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrdersXproduct extends Model
{
    public $table = "tbl_ordersXproducts";

    protected $fillable = [
        'order_id',
        'product_id',
        'quantity',
        'subtotal'
    ];

    public function order(){
        return $this->belongsTo('App\Models\Order', 'order_id');
    }

    public function product(){
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function productXcontract(){
        return $this->belongsTo('App\Models\ProductsXContract', 'product_id')->with('product');
    }


}
