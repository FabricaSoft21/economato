<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    public $table = 'tbl_genders';

    protected $fillable = [
        'name'
    ];
}
