<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeProduct extends Model
{
    public $table = "tbl_type_products";

    protected $fillable = [
        'name',
    ];

    public function products(){
        return $this->hasMany('App\Models\Product');
    }


}
