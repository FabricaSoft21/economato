<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    public $table = "tbl_recipes";

    protected $fillable = [
        'recipe_name',
        'recipes_cost',
        'image',
        'description',
        'status',
    ];

    public function recipesXproducts(){
        return $this->hasMany('App\Models\RecipesXproduct');
    }


}
