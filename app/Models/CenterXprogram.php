<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CenterXprogram extends Model
{
    public $table = 'tbl_centerXprogram';

    protected $fillable = [
        'center_id',
        'program_id'
    ];


    public function center(){
        return $this->belongsTo('App\Models\Center');
    }

    public function program(){
        return $this->belongsTo('App\Models\Program');
    }

}
