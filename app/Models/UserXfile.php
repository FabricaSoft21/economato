<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserXfile extends Model
{
    public $table = "tbl_userXfiles";

    protected $fillable = [
        'user_id',
        'file_id',
    ];

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function file(){
        return $this->belongsTo('App\Models\File', 'file_id');
    }

}
