<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
    
    public $table = "tbl_budgets";

    protected $fillable  =[
      'initial_budget',
      'budget_code',
      'remaining',
      'budget_begin',
      'budget_finish',
      'status'
    ];

    
    public function aditionalBudgets(){
      return $this->hasMany('App\Models\AditionalBudget');
    }

    public function budgetsXcharacterizations(){
      return $this->hasMany('App\Models\BudgetXCharacterization');
    }

}
