<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsXContract extends Model
{

    public $table = "tbl_productsXcontracts";


    protected $fillable = [
        'pruduct_id',
        'contract_id',
        'tax_id',
        'quantity',
        'unit_price',
        'total_with_tax',
        'tax_value',
        'total',
        'quantity_agreed',
        'status',
    ];

    //

    public function product()
    {
        //return $this->belongsTo('App\Models\Product', 'pruduct_id')->with('tax', 'measureUnit');
        return $this->belongsTo('App\Models\Product', 'pruduct_id');
    }

    public function contract()
    {
        return $this->belongsTo('App\Models\Contract', 'contracts_id');
    }

    public function tax()
    {
        return $this->belongsTo('App\Models\tax', 'taxes_id');
    }
}
