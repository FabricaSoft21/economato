<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    public $table = "tbl_providers";

    protected $fillable = [
        'NIT',
        'name',
        'last_name',
        'phone',
        'email',
        'contact_name',
        'phone_contact',
        'status',
    ];

    public function contracts(){
        return $this->hasMany('App\Models\Contract');
    }



}
