<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    public $table = "tbl_programs";

    protected $fillable = [
        'name',
        'version',
        'description',
        'status',
    ];

    public function files(){
        return $this->hasMany('App\Models\File');
    }

    public function location(){
        return $this->belongsTo('App\Models\location', 'location_id');
    }

    public function centerXprograms(){
        return $this->hasMany('App\Models\CenterXprogram');
    }

}
