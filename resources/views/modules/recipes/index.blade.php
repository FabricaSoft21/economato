@extends('layouts.app')

@section('title')
Recipes
@parent
@stop
@section('topScripts')
<link href="{{asset('vendors/dropify/dist/css/dropify.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')

<link href="{{asset('css/recipes.css')}}" rel="stylesheet" type="text/css">
<div class="card">
    <div class="card-body">
        <div class="card-header">
            <h4>Recetas</h4>
        </div>
        <div class="card-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm">
                    
                                    <form action="{{ route('recipes.save') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="col-md-112">

                                            <input type="text" class="form-control {{ $errors->has('recipe_name') ? ' is-invalid' : '' }}" name="recipe_name" id="recipe_name" value="{{ old('recipe_name') }}" placeholder="Nombre de la receta">
                                            <label for="recipe_name"> Nombre de la receta (*) </label>
                                                @if($errors->has('recipe_name'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('recipe_name') }}</strong>
                                                            </span>
                                                @endif

                                            <textarea class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" rows="3" placeholder="Descripcion" name="description" id="description"  value="{{ old('description') }}"></textarea>
                                            <label for="description"> Descripcion de la receta </label>
                                                    @if($errors->has('description'))
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('description') }}</strong>
                                                                </span>
                                                    @endif
                                        </div>
                                        <div class="col-md-112">

                                            <input type="file" class="dropify {{ $errors->has('recipe_image') ? ' is-invalid' : '' }}"  name="recipe_image" id="recipe_image"  data-allowed-file-extensions = "jpg png" />
                                            @if($errors->has('recipe_image'))
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('recipe_image') }}</strong>
                                                                </span>
                                             @endif

                                        </div>
                                          
                                        <div class="col-md-112">

                                            <br>
                                            <br>

                                            <button type="submit" class="btn btn-primary btn-lg">Registrar receta</button>
                                            <br>

                                        </div>



                                    </form>

                            </div>
                          
                            <div class="col-sm">
                                <div style="height:400px; overflow:auto;">
                                <table class="table table-borderless table-hover">
                                    <thead>
                                        <tr>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">cantidad de productos</th>
                                        <th scope="col">Creación</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($recipes as $recipe)
                                        <tr>
                                        <th scope="row"><a href="{{route('recipes.edit', ['id' => $recipe->id])}}">{{$recipe->recipe_name}}</a></th>
                                        <td>{{count($recipe->recipesXproducts)}}</td>
                                        <td>{{\FormatTime::longTimeFilter($recipe->created_at)}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    </table>
                            </div>
                        </div>
                        </div>



                </div>
                <br>
                <br>
                <br>

                </div>

                <br>
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <section class="hk-sec-wrapper">
                                <h5 class="hk-sec-title">Consultar recetas registradas:</h5>
                                <form class="navbar-search-alt" id="searchRecipe">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><span class="feather-icon"><i data-feather="search"></i></span></span>
                                        </div>
                                        <input class="form-control" type="search" name="search" id="search" placeholder="recetas registradas ({{$n_recetas}})" aria-label="Search">
                                </form>
                            </section>
                        </div>
                    </div>  
                </div>
        </div>
    </div>
    <div class="container">
        <div class="row" id="card_recipes">
            
            
        </div>
    </div>
</div>

        
       
    




        @stop

@section('lowerScripts')
<script src="/lowerScripts/recipe.js"></script>
<script src="{{asset('vendors/dropify/dist/js/dropify.min.js')}}"></script>
<script>
$(document).ready(function () { 
    //manejo de archivos con dropify
    $('#recipe_image').dropify({
        messages: {
            'default': 'Arrastra y suelta una fotografia aquí o haz clic',
            'replace': 'Arrastra y suelta o haz clic para reemplazar',
            'remove': 'Remover',
            'error': 'Ooops, sucedió algo malo.'
        }
    });
});
$('#upload-photo').click(()=>{
    $('#upload-profile').submit();
});

</script>
@endsection
