@extends('layouts.app')

@section('title')
Recipes
@parent
@stop
@section('topScripts')
<link href="{{asset('vendors/dropify/dist/css/dropify.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')


<div class="card">
    <div class="card-body">



        <div class="card-header">
            <a href="{{route('recipes.index')}}">
            <h4><span class="badge badge-light"><- Regresar a las recetas</span></h4>
            </a>
        </div>

        <div class="card-body">
            <div class="container-fluid">

                    <div class="container">
                        <div class="row">
                            <div class="col-sm">

                                <form action="{{ route('recipes.update') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="">

                                            <label for="recipe_name"> Nombre de la receta (*) </label>
                                            <input type="text" class="form-control {{ $errors->has('recipe_name') ? ' is-invalid' : '' }}" name="recipe_name" id="recipe_name" value="{{$recipe->recipe_name}}">
                                            

                                                @if($errors->has('recipe_name'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('recipe_name') }}</strong>
                                                            </span>
                                                @endif

                                            <br>
                                            <label for="description"> Descripcion de la receta </label>
                                            <textarea class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" rows="3" placeholder="descripcion" name="description" id="description">{{$recipe->description}}</textarea>
                                            

                                                  @if($errors->has('description'))
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('description') }}</strong>
                                                                </span>
                                                    @endif




                                        </div>
                                        <div class="">
                                        <br>
                                        <input type="file" class="dropify {{ $errors->has('recipe_image') ? ' is-invalid' : '' }}"  name="recipe_image" id="recipe_image"  data-allowed-file-extensions = "jpg png" data-default-file="{{asset('uploads/recipes/'.$recipe->image)}}" />
                                            @if($errors->has('recipe_image'))
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('recipe_image') }}</strong>
                                                                </span>
                                             @endif
                                            <input type="hidden" class="form-control" name="recipe" id="recipe" value="{{$recipe->id}}" />

                                        </div>
                                        <div class="">

                                            <br>
                                            <br>

                                            <div style="width: 330px; margin: 0 auto; text-align:center">
                                                <button type="submit" class="btn btn-primary btn-lg">Modificar receta</button>
                                            </div>

                                        </div>

                                        

                                    </form>



                                    <br>
                                    <br>
                                    <a href="{{route('recipes.delete', ['id' => $recipe->id])}}" type="button" class="btn btn-danger btn-lg btn-block mt-20">Eliminar receta</a>


                                    <!-- Modal -->
                                    <div class="modal fade" id="ModalProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">Agregar producto</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body" >
                                        <div id="ModalProductBody">
    
                                        </div>

                                           
                                                
                                                <input type="number" class="form-control" name="product_quantity" id="product_quantity">
                                                <label for="product_quantity"> Cantidad en<span id="quantityMU" style="color:lightseagreen"> </span>(*)</label>

                                                <input type="hidden" class="form-control" name="idProduct" id="idProduct" value="">
                                                <input type="hidden" class="form-control" name="idRecipe" id="idRecipe" value="{{$recipe->id}}">


                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                            <button type="button" class="btn btn-primary" onclick="addProduct()">Añadir</button>
                                            
                                        </div>
                                        </div>
                                    </div>
                                    </div>



                            </div>
                            <div class="col-sm">
                            <h4><p class="text-center">Productos asociados</p></h4>

                                <div id="listProducts">

                                </div>


                            </div>
                            <div class="col-sm">
                            <form class="navbar-search-alt" id="searchProduct">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><span class="feather-icon"><i data-feather="search"></i></span></span>
                                </div>
                                <input class="form-control" type="search" name="search" id="search" placeholder="producto" aria-label="Search">
                                <input class="form-control" type="hidden" id="recipe_id" value="{{$recipe->id}}">
                            </div>
                            </form>

                           <div id="products" style="overflow-y:auto; height:400px;">

                           </div>

                <br>
                <br>
                <br>

            </div>
          
        </div>


                                    <div class="modal fade" id="modalRemoveProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalCenterTitle">Retirar producto</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                        
                                                        <div id="infoProductRemove">

                                                        </div>
                                                        <hr>

                                                        
                                                            <input type="number" class="form-control" name="product_quantity_remove" id="product_quantity_remove" value="">
                                                            <label for="product_quantity"> Cantidad a retirar(*) </label>

                                                            <input type="hidden" class="form-control" name="idRecipeXProduct" id="idRecipeXProduct" value="">


                                                        </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                        <button class="btn btn-primary" onclick="subtractProduct()">Retirar productos</button>
                                                    </div>
                                                    </div>
                                                </div>
                                        </div>



        @stop

        

@section('lowerScripts')
<script src="{{asset('vendors/dropify/dist/js/dropify.min.js')}}"></script>
<script src="/lowerScripts/recipe.js"></script>
<script>
$(document).ready(function () { 
    //manejo de archivos con dropify
    $('#recipe_image').dropify({
        messages: {
            'default': 'Arrastra y suelta una fotografia aquí o haz clic',
            'replace': 'Arrastra y suelta o haz clic para reemplazar',
            'remove': 'Remover',
            'error': 'Ooops, sucedió algo malo.'
        }
    });
});
$('#upload-photo').click(()=>{
    $('#upload-profile').submit();
});

</script>
@endsection