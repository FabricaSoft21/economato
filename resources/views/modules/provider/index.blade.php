@extends('layouts.app')
@section('title')
Proveedores
@parent
@stop
@section('topScripts')
<!-- Data Table CSS -->
<link href="{{asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Toastr CSS -->
<link href="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-refresh">
            <div class="refresh-container">
                <div class="loader-pendulums"></div>
            </div>
            <div class="card-header card-header-action">
                <h4>Proveedores</h4>
                <div class="d-flex align-items-center card-action-wrap">

                    <a class="inline-block mr-15" data-toggle="collapse" href="#collapse_1" aria-expanded="true">
                        <i class="zmdi zmdi-chevron-down"></i>
                    </a>
                    <a href="#" class="inline-block refresh mr-15" onclick="listProviders()">
                        <i class="ion ion-md-radio-button-off"></i>
                    </a>
                    <a href="#" class="inline-block full-screen mr-15">
                        <i class="ion ion-md-expand"></i>
                    </a>
                </div>
            </div>
            <div id="collapse_1" class="collapse show">
                <div class="card-body">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#register">
                        Nuevo &#32; <i class="fa fa-plus "></i>
                    </button>
                    <hr>
                    <div class="table-wrap">
                        <table id="tableProviders" class="table table-hover w-100 ">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>NIT</th>
                                    <th>Nombre</th>
                                    <th>Teléfono</th>
                                    <th>Correo</th>
                                    <th>Contacto</th>
                                    <th>Teléfono de contacto</th>
                                    <th>Estado</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody id="tableProvidersBody">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal para registrar -->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nuevo proveedor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <label for="Nit">NIT:</label>
                        <input type="text" name="Nit" id="Nit" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label for="name">Nombre:</label>
                        <input type="text" name="name" id="name" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label for="phone">Teléfono:</label>
                        <input type="text" name="phone" id="phone" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label for="email">Correo:</label>
                        <input type="text" name="email" id="email" class="form-control">
                    </div>
                    <div class="col-md-12">
                        <hr>
                        <h6><b>Persona de contacto</b></h6>
                        <br>
                    </div>
                    <div class="col-md-4">
                        <label for="concatName">Nombres</label>
                        <input type="text" name="concatName" id="concatName" class="form-control">
                    </div>
                    <div class="col-md-4">
                        <label for="contactLastName">Apellidos</label>
                        <input type="text" name="contactLastName" id="contactLastName" class="form-control"></div>
                    <div class="col-md-4">
                        <label for="phoneContact">Teléfono</label>
                        <input type="text" name="phoneContact" id="phoneContact" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="cleanInputs()">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="validateData()">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para ver -->
<div class="modal fade" id="show" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nuevo proveedor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <label >NIT:</label>
                        <input type="text" name="Nit" id="NitShow" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label >Nombre:</label>
                        <input type="text" name="name" id="nameShow" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label >Teléfono:</label>
                        <input type="text" name="phone" id="phoneShow" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label >Correo:</label>
                        <input type="text" name="email" id="emailShow" class="form-control">
                    </div>
                    <div class="col-md-12">
                        <hr>
                        <h6><b>Persona de contacto</b></h6>
                        <br>
                    </div>
                    <div class="col-md-4">
                        <label >Nombres</label>
                        <input type="text" name="concatName" id="concatNameShow" class="form-control">
                    </div>
                    <div class="col-md-4">
                        <label >Apellidos</label>
                        <input type="text" name="contactLastName" id="contactLastNameShow" class="form-control"></div>
                    <div class="col-md-4">
                        <label >Teléfono</label>
                        <input type="text" name="phoneContact" id="phoneContactShow" class="form-control">
                    </div>
                </div>
            </div>
            <div id="modalFooter" class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" >Cerrar</button>
                <button id="buttonEdit" style="display:none;" type="button" class="btn btn-success" data-dismiss="modal" onclick="update()">Actualizar</button>
            </div>
        </div>
    </div>
</div>
@stop
@section('lowerScripts')
<!-- Owl JavaScript -->
<script src="{{ asset('vendors/owl.carousel/dist/owl.carousel.min.js')}}"></script>

<!-- Owl Init JavaScript -->
<script src="{{ asset('dist/js/owl-data.js')}}"></script>
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{ asset('vendors/jszip/dist/jszip.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<!-- Toastr JS -->
<script src="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
<script src="{{ asset('lowerScripts/provider.js')}}"></script>

@stop