@extends('layouts.app')
@section('title')
Contratos
@parent
@stop
@section('topScripts')
<!-- Data Table CSS -->
<link href="{{asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Toastr CSS -->
<link href="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
{{--Tabla contratos --}}
<div class="row">
    <div class="col-md-12">
        <div class="card card-refresh">
            <div class="refresh-container">
                <div class="loader-pendulums"></div>
            </div>
            <div class="card-header card-header-action">
                <h4>Contratos</h4>
                <div class="d-flex align-items-center card-action-wrap">

                    <a class="inline-block mr-15" data-toggle="collapse" href="#collapse_1" aria-expanded="true">
                        <i class="zmdi zmdi-chevron-down"></i>
                    </a>
                    <a href="#" class="inline-block refresh mr-15" >
                        <i class="ion ion-md-radio-button-off"></i>
                    </a>
                    <a href="#" class="inline-block full-screen mr-15">
                        <i class="ion ion-md-expand"></i>
                    </a>
                </div>
            </div>
            <div id="collapse_1" class="collapse show">
                <div class="card-body">
                   @if($countVigente <= 0  )
                    <a href="{{route('contracts.create')}}" class="btn btn-primary" >
                        Nuevo &#32; <i class="fa fa-plus "></i>
                    </a>
                    @else 
                        <div class="alert alert-info" role="alert">
                                Lo sentimos, no puedes registrar un nuevo contrato mientras haya un contrato vigente, gracias.
                        </div>
                    @endif

                    <hr>
                    <div class="table-wrap">
                        <table id="contracts" class="table table-hover w-100 ">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nr° contrato</th>
                                    <th>Fecha inicio</th>
                                    <th>Fecha final</th>
                                    <th>Proveedor</th>
                                    <th>Contrato url</th>
                                    <th>Productos</th>
                                    <th>Estado</th>
                                    <th>Fecha creación</th>
                                    
                                </tr>
                            </thead>
                            <tbody id="tableContractsBody">
                                @foreach ($contracts as $contract)
                                      <tr>
                                      <td>{{$loop->iteration}}</td>
                                      <td>{{$contract->number}}</td>
                                      <td>{{$contract->start_date}}</td>
                                      <td>{{$contract->finis_date}}</td>
                                      <td>{{$contract->provider->name }}</td>
                                      <td> <button class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2" onclick="viewPDF('{{$contract->contracts_url}}')" ><span class="btn-icon-wrap" ><i class="icon ion-md-document"></i></span></button></td>
                                      <td> <button onclick="viewProducts('{{$contract->id}}')"  class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap" ><i class="fa fa-eye"></i></span></button></td>
                                      <td>
                                        @if($contract->status == 0 )
                                        <button type="button" class="btn btn-success btn-rounded btn-xs">Vigente</button>
                                        @else 
                                        <button type="button" class="btn btn-danger btn-rounded btn-xs">Terminado</button>
                                        @endif
                                      </td>
                                      <td>{{\FormatTime::longTimeFilter($contract->created_at)}}</td>
                                      </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--Tabla contratos --}}
{{--Modal Productos --}}
<div class="modal fade " id="modalProducts" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" >Productos / Contrato</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="products-contract">
            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

{{--Modal Productos --}}
@endsection

@section('lowerScripts')
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{ asset('vendors/jszip/dist/jszip.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<!-- Data Table JavaScript -->
<!-- Toastr JS -->
<script src="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
<script src="{{asset('js/DateFormat.js')}}"></script>
<script src="{{ asset('lowerScripts/contract.js')}}"></script>
@endsection