@extends('layouts.app')
@section('title')
Contratos
@parent
@stop
@section('topScripts')
<!-- Data Table CSS -->
<link href="{{asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Toastr CSS -->
<link rel="stylesheet" href="{{asset('vendors/jquery-steps/demo/css/jquery.steps.css')}}">
<link rel="stylesheet" href="{{asset('css/myStyles.css')}}">
<style>
.wizard > .content{
    overflow: auto !important;
    height: 620px;
}
.wizard > .content > .body ul > li {
    display: flex;
}
.dropify-wrapper {
    height: 192px !important;
    border: 1px solid #22AF47 !important;
}
/* The customcheck */

</style>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('vendors/dropify/dist/css/dropify.min.css')}}" rel="stylesheet" type="text/css"/>
@stop
@section('content')
     <!-- Row -->
                <div class="row">
                    <div class="col-xl-12">
                        <form  id="form-contract" enctype="multipart/form-data" >
                            <div class="mb-2">
                                <h2>Nuevo contrato</h2>
                            </div>
                              <div class="clear"></div>
                                <div id="wizard-contract">
                                            <h3>
                                                <span class="wizard-icon-wrap"><i class="ion ion-md-people"></i></span>
                                                <span class="wizard-head-text-wrap">
                                                    <span class="step-head">Proveedor</span>
                                                </span>	
                                            </h3>
                                            <section>
                                                <h3 class="display-6 mb-40">Seleccione un proveedor</h3>
                                                     <table id="providers" class="table table-hover w-100 ">
                                                        <thead>
                                                            <tr>
                                                                <th>Nit</th>
                                                                <th>Empresa</th>
                                                                <th>Contacto</th>
                                                                <th>Seleccionar</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tableProvidersBody"></tbody>
                                                    </table>    
                                            </section>
                                            <h3>
                                                <span class="wizard-icon-wrap"><i class="ion ion-md-basket"></i>    </i></span>
                                                <span class="wizard-head-text-wrap">
                                                    <span class="step-head">Productos</span>
                                                </span>	
                                            </h3>
                                            <section>
                                                <h3 class="display-6 mb-40">Seleccione  los productos</h3>
                                                <table id="products" class="table table-hover w-100 ">
                                                            <thead>
                                                                <tr>
                                                                    <th>Nombre</th>
                                                                    <th>Categoria</th>
                                                                    <th>Unidad medida</th>
                                                                    <th>Presentación</th>
                                                                    <th>Iva %</th>
                                                                    <th><label class="customcheck">Todos<input type="checkbox" id="products-all"  onclick="addProducts()" class="check-products"> <span class="checkmark"></span></label></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="tableProductsBody"></tbody>
                                                </table>    
                                            </section>
                                            <h3>
                                                <span class="wizard-icon-wrap"><i class="icon ion-md-list"></i></span>
                                                <span class="wizard-head-text-wrap">
                                                    <span class="step-head">Detalles productos</span>
                                                </span>	
                                            </h3>
                                            <section>
                                                <h3 class="display-6 mb-40">Detalles de los productos</h3>
                                            {{--Completar información de los productos--}}
                                             <div id="detailsProducts">

                                             </div>
                                            </section>

                                              <h3>
                                                <span class="wizard-icon-wrap"><i class="icon ion-md-clipboard"></i></i></span>
                                                <span class="wizard-head-text-wrap">
                                                    <span class="step-head">Detalles contrato</span>
                                                </span>	
                                            </h3>
                                            <section>
                                                <h3 class="display-6 mb-40">Detalles del contrato</h3>
                                            {{--Fechas de inicio y finalidad de un contrato --}}
                                            <div class="row">
                                                 <div class="col-md-4">
                                                        <span>Código de contrato</span>
                                                         <input  type="text" id="num_contract"  style="width: 280px;" class="form-control" />
                                                </div> 
                                                <div class="col-md-4" >
                                                    <span>Fecha de inicio</span>
                                                  <input id="startDate" width="276" data-date-format="yyyy-mm-dd" />
                                                </div>
                                                <div class="col-md-4">
                                                        <span>Fecha de finalización</span>
                                                         <input id="endDate" data-date-format="yyyy-mm-dd"  width="276" />
                                                </div> 
                                            </div>   
                                            <div class="spacing"></div>
                                            
                                            <div class="row">
                                                <div class="col-md-4">
                                                   <div class="card border-success">
                                                        <div class="card-header">Proveedor</div>
                                                        <div class="card-body text-success" style="text-align: center;">
                                                            <i  class="icon ion-md-contact" style="font-size: 4em;"></i>
                                                            <p class="card-text" id="resumen_provider"></p>
                                                        </div>
                                                    </div>
                                                </div>   
                                                <div class="col-md-4">
                                                   <div class="card border-success">
                                                        <div class="card-header">Productos</div>
                                                        <div class="card-body text-success" style="text-align: center;">
                                                            <i  class="icon ion-md-cart" style="font-size: 4em;"></i>
                                                            <p class="card-text" id="resumen_product"> </p>
                                                        </div>
                                                    </div>
                                                </div>    
                                                <div class="col-md-4">
									                    <input type="file" id="input-file-now" class="dropify"   data-allowed-file-extensions = "pdf" />
                                                </div>    
                                            </div> 

                                            
                                        </section>
                                </div>
                          </form>
                    </div>
                </div>
                <!-- /Row -->                              
       <!-- Modal edit -->
        <div class="modal fade" id="editProducts" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenter" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Editar Productos</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            
                            <input type="hidden" id="editProductId" value="">
                            <input type="hidden" id="Index" value="">
                            <div class="row">
                                        <div class="col-md-6 form-group">
                                            <label for="name_product">Nombre Producto</label>
                                            <input type="text" class="form-control" id="name_product" value="" disabled>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label for="category_product">Categoria</label>
                                            <input type="text" class="form-control" id="category_product" value="" disabled>
                                        </div> 
                            </div>
                            <div class="row">
                                        <div class="col-md-6 form-group">
                                            <label for="measure_product">Unidad de Medida</label>
                                            <input type="text" class="form-control" id="measure_product" value="" disabled >
                                        </div> 
                                        <div class="col-md-6 form-group">
                                            <label for="presentation_product">Presentación</label>
                                            <input type="text" class="form-control" id="presentation_product" value="" disabled >
                                        </div> 
                            </div>
                            <div class="row">
                               
                                <div class="col-md-6 form-group">
                                            <label class="control-label mb-10">Iva %</label>
                                            <input type="text" placeholder="" class="form-control" id="iva_product" disabled>
                                            <span class="form-text text-muted"> 99% </span>
                                </div>

                                <div class="col-md-6 form-group">
                                        <label class="control-label mb-10">Precio unitario</label>
                                        <input type="number" placeholder="" class="form-control" id="Price_product" onkeyup="GeneratePrices()" min="1">
                                        <span class="form-text text-muted">$ 15000</span>
                                </div>
                            </div>
                            <div class="row">
                               
                                    <div class="col-md-6 form-group">
                                                <label class="control-label mb-10">$ valor Iva </label>
                                                <input type="text" placeholder="" class="form-control" id="iva_total" id="val_iva" disabled>
                                                <span class="form-text text-muted"> precio unitario * IVA /100</span>
                                    </div>
    
                                    <div class="col-md-6 form-group">
                                            <label class="control-label mb-10">Total </label>
                                            <input type="text" placeholder="" class="form-control" id="val_total" disabled >
                                            <span class="form-text text-muted">$ precio unitario + IVA</span>
                                    </div>
                                    
                                </div>
                        </form> 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="updateStatus()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>                                       
@endsection
@section('lowerScripts')
{{--Data Tables --}}
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{ asset('vendors/jszip/dist/jszip.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
{{--Data Tables --}}
{{--Modal form wizard--}}
<script src="{{asset('vendors/jquery-steps/build/jquery.steps.min.js')}}"></script>
{{--Modal form wizard--}}
{{--Upload archives dropifi--}}
<script src="{{asset('vendors/dropify/dist/js/dropify.min.js')}}"></script>
{{--Upload archives dropifi--}}
<script src="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
    <script src="{{asset('js/DatePicker.js')}}" type="text/javascript"></script>

<script src="{{asset('lowerScripts/create_contract.js')}}"></script>
@endsection