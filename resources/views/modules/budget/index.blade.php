@extends('layouts.app')
@section('title')
Presupuestos
@parent
@stop

@section('topScripts')
<!-- Data Table CSS -->
<link href="{{asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Toastr CSS -->
<link href="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">

<link href="{{asset('vendors/daterangepicker/daterangepicker.css')}}" rel="stylesheet" type="text/css" />
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-refresh">
            <div class="card-header">
                <div class="refresh-container">
                    <div class="loader-pendulums"></div>
                </div>
                <div class="card-header card-header-action">
                    <h4>Presupuestos</h4>
                    <div class="d-flex align-items-center card-action-wrap">
                        <a class="inline-block mr-15" data-toggle="collapse" href="#collapse_1" aria-expanded="true">
                            <i class="zmdi zmdi-chevron-down"></i>
                        </a>
                        <a onclick="listBudgets()" class="inline-block refresh mr-15">
                            <i class="ion ion-md-radio-button-off"></i>
                        </a>
                        <a href="#" class="inline-block full-screen mr-15">
                            <i class="ion ion-md-expand"></i>
                        </a>
                        <a class="inline-block card-close" href="#" data-effect="fadeOut">
                            <i class="ion ion-md-close"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div id="callapse_1" class="collapse show">
                <div class="card-body">
                    @if($countVigente <= 0) <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#register">Nuevo <i class="fa fa-plus "></i></button>
                        @endif
                        <hr>
                        <div class="table-wrap">
                            <table id="budgetTable" class="table table-light table-hover">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Presupuesto Inicial</th>
                                        <th>Código</th>
                                        <th>Inicio de presupuesto</th>
                                        <th>Fin del presupuesto</th>
                                        <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody id="tableBudgetBody">

                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal para registrar -->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Registrar Presupuesto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="Código del presupuesto">Código del presupuesto</label>
                            <input type="text" name="budget_code" id="budget_code" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="Fecha Inicial">Fecha Inicial</label>
                            <input type="date" name="budget_begin" id="budget_begin" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="Fecha Final">Fecha Final</label>
                            <input type="date" name="budget_finish" id="budget_finish" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h4 style="text-align: center;">Caracterizaciones</h4>
                        <hr>
                    </div>
                    @foreach($charact as $ch)
                    @if($ch->status == 0)
                    <div class="col-md-6">
                        <label for="">{{$ch->name}}</label>
                        <input type="text" onkeydown="assing({{$ch->id}})" id="value{{$ch->id}}" class="form-control caract">
                    </div>
                    <div class="col-md-6">
                        <label for="">porcentaje</label>
                        <input type="text" readonly id="porcentage{{$ch->id}}" class="form-control valr">
                    </div>
                    @endif
                    @endforeach
                    <div style="text-align: center;" class="col-md-12">
                        <br>
                        <label for="">presupuesto</label>
                        <h4 " id=" total"></h4>
                        <input type="hidden" id="total_hiddden">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-gradient-secondary" data-dismiss="modal" onclick="cleanInputs()">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="register()">Guardar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal para ver -->
<div class="modal fade" id="see_budget" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ver Presupuesto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Presupuesto Inicial">Presupuesto Inicial</label>
                            <input type="number" name="initial_budget" id="initial_budgets" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Código del presupuesto">Código del presupuesto</label>
                            <input type="text" name="budget_code" id="budget_codes" class="form-control">
                        </div>
                    </div>
                    @foreach($charact as $ch)
                    <div class="col-md-6">
                        <label for="">{{$ch->name}}</label>
                        <input type="text" placeholder="100%" id="see{{$ch->id}}" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label for="">valor </label>
                        <input type="text" disabled id="see_val{{$ch->id}}" class="form-control">
                    </div>
                    @endforeach
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Fecha Inicial">Fecha Inicial</label>
                            <input type="date" name="budget_begin" id="budget_begins" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Fecha Final">Fecha Final</label>
                            <input type="date" name="budget_finish" id="budget_finishs" class="form-control">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-gradient-secondary" data-dismiss="modal" onclick="cleanInputs()">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="aditional" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Registrar Presupuesto Adicional</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Código del presupuesto">Código Presupuesto</label>
                            <select id="budget_id-r" value="{{old('')}}" name="budget_id" class="form-control">
                                <option value="">Seleccione un Código de presupuesto</option>
                                @foreach($budget as $value)
                                <option value="{{$value->id}}">
                                    {{$value->budget_code}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Presupuesto Adicional">Presupuesto Adicional</label>
                            <input type="number" name="aditional_budget-r" id="aditional_budget-r" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Código">Código</label>
                            <input type="text" name="aditional_budget_code-r" id="aditional_budget_code-r" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Fecha Inicial">Fecha Inicial</label>
                            <input type="date" name="aditional_begin_date-r" id="aditional_begin_date-r" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Fecha Final">Fecha Final</label>
                            <input type="date" name="aditional_finish_date-r" id="aditional_finish_date-r" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-gradient-secondary" data-dismiss="modal" onclick="cleanInputs()">Cerrar</button>
                    <button type="button" class="btn btn-primary" onclick="save_aditional_budget()">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</div>


@stop

@section('lowerScripts')
<!-- Owl JavaScript -->
<script src="{{ asset('vendors/owl.carousel/dist/owl.carousel.min.js')}}"></script>

<!-- Owl Init JavaScript -->
<script src="{{ asset('dist/js/owl-data.js')}}"></script>
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{ asset('vendors/jszip/dist/jszip.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<!-- Toastr JS -->
<script src="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>

<script src="{{asset ('/lowerScripts/budget.js')}}"></script>
@stop