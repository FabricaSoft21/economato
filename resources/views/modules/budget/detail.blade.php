@extends('layouts.app')
@section('title')
Presupuesto
@parent
@stop

@section('topScripts')
<!-- Data Table CSS -->
<link href="{{asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Toastr CSS -->
<link href="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">

<link href="{{asset('vendors/daterangepicker/daterangepicker.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('vendors/jquery-toggles/css/toggles.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('vendors/jquery-toggles/css/themes/toggles-light.css')}}" rel="stylesheet" type="text/css">
@stop

@section('content')
<input type="hidden" value="{{$budget->id}}" id="budget_id">
<div class="row">
    <div class="col-md-12">
        <div class="card card-refresh">
            <div class="card-header">
                <div class="refresh-container">
                    <div class="loader-pendulums"></div>
                </div>
                <div class="card-header card-header-action">
                    <h4>Presupuesto {{$budget->budget_code}}</h4>
                    <div class="d-flex align-items-center card-action-wrap">
                        <h4>porcentaje restante {{$porcentage_exact}}%</h4>
                    </div>
                </div>
            </div>
            <div id="callapse_1" class="collapse show">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            @if($porcentage_exact <= 30) 
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#aditional">Agregar Presupuesto <i class="fa fa-plus "></i></button>
                            @endif
                        </div>
                        <div class="col-md-8">
                        @if($porcentage_exact <= 30 && $porcentage_exact>15)
                        <div class="progress-wrap">
                            <div class="progress progress-bar-rounded">
                              <div class="progress-bar  progress-bar-striped bg-warning w-{{$porcetege_round}}" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                        @endif
                        @if($porcentage_exact <= 15)
                        <div class="progress-wrap">
                            <div class="progress progress-bar-rounded">
                              <div class="progress-bar  progress-bar-striped bg-danger w-{{$porcetege_round}}" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                        @endif
                        @if($porcentage_exact > 30)
                        <div class="progress-wrap">
                            <div class="progress progress-bar-rounded">
                              <div class="progress-bar  progress-bar-striped bg-info w-{{$porcetege_round}}" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                        @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <hr>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header card-header-action">
                                <h6>Presupuesto Aportado</h6>
                                <div class="d-flex align-items-center card-action-wrap">
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="hk-legend-wrap mb-20">
                                    @foreach($charact as $ch)
                                    <div class="hk-legend">
                                        <span>{{$ch->characterization->name}}: {{$ch->porcentage}}%</span>
                                    </div>
                                    @endforeach
                                </div>
                                <div id="e_chart_9" class="echart" style="height:291px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header card-header-action">
                                <h6>Presuptesto consumido</h6>
                                <div class="d-flex align-items-center card-action-wrap">
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="hk-legend-wrap mb-20">

                                </div>
                                <div id="e_chart_1" class="echart" style="height:294px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


@stop

@section('lowerScripts')
<!-- Owl JavaScript -->
<script src="{{ asset('vendors/owl.carousel/dist/owl.carousel.min.js')}}"></script>

<!-- Owl Init JavaScript -->
<script src="{{ asset('dist/js/owl-data.js')}}"></script>
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{ asset('vendors/jszip/dist/jszip.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<!-- Toastr JS -->
<script src="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>


<script src="{{asset('vendors/echarts/dist/echarts-en.min.js')}}"></script>

<script src="/lowerScripts/budget_detail.js"></script>
@stop