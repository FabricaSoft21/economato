@extends('layouts.app')

@section('title')
Pedidos Aprobados
@parent
@stop

@section('topScripts')
<!-- Data Table CSS -->
<link href="{{asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Toastr CSS -->
<link href="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
@stop


@section('content')




<div class="card-header tab-card-header">
    <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Producción de centro</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Instructores - Formación</a>
        </li>

    </ul>
</div>


<div class="tab-content" id="myTabContent">
    <!-- contenedor #1 -->
    <div class="tab-pane fade show active p-3" id="one" role="tabpanel" aria-labelledby="one-tab">

        <!-- tabla para listar los datos -->
        <div class="row">
            <div class="col-md-12">
                <div class="card card-refresh">
                    <div class="card-header">
                        <div class="refresh-container">
                            <div class="loader-pendulums"></div>
                        </div>
                        <div class="card-header card-header-action">
                            <h4>@role('Coordinación')Producción de centro @endrole</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-wrap">
                            <table id="orderCenter" class="table table-light table-hover">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Titulo</th>
                                        <th>Descripción</th>
                                        <th>N° Personas</th>
                                        <th>Costo</th>
                                        <th>Estado</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody id="tableHistoryBody">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!-- Modal para mostrar todos los datos -->
        <div class="modal fade" id="showHistoryOrders" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="nameView"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">



                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <div class="row">
                                        <thead class="Thead-Success	">
                                            <tr>
                                                <th>Titulo</th>
                                                <th>Descripción</th>
                                                <th>Fecha del evento</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr id="dataOne">
                                                <td id="title"></td>
                                                <td id="descripcion"></td>
                                                <td id="order_date"></td>
                                            </tr>
                                        </tbody>
                                    </div>
                                    <br>
                                </table>

                                <table class="table table-hover table-bordered">
                                    <div class="row">
                                        <thead>
                                            <tr>
                                                <th>Nombre del centro</th>
                                                <th>Solicitante</th>
                                                <th>Estado de la solicitud</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <td id="nombreCentro"></td>
                                            <td id="username"></td>
                                            <td id="status_id"></td>
                                        </tbody>
                                    </div>


                                </table>
                            </div>
                        </div>

                    </div>


                    <button type="button" id="btnProductsProductionCenter" class="btn btn-primary btn-lg btn-block">Ver información con detalles <i class="fa fa-mouse-pointer"></i></button>


                        <!-- Modal para mostrar detalle de la solicitud de centro de produccion -->

                                            <!-- Modal para ver -->
                    <div class="modal fade" id="show" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
                        <div class="modal-dialog modal-xl" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="dateShow">Fecha:</label>
                                            <input type="date" name="dateShow" id="dateShow" class="form-control" readonly>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="titleShow">Título:</label>
                                            <input type="text" name="titleShow" id="titleShow" class="form-control" readonly>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="quantity_peopleShow">Cantidad de personas:</label>
                                            <input type="text" name="quantity_peopleShow" id="quantity_peopleShow" class="form-control" readonly>

                                            <label for="locationShow">Centro:</label>
                                            <input type="text" name="locationShow" id="locationShow" class="form-control" readonly>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="descripcionShow">Descripción:</label>
                                            <textarea name="descripcionShow" id="descripcionShow" cols="30" rows="4" class="form-control" readonly></textarea>
                                        </div>
                                        <div class="col-md-12" style="padding-right:2rem;">
                                            <hr>
                                            <h3 class="float-left"><b>Productos</b></h3>
                                            <h2 class="float-right"><b>Costo:</b> $ <span id="costShow">0</span> </h2>
                                        </div>
                                        <div class="col-md-12">
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-sm mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Producto</th>
                                                        <th>Valor Unitario</th>
                                                        <th>Cantidad</th>
                                                        <th>Subtotal</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="bodyProductsShow">
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div id="modalFooter" class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    <button id="buttonEdit" style="display:none;" type="button" class="btn btn-success" data-dismiss="modal" onclick="update()">Actualizar</button>
                                </div>
                            </div>
                        </div>
                    </div>







                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>



    </div>

    <!-- contenedor #2  -->
    <div class="tab-pane fade p-3" id="two" role="tabpanel" aria-labelledby="two-tab">


        <!-- tabla para listar los datos -->
        <div class="row">
            <div class="col-md-12">
                <div class="card card-refresh">
                    <div class="card-header">
                        <div class="refresh-container">
                            <div class="loader-pendulums"></div>
                        </div>
                        <div class="card-header card-header-action">
                            <h4>@role('Coordinación')Solicitudes de formación @endrole</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-wrap">
                            <table id="requestTable" class="table table-light table-hover datatables">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Solicitante</th>
                                        <th>Ficha</th>
                                        <th>Costo</th>
                                        <th>Estado</th>
                                        <th>Descripción</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody id="tableRequestHistoryBody">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!-- Modal para mostrar todos los datos -->
        <div class="modal fade" id="showHistoryRequest" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="nameView"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">

                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <div class="row">
                                        <thead class="Thead-Success	">
                                            <tr>
                                                <th>Responsable</th>
                                                <th>Descripción</th>
                                                <th>Costo</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr id="dataOne">
                                                <td id="usernames"></td>
                                                <td id="descriptions"></td>
                                                <td id="costt"></td>
                                            </tr>
                                        </tbody>
                                    </div>
                                    <br>
                                </table>

                                <table class="table table-hover table-bordered">
                                    <div class="row">
                                        <thead>
                                            <tr>
                                                <th>Ficha</th>
                                                <th>Estado de la solicitud</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <td id="file"></td>
                                            <td id="status_ids"></td>
                                        </tbody>
                                    </div>


                                </table>
                            </div>
                        </div>

                    </div>

                          <button type="button" id="btnProducts" class="btn btn-primary btn-lg btn-block">Ver información con detalles <i class="fa fa-mouse-pointer"></i></button>



                                            <!-- Modal para mostrar productos-->
                        <div class="modal fade" id="showInformation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
                            <div class="modal-dialog modal-xl" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Informacion de la Solicitud</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <h4>Ficha: <span id="showFile" class="badge badge-pill badge-light"> </span></h4>
                                                <p class="text-center">Aprendicez: <span id="nApprentices"></span></p>
                                                <p class="text-center">Estado: <span id="status"></span></p>
                                                <p class="text-center">Caracterizacion: <span id="characterization"></span></p>
                                                @role('Coordinación')
                                                <br>
                                                <p style="color:midnightblue">Responsable: <span id="responsible"></span></p>
                                                <p style="color:midnightblue">Identificacion: <span id="identity"></span></p>

                                                @endrole
                                            </div>
                                            <div class="col-md-7">
                                                <label for="showDescription">Descripción:</label>
                                                <textarea name="showDescription" id="ShowDescription" cols="30" rows="4" class="form-control" disabled></textarea>
                                            </div>
                                            <div class="col-md-12" style="padding-right:2rem;">
                                                <hr>
                                                <h3 class="float-left"><b>Productos</b></h3>
                                                <h2 class="float-right"><b>Costo:</b> $ <span id="showCost">0</span> </h2>
                                            </div>
                                            <div class="col-md-12">
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12" id="div-edit" style="display:none;">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <label for="searchEdit">Buscar productos:</label>
                                                        <input type="text" id="searchEdit" class="form-control" placeholder="Buscar" onkeyup="search(value)">
                                                    </div>
                                                    <div class="col-md-5" id="quantityDivEdit" style="display:none;">
                                                        <label for="">Cantidad en <span id="meUnitEdit"></span>: </label>
                                                        <input type="text" class="form-control" placeholder="Cantidad" id="quantityEdit">
                                                    </div>
                                                    <div class="col-md-2 float-right" style=" display: flex; align-items: flex-end;">
                                                        <button id="btnAddProductEdit" type="button" class="btn btn-success" style="display:none;">Agregar <i class="la la-plus"></i> </button>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <ul class="list-group" id="resultsEdit">


                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <table class="table mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Producto</th>
                                                            <th>Valor Unitario</th>
                                                            <th>Cantidad</th>
                                                            <th>Valor Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="listProductsShow">

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Cerrar</button>
                                        <button id="buttonEdit" style="display:none;" type="button" class="btn btn-success" >Actualizar</button>
                                    </div>
                                </div>
                            </div>
                        </div>






                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>




    </div>
</div>

@stop



@section('lowerScripts')
<!-- Owl JavaScript -->



<script src="{{ asset('vendors/owl.carousel/dist/owl.carousel.min.js')}}"></script>

<!-- Owl Init JavaScript -->
<script src="{{ asset('dist/js/owl-data.js')}}"></script>
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{ asset('vendors/jszip/dist/jszip.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<!-- Toastr JS -->
<script src="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
<script src="{{ asset('lowerScripts/productionCenter/ProductionHistoryRequest.js')}}"></script>
<script src="{{ asset('lowerScripts/request/requestHistory.js')}}"></script>





@endsection