@extends('layouts.app')
@section('title')
Centro de Producción
@parent
@stop
@section('topScripts')
<!-- Data Table CSS -->
<link href="{{asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Toastr CSS -->
<link href="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('Select2/css/select2.css')}}" rel="stylesheet" type="text/css">
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-refresh">
            <div class="refresh-container">
                <div class="loader-pendulums"></div>
            </div>
            <div class="card-header card-header-action">
                <h4>Ordenes de centro de producción</h4>
                <div class="d-flex align-items-center card-action-wrap">

                    <a class="inline-block mr-15" data-toggle="collapse" href="#collapse_1" aria-expanded="true">
                        <i class="zmdi zmdi-chevron-down"></i>
                    </a>
                    <a href="#" class="inline-block refresh mr-15" onclick="listOrders()">
                        <i class="ion ion-md-radio-button-off"></i>
                    </a>
                    <a href="#" class="inline-block full-screen mr-15">
                        <i class="ion ion-md-expand"></i>
                    </a>
                </div>
            </div>
            <div id="collapse_1" class="collapse show">
                <div class="card-body">
                    @role('Lider-Produccion-centro')
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#register">
                        Nuevo &#32; <i class="fa fa-plus "></i>
                    </button>
                    @endrole
                    <hr>
                    <div class="table-wrap">

                        <table id="tableOrders" class="table table-hover w-100 ">

                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Título</th>
                                    <th>Fecha</th>
                                    <th>N° Personas</th>
                                    <th>Costo</th>
                                    <th>Centro</th>
                                    <th>Estado</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>

                            <tbody id="tableOrdersBody">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal para registrar -->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h5 class="modal-title">Nueva orden de producción</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <label for="date">Fecha:</label>
                        <input type="date" name="date" id="date" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label for="title">Título:</label>
                        <input type="text" name="title" id="title" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label for="quantity_people">Cantidad de personas:</label>
                        <input type="text" name="quantity_people" id="quantity_people" class="form-control">

                        <label for="location_id">Centro:</label>
                        <select name="location_id" id="location_id" class="form-control">
                            @foreach($centers as $c)
                            <option value="{{$c->id}}">{{$c->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="email">Descripción:</label>
                        <textarea name="descripcion" id="descripcion" cols="30" rows="4" class="form-control"></textarea>
                    </div>
                    <div class="col-md-12" style="padding-right:2rem;">
                        <hr>
                        <h3 class="float-left"><b>Productos</b></h3>
                        <h2 class="float-right"><b>Costo:</b> $ <span id="cost">0</span> </h2>
                    </div>
                    <div class="col-md-12">
                        <hr>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-5">
                        <label for="search">Buscar productos:</label>
                        <input type="text" id="search" class="form-control" placeholder="Buscar" onkeyup="search(value)">
                    </div>
                    <div class="col-md-5" id="quantityDiv" style="display:none;">
                        <label for="search">Cantidad en <span id="meUnit"></span>: </label>
                        <input type="text" class="form-control" placeholder="Cantidad" id="quantity">
                    </div>
                    <div class="col-md-2 float-right" style=" display: flex; align-items: flex-end;">
                        <button id="btnAddProduct" type="button" class="btn btn-success" style="display:none;">Agregar <i class="la la-plus"></i> </button>
                    </div>
                    <div class="col-md-12">
                        <ul class="list-group" id="results">


                        </ul>
                    </div>
                    <div class="col-md-12">
                        <table class="table mb-0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Producto</th>
                                    <th>Valor Unitario</th>
                                    <th>Cantidad</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                            <tbody id="bodyProducts">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="cleanInputs()">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="register()">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para ver -->
<div class="modal fade" id="show" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <label for="dateShow">Fecha:</label>
                        <input type="date" name="dateShow" id="dateShow" class="form-control" readonly>
                    </div>
                    <div class="col-md-6">
                        <label for="titleShow">Título:</label>
                        <input type="text" name="titleShow" id="titleShow" class="form-control" readonly>
                    </div>
                    <div class="col-md-6">
                        <label for="quantity_peopleShow">Cantidad de personas:</label>
                        <input type="text" name="quantity_peopleShow" id="quantity_peopleShow" class="form-control" readonly>

                        <label for="locationShow">Centro:</label>
                        <input type="text" name="locationShow" id="locationShow" class="form-control" readonly>
                    </div>
                    <div class="col-md-6">
                        <label for="descripcionShow">Descripción:</label>
                        <textarea name="descripcionShow" id="descripcionShow" cols="30" rows="4" class="form-control" readonly></textarea>
                    </div>
                    <div class="col-md-12" style="padding-right:2rem;">
                        <hr>
                        <h3 class="float-left"><b>Productos</b></h3>
                        <h2 class="float-right"><b>Costo:</b> $ <span id="costShow">0</span> </h2>
                    </div>
                    <div class="col-md-12">
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-sm mb-0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Producto</th>
                                    <th>Valor Unitario</th>
                                    <th>Cantidad</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                            <tbody id="bodyProductsShow">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="modalFooter" class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button id="buttonEdit" style="display:none;" type="button" class="btn btn-success" data-dismiss="modal" onclick="update()">Actualizar</button>
            </div>
        </div>
    </div>
</div>
@stop
@section('lowerScripts')
<!-- Owl JavaScript -->
<script src="{{ asset('vendors/owl.carousel/dist/owl.carousel.min.js')}}"></script>

<!-- Owl Init JavaScript -->
<script src="{{ asset('dist/js/owl-data.js')}}"></script>
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{ asset('vendors/jszip/dist/jszip.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<!-- Toastr JS -->
<script src="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
<!-- Select 2 -->
<script src="{{asset('Select2/js/select2.js')}}"></script>
<script src="{{ asset('lowerScripts/productionCenter/centerProductionBase.js')}}"></script>
@role('Lider-Produccion-centro')
<script src="{{ asset('lowerScripts/productionCenter/centerProduction.js')}}"></script>
@endrole
@role('Coordinación')
<script src="{{ asset('lowerScripts/productionCenter/centerProductionC.js')}}"></script>
@endrole
@role('Bodega')
<script src="{{ asset('lowerScripts/productionCenter/centerProductionB.js')}}"></script>
@endrole
@stop