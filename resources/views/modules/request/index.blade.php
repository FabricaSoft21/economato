@extends('layouts.app')
@section('title')
Solicitudes
@parent
@stop

@section('topScripts')
<!-- Data Table CSS -->
<link href="{{asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Toastr CSS -->
<link href="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">

<link href="{{asset('vendors/daterangepicker/daterangepicker.css')}}" rel="stylesheet" type="text/css" />
@stop

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="card card-refresh">
            <div class="card-header">
                <div class="refresh-container">
                    <div class="loader-pendulums"></div>
                </div>
                <div class="card-header card-header-action">

                    <h4>@role('Instructores')Mis Solicitudes @endrole @role('Coordinación')Solicitudes @endrole</h4>

                    <div class="d-flex align-items-center card-action-wrap">
                        <a class="inline-block mr-15" data-toggle="collapse" href="#collapse_1" aria-expanded="true">
                            <i class="zmdi zmdi-chevron-down"></i>
                        </a>
                        <a onclick="listRequest()" class="inline-block refresh mr-15">
                            <i class="ion ion-md-radio-button-off"></i>
                        </a>
                        <a href="#" class="inline-block full-screen mr-15">
                            <i class="ion ion-md-expand"></i>
                        </a>
                        <a class="inline-block card-close" href="#" data-effect="fadeOut">
                            <i class="ion ion-md-close"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div id="callapse_1" class="collapse show">
                <div class="card-body">
                    @role('Instructores')
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#register"><i class="fa fa-plus "></i>
                        Nueva Solicitud
                    </button>
                    @endrole
                    <hr>
                    <div class="table-wrap">
                        <table id="requestTable" class="table table-light table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th id="hInstr">Instructor</th>
                                    <th>fecha</th>
                                    <th>ficha</th>
                                    <th style="text-align:center;">estado</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody id="tableRequestBody">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal para registrar -->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Solicitud</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5">
                        <label for="">Ficha</label>
                        <select id="file_id" class="form-control">
                            <option value="">Seleccione...</option>
                            @foreach($files as $fi)
                            @if($fi->file->status == 0)
                            <option value="{{$fi->file->id}}">{{$fi->file->number}}</option>
                            @endif
                            @endforeach
                        </select>

                        <br>
                        <form class="navbar-search-alt" id="searchRecipe">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><span class="feather-icon"><i data-feather="search"></i></span></span>
                                </div>
                                <input class="form-control" type="search" name="search" id="search" placeholder="Receta" aria-label="Search">
                            </div>
                        </form>

                        <br>
                        <div class="row">
                            <div id="infRecipe" class="col-md-6">

                            </div>
                            <div class="col-md-6">
                                <div class="" id="quantityDivPerson" style="display:none;">
                                    <label for="">Numero de personas<span id="meUnit"></span>: </label>
                                    <input type="text" class="form-control" placeholder="Cantidad" id="quantityPerson">
                                </div>
                                <br>
                                <div class="" style=" display: flex; align-items: flex-end;">
                                    <button id="btnAddRecipe" type="button" class="btn btn-primary btn-lg btn-block" style="display:none;">Agregar <i class="la la-plus"></i> </button>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-7">
                        <label for="descripcion">Descripción:</label>
                        <textarea name="descripcion" id="descripcion" cols="30" rows="4" class="form-control"></textarea>

                        <br>
                        <div id="excludedProducts">

                        </div>

                    </div>
                    <div class="col-md-12" style="padding-right:2rem;">
                        <hr>
                        <h3 class="float-left"><b>Productos</b></h3>
                        <h2 class="float-right"><b>Costo:</b> $ <span id="cost">0</span> </h2>
                    </div>
                    <div class="col-md-12">
                        <hr>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-5">
                        <label for="search">Buscar productos:</label>
                        <input type="text" id="search" class="form-control" placeholder="Buscar" onkeyup="search(value)">
                    </div>
                    <div class="col-md-5" id="quantityDiv" style="display:none;">
                        <label for="">Cantidad en <span id="meUnit"></span>: </label>
                        <input type="text" class="form-control" placeholder="Cantidad" id="quantity">
                    </div>
                    <div class="col-md-2 float-right" style=" display: flex; align-items: flex-end;">
                        <button id="btnAddProduct" type="button" class="btn btn-success" style="display:none;">Agregar <i class="la la-plus"></i> </button>
                    </div>
                    <div class="col-md-12">
                        <ul class="list-group" id="results">


                        </ul>
                    </div>
                    <div class="col-md-12">
                        <table class="table mb-0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Producto</th>
                                    <th>Valor Unitario</th>
                                    <th>Cantidad</th>
                                    <th>Valor Total</th>
                                </tr>
                            </thead>
                            <tbody id="bodyProducts">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="cleanInputs()">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="register()">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para mostrar -->
<div class="modal fade" id="showInformation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Informacion de la Solicitud</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5">
                        <h4>Ficha: <span id="showFile" class="badge badge-pill badge-light"> </span></h4>
                        <p class="text-center">Aprendicez: <span id="nApprentices"></span></p>
                        <p class="text-center">Estado: <span id="status"></span></p>
                        <p class="text-center">Caracterizacion: <span id="characterization"></span></p>
                        @role('Coordinación')
                        <br>
                        <p style="color:midnightblue">Responsable: <span id="responsible"></span></p>
                        <p style="color:midnightblue">Identificacion: <span id="identity"></span></p>

                        @endrole
                    </div>
                    <div class="col-md-7">
                        <label for="showDescription">Descripción:</label>
                        <textarea name="showDescription" id="ShowDescription" cols="30" rows="4" class="form-control" disabled></textarea>
                    </div>
                    <div class="col-md-12" style="padding-right:2rem;">
                        <hr>
                        <h3 class="float-left"><b>Productos</b></h3>
                        <h2 class="float-right"><b>Costo:</b> $ <span id="showCost">0</span> </h2>
                    </div>
                    <div class="col-md-12">
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" id="div-edit" style="display:none;">
                        <div class="row">
                            <div class="col-md-5">
                                <label for="searchEdit">Buscar productos:</label>
                                <input type="text" id="searchEdit" class="form-control" placeholder="Buscar" onkeyup="search(value)">
                            </div>
                            <div class="col-md-5" id="quantityDivEdit" style="display:none;">
                                <label for="">Cantidad en <span id="meUnitEdit"></span>: </label>
                                <input type="text" class="form-control" placeholder="Cantidad" id="quantityEdit">
                            </div>
                            <div class="col-md-2 float-right" style=" display: flex; align-items: flex-end;">
                                <button id="btnAddProductEdit" type="button" class="btn btn-success" style="display:none;">Agregar <i class="la la-plus"></i> </button>
                            </div>
                            <div class="col-md-12">
                                <ul class="list-group" id="resultsEdit">


                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <table class="table mb-0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Producto</th>
                                    <th>Valor Unitario</th>
                                    <th>Cantidad</th>
                                    <th>Valor Total</th>
                                </tr>
                            </thead>
                            <tbody id="listProductsShow">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" >Cerrar</button>
                <button id="buttonEdit" style="display:none;" type="button" class="btn btn-success" >Actualizar</button>
            </div>
        </div>
    </div>
</div>

@stop


@section('lowerScripts')
<!-- Owl JavaScript -->
<script src="{{ asset('vendors/owl.carousel/dist/owl.carousel.min.js')}}"></script>

<!-- Owl Init JavaScript -->
<script src="{{ asset('dist/js/owl-data.js')}}"></script>
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{ asset('vendors/jszip/dist/jszip.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<!-- Toastr JS -->
<script src="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
<script src="{{asset('lowerScripts/request/requestB.js')}}"></script>
@role('Instructores')
<script src="{{asset('lowerScripts/request/request.js')}}"></script>
@endrole
@role('Coordinación')
<script src="{{asset('lowerScripts/request/requestC.js')}}"></script>
@endrole
@role('Bodega')
<script src="{{asset('lowerScripts/request/requestBodega.js')}}"></script>
@endrole
@stop