@extends('layouts.app')
@section('title')
Solicutudes
@parent
@stop

@section('topScripts')
<!-- Data Table CSS -->
<link href="{{asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Toastr CSS -->
<link href="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">

<link href="{{asset('vendors/daterangepicker/daterangepicker.css')}}" rel="stylesheet" type="text/css" />
@stop

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="card card-refresh">
            <div class="card-header">
                <div class="refresh-container">
                    <div class="loader-pendulums"></div>
                </div>
                <div class="card-header card-header-action">
                     <h4>Solicitudes</h4>

                     <br>
                </div>
            </div>

                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                        <th scope="col">Id</th>
                                        <th scope="col">Responsable</th>
                                        <th scope="col">Costo</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $order)
                                        <tr>
                                        <th scope="row">{{$order->id}}</th>
                                        <td>{{$order->user->name}}</td>
                                        <td>{{$order->cost}}</td>
                                        <td>{{$order->statusOrder->name}}</td>
                                        <td>
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#infProducts_{{$order->id}}">Productos</button>
                                            <a type="button" class="btn btn-primary" href="{{ route('request.approve', ['id'=>$order->id]) }}">Aprobar</a>
                                        </td>
                                        </tr>



                                                <!-- Button trigger modal -->
                                                

                                                    <!-- Modal -->
                                                    <div class="modal fade" id="infProducts_{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="infProducts_{{$order->id}}" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Ver productos</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            ...

                                                            @foreach($order->ordersXproducts as $orderXproduct)

                                                                <p>{{$orderXproduct->product->name}} tipo de producto:{{$orderXproduct->product->typeProduct->name}} cantidad:<span style="color:grey">{{$orderXproduct->quantity}}</span></p>
                                                                <p>creado hace {{\FormatTime::LongTimeFilter($orderXproduct->product->created_at)}}</p>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    </div>



                                    @endforeach
                                    </tbody>
                                </table>



                           

                

        </div>
    </div>
</div>
               


@stop

@section('lowerScripts')
<!-- Owl JavaScript -->
<script src="{{ asset('vendors/owl.carousel/dist/owl.carousel.min.js')}}"></script>

<!-- Owl Init JavaScript -->
<script src="{{ asset('dist/js/owl-data.js')}}"></script>
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{ asset('vendors/jszip/dist/jszip.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<!-- Toastr JS -->
<script src="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
<script src="{{asset('lowerScripts/request.js')}}"></script>
@stop