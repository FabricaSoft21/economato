@extends('layouts.app')

@section('title')
Presupuesto Adicional
@parent
@stop

@section('topScripts')
<!-- Data Table CSS -->
<link href="{{asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Toastr CSS -->
<link href="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
@stop

@section('content')

<!-- Lista de Presupuestos -->
<div class="row">
    <div class="col-md-12">
        <div class="card card-refresh">
            <div class="refresh-container">
                <div class="loader-pendulums"></div>
            </div>
            <div class="card-header card-header-action">
                <h4>Presupuestos Adicionales</h4>
                <div class="d-flex align-items-center card-action-wrap">
                    <a class="inline-block mr-15" data-toggle="collapse" href="#collapse_1" aria-expanded="true">
                        <i class="zmdi zmdi-chevron-down"></i>
                    </a>
                    <a onclick="list_aditional_budget()" class="inline-block refresh mr-15">
                        <i class="ion ion-md-radio-button-off"></i>
                    </a>
                    <a href="#" class="inline-block full-screen mr-15">
                        <i class="ion ion-md-expand"></i>
                    </a>
                    <a class="inline-block card-close" href="#" data-effect="fadeOut">
                        <i class="ion ion-md-close"></i>
                    </a>
                </div>
            </div>
            <div id="collapse_1" class="collapse show">
                <div class="card-body">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#register"><i class="fa fa-plus "></i> Nuevo Presupuesto
                    </button>
                    <hr>
                    <div class="table-wrap">
                        <table id="adiBudgetTable" class="table table-bordered w-100 table-hover">
                            <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Código del presupuesto</th>
                                    <th>Presupuesto Adicional</th>
                                    <th>Código</th>
                                    <th>Fecha Inicial</th>
                                    <th>Fecha Final</th>
                                    <th>Estado</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody id="table_aditional_budget_body">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal para ver -->
<div class="modal fade" id="see_aditional_budget" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ver Presupuestos Adicionales</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Código Presupuesto">Código Presupuesto</label>
                            <input type="number" name="budget_id" id="budget_id" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Presupuesto Adicional">Presupuesto Adicional</label>
                            <input type="number" name="aditional_budget" id="aditional_budget" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Código">Código</label>
                            <input type="text" name="aditional_budget_code" id="aditional_budget_code" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Fecha Inicial">Fecha Inicial</label>
                            <input type="date" name="aditional_begin_date" id="aditional_begin_date" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Fecha Final">Fecha Final</label>
                            <input type="date" name="aditional_finish_date" id="aditional_finish_date" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-gradient-secondary" data-dismiss="modal" onclick="cleanInputs()">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal para registrar -->



@stop

@section('lowerScripts')
<!-- Owl JavaScript -->
<script src="{{ asset('vendors/owl.carousel/dist/owl.carousel.min.js')}}"></script>

<!-- Owl Init JavaScript -->
<script src="{{ asset('dist/js/owl-data.js')}}"></script>
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{ asset('vendors/jszip/dist/jszip.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<!-- Toastr JS -->
<script src="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>

<script src="/lowerScripts/adibudget.js"></script>
@stop