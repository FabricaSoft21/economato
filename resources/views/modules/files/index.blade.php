@extends('layouts.app')
@section('title')
Fichas
@parent
@stop
@section('topScripts')
<!-- Data Table CSS -->
<link href="{{asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Toastr CSS -->
<link href="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('Select2/css/select2.css')}}" rel="stylesheet" type="text/css">
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-refresh">
            <div class="refresh-container">
                <div class="loader-pendulums"></div>
            </div>
            <div class="card-header card-header-action">
                <h4>Fichas</h4>
                <div class="d-flex align-items-center card-action-wrap">
                    <a class="inline-block mr-15" data-toggle="collapse" href="#collapse_1" aria-expanded="true">
                        <i class="zmdi zmdi-chevron-down"></i>
                    </a>
                    <a onclick="files()" class="inline-block refresh mr-15">
                        <i class="ion ion-md-radio-button-off"></i>
                    </a>
                    <a href="#" class="inline-block full-screen mr-15">
                        <i class="ion ion-md-expand"></i>
                    </a>
                    <a class="inline-block card-close" href="#" data-effect="fadeOut">
                        <i class="ion ion-md-close"></i>
                    </a>
                </div>
            </div>
            <div id="collapse_1" class="collapse show">
                <div class="card-body">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#register">
                        Nueva ficha
                    </button>
                    <hr>
                    <div class="table-wrap">
                        <table id="fileTable" class="table table-light table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Programa</th>
                                    <th>Caracterizacion</th>
                                    <th>Ficha</th>
                                    <th>Aprendices</th>
                                    <th>Estado</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody id="tableFilesBody">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal para registrar -->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Registrar ficha</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <label for="">Numero de la ficha</label>
                        <input type="text" id="number" class="form-control">
                        <br>
                    </div>
                    <div class="col-md-6">
                        <label for="">Numero de aprendices</label>
                        <input id="apprentices" class="form-control" type="text">
                        <br>
                    </div>
                    <div class="col-md-4">
                        <label for="">fecha de inicio</label>
                        <input id="start_date" class="form-control" type="date">
                        <br>
                    </div>
                    <div class="col-md-4">
                        <label for="">Fecha de terminacion</label>
                        <input class="form-control" id="finish_date" type="date">
                        <br>
                    </div>
                    <div class="col-md-4">
                        <label>Caracterizacion</label>
                        <select class="form-control" id="charact_id">
                            <option value="">Seleccione...</option>
                            @foreach($charac as $ch)
                            <option value="{{$ch->id}}">{{$ch->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label>Centro de formacion</label>
                        <select onchange="get_program()" class="form-control" id="center_id">
                            <option value="">Seleccione...</option>
                            @foreach($center as $ce)
                            <option value="{{$ce->id}}">{{$ce->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label>Programa de formacion</label>
                        <select class="form-control" id="program_id">
                        </select>
                    </div>
                    <div class="col-md-12">
                        <hr>
                    </div>
                    <div class="col-md-5">
                        <label for="">Instructores</label>
                        <select onchange='add_table(value)' class="form-control" name="" id="teacher_id">
                            <option value="">Seleccione...</option>
                            @foreach($teacher as $th)
                            <option value="{{$th}}">{{$th->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-7">
                        <label for="">Instructores seleccionados</label>
                        <table class="table">
                            <thead>
                                <tr class="table table-bordered">
                                    <th>N°</th>
                                    <th>Nombre del instructor</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody id="tbl_teachers">

                            </tbody>
                        </table>
                    </div>
                    <div>

                    </div>
                    <div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" onclick="register()" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>



<!-- Modal para ver -->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Información de la ficha</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Numero de la ficha</label>
                            <input type="text" id="numbers" class="form-control">

                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Numero de aprendices</label>
                            <input id="apprenticess" class="form-control" type="text">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">fecha de inicio</label>
                            <input id="start_dates" class="form-control" type="date">
                        </div>
                    </div>



                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Fecha de terminacion</label>
                            <input class="form-control" id="finish_dates" type="date">

                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Caracterizacion</label>
                            <select class="form-control" id="charact_ids">
                           
                            </select>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Centro de formacion</label>
                            <select onchange="getProgramUpdate()" class="form-control" id="center_ids">
                                <option>Selecione un centro de formación.</option>
                                @foreach($center as $ce)
                                <option value="{{$ce->id}}">{{$ce->name}}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                </div>



                <div class="col-md-6">
                    <div class="form-group">
                        <label>Programa de formacion</label>
                        <select class="form-control" id="program_ids">

                        </select>
                    </div>
                </div>
            </div>


            <div class="modal-footer">
                <button type="button" onclick="cleanInputs()" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal para actualizar -->
<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Actualizar ficha</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Numero de la ficha</label>
                            <input type="text" id="numberss" class="form-control">

                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Numero de aprendices</label>
                            <input id="apprenticesss" class="form-control" type="text">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">fecha de inicio</label>
                            <input id="start_datess" class="form-control" type="date">
                        </div>
                    </div>



                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Fecha de terminacion</label>
                            <input class="form-control" id="finish_datess" type="date">

                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Caracterizacion</label>
                            <select class="form-control" id="charact_idss">
                                @foreach($charac as $ch)

                                <option value="{{$ch->id}}">{{$ch->name}}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Centro de formacion</label>
                            <select onchange="getProgramUpdate()" class="form-control" id="center_idss">
                                <option>Selecione un centro de formación.</option>
                                @foreach($center as $ce)
                                <option value="{{$ce->id}}">{{$ce->name}}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                </div>



                <div class="col-md-6">
                    <div class="form-group">
                        <label>Programa de formacion</label>
                        <select class="form-control" id="program_idss">

                        </select>
                    </div>
                </div>
            </div>


            <div class="modal-footer">
                <button type="button" onclick="cleanInputs()" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="buttonEdit" class="btn btn-primary">Actualizar</button>
            </div>
        </div>
    </div>
</div>





@stop
@section('lowerScripts')
<!-- Owl JavaScript -->
<script src="{{ asset('vendors/owl.carousel/dist/owl.carousel.min.js')}}"></script>

<!-- Owl Init JavaScript -->
<script src="{{ asset('dist/js/owl-data.js')}}"></script>
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{ asset('vendors/jszip/dist/jszip.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<!-- Toastr JS -->
<script src="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
<script src="{{asset('Select2/js/select2.js')}}"></script>

<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="{{asset('lowerScripts/file.js')}}"></script>
@stop