@extends('layouts.app')

@section('title')
Mi Perfil
@parent
@stop


@section('content')

<div class="container">
    <div class="row">
        <div class="card col-md-10">
            <div class="card-header">
            <h4>Cambiar credenciales</h4>
            </div>
            <form action="{{route('user.changePassword')}}" method="POST">
              @csrf
                <label for="contraseña_actual">Contraseña Actual</label>
                <input class="form-control{{ $errors->has('contraseña_actual') ? ' is-invalid' : '' }}" type="password" name="contraseña_actual" id="contraseña_actual" value="" />
                @if ($errors->has('contraseña_actual'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('contraseña_actual') }}</strong>
                            </span>
                @endif

                <label for="nueva_contraseña">Nueva Contraseña</label>
                <input class="form-control{{ $errors->has('nueva_contraseña') ? ' is-invalid' : ''  }}" type="password" name="nueva_contraseña" id="nueva_contraseña" value="" />
                @if ($errors->has('nueva_contraseña'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nueva_contraseña') }}</strong>
                            </span>
                @endif

                <label for="confirmacion_contraseña">Confirmar </label>
                <input class="form-control{{ $errors->has('confirmacion_contraseña') ? ' is-invalid' : ''  }}" type="password" name="confirmacion_contraseña" id="confirmacion_contraseña" value="" />
                @if ($errors->has('confirmacion_contraseña'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('confirmacion_contraseña') }}</strong>
                            </span>
                @endif
                <br>
                <button type="submit" class="btn btn-success">Actualizar</button>
            </form>
            <br>
        </div>
    </div>
</div>


@stop