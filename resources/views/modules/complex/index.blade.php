@extends('layouts.app')
@section('title')
Complejos
@parent
@stop
@section('topScripts')
<!-- Data Table CSS -->
<link href="{{asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Toastr CSS -->
<link href="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-refresh">
            <div class="refresh-container">
                <div class="loader-pendulums"></div>
            </div>
            <div class="card-header card-header-action">
                <h4>Complejos</h4>
                <div class="d-flex align-items-center card-action-wrap">
                    <a class="inline-block mr-15" data-toggle="collapse" href="#collapse_1" aria-expanded="true">
                        <i class="zmdi zmdi-chevron-down"></i>
                    </a>
                    <a onclick="listComplex()" class="inline-block refresh mr-15">
                        <i class="ion ion-md-radio-button-off"></i>
                    </a>
                    <a href="#" class="inline-block full-screen mr-15">
                        <i class="ion ion-md-expand"></i>
                    </a>
                    <a class="inline-block card-close" href="#" data-effect="fadeOut">
                        <i class="ion ion-md-close"></i>
                    </a>
                </div>
            </div>
            <div id="collapse_1" class="collapse show">
                <div class="card-body">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#register"><i class="fa fa-plus "></i>
                        Nuevo complejo
                    </button>
                    <hr>
                    <div class="table-wrap">
                        <table id="complexTable" class="table table-hover table-bordered w-100">
                            <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Nombre</th>
                                    <th>Regional</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody id="tableComplexBody">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal para registrar Complejos -->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Registrar complejo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Nombre del complejo</label>
                        <input type="text" id="name" class="form-control">
                        <br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label for="Regional">Regional</label>
                        <select class="form-control" name="regional_id" id="regional_id">
                            <option value="regional_id">Seleccione una regional</option>

                            @foreach($region as $value)
                            <option value="{{$value->id}}">
                                {{$value->name}}
                            </option>
                            @endforeach

                        </select>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" onclick="cleanInputs()" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" onclick="register()" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>



<!-- Modal para ver Complejos -->
<div class="modal fade" id="see_complex" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ver complejos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Nombre del complejo</label>
                        <input type="text" id="names" class="form-control">
                        <br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label for="Regional">Regional</label>
                        <select name="regional_ids" id="regional_ids" class="form-control">
                            @foreach($region as $value)
                            <option value="{{$value->id}}">{{$value->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" onclick="cleanInputs()" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="buttonEdit" class="btn btn-primary">Actualizar</button>
            </div>
        </div>
    </div>
</div>



@stop
@section('lowerScripts')
<!-- Owl JavaScript -->
<script src="{{ asset('vendors/owl.carousel/dist/owl.carousel.min.js')}}"></script>

<!-- Owl Init JavaScript -->
<script src="{{ asset('dist/js/owl-data.js')}}"></script>
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{ asset('vendors/jszip/dist/jszip.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<!-- Toastr JS -->
<script src="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>


<script src="/lowerScripts/complex.js"></script>
@stop