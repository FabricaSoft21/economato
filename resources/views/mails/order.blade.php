<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Nuevo pedido</title>
</head>
<body>
    <p>Hola! acaban de aprobar el pedido de  {{$email->user}}.</p>
    <p>Caracterìsticas del pedido:</p>
    <ul>
        <li>Instructor: {{$email->user}}</li>
        <li>Fecha: {{$email->date}}</li>
        <li>Costo: $ {{$email->cost}}</li>
    </ul>
    <ul>
        <li>
            <a>
                Ver en Economato
            </a>
        </li>
    </ul>
</body>
</html>