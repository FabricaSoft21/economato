@extends('layouts.app')
@section('title')
403
@parent
@stop
@section('topScripts')
<link type="text/css" rel="stylesheet" href="{{asset('css/errors.css')}}" />

@endsection
@section('content')
    	<div id="notfound">
		<div class="notfound">
			<div style="width: 100%;" class="notfound-404">
				<h1 style="width: 100%;">:(</h1>
			</div>
			<h2>403- Acceso denegado</h2>
			<p>Lo sentimos no tienes permisos para entrar a este sitió</p>
        <a href="{{route('login')}}">Volver al inicio</a>
		</div>
	</div>
@endsection