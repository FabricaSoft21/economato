@extends('layouts.app')

@section('title')
Gestion de usuarios
@parent
@stop

@section('topScripts')
<!-- Data Table CSS -->
<link href="{{asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Toastr CSS -->
<link href="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-refresh">
            <div class="card-header">
                <div class="refresh-container">
                    <div class="loader-pendulums"></div>
                </div>
                <div class="card-header card-header-action">
                    <h4>usuarios registrados</h4>
                    <div class="d-flex align-items-center card-action-wrap">
                        <a class="inline-block mr-15" data-toggle="collapse" href="#collapse_1" aria-expanded="true">
                            <i class="zmdi zmdi-chevron-down"></i>
                        </a>
                        <a onclick="listUsers()" class="inline-block refresh mr-15">
                            <i class="ion ion-md-radio-button-off"></i>
                        </a>
                        <a href="#" class="inline-block full-screen mr-15">
                            <i class="ion ion-md-expand"></i>
                        </a>
                        <a class="inline-block card-close" href="#" data-effect="fadeOut">
                            <i class="ion ion-md-close"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div id="callapse_1" class="collapse show">
                <div class="card-body">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#register">
                        Nuevo <i class="fa fa-plus "></i>
                    </button>
                    <br>
                    <hr>
                    <br>
                    <div class="table-wrap">
                        <table id="UsersTable" class="table table-ligh t table-hover">
                            <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Nombre</th>
                                    <th>Rol</th>
                                    <th>Correo</th>
                                    <th>Estado</th>
                                </tr>
                            </thead>
                            <tbody id="tableUsersBody">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Registrar usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Nombre</label>
                        <input type="text" id="name" class="form-control">
                        <br>
                    </div>

                    <div class="col-md-12">
                        <label for="">Apellidos</label>
                        <input type="text" id="lastname" class="form-control">
                        <br>
                    </div>

                    <div class="col-md-12">
                        <label for="">Número de identificación</label>
                        <input type="number" id="identification" class="form-control">
                        <br>
                    </div>

                    <div class="col-md-12">
                        <label for="">Correo electrónico</label>
                        <input type="email" id="email" class="form-control">
                        <br>
                    </div>

                    <div class="col-md-12">
                        <label for="">Número de teléfono</label>
                        <input type="number" id="phone" class="form-control">
                        <br>
                    </div>

                    <div class="col-md-12">
                        <label for="">Rol</label>
                        <select name="" id="role_id" class="form-control">
                            <option value="Instructores">Instructor</option>
                            <option value="Administrador">Administrador</option>
                            <option value="Bodega">Bodega</option>
                            <option value="Coordinación">Coordinación</option>
                            <option value="Lider-Produccion-centro">Lider de producción</option>
                            <option value="Presupuestal">Presupuestal</option>
                        </select>
                    </div>
                </div>


                <hr>

                <hr>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="cleanInputs()">Cancelar</button>
                <button type="button" onclick="register()" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>


@stop
@section('lowerScripts')
<script src="{{ asset('vendors/owl.carousel/dist/owl.carousel.min.js')}}"></script>

<!-- Owl Init JavaScript -->
<script src="{{ asset('dist/js/owl-data.js')}}"></script>
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{ asset('vendors/jszip/dist/jszip.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<!-- Toastr JS -->
<script src="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
<script src="{{asset('lowerScripts/user.js')}}"></script>
@endsection