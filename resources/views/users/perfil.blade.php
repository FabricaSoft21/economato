@extends('layouts.app')

@section('title')
Mi Perfil
@parent
@stop

@section('topScripts')
<link href="{{asset('vendors/dropify/dist/css/dropify.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <span>Información de Perfil</span>
                </div>
                <div class="card-body">
                    <div class="col-md-10 col-md-offset-1" id="photo">

                        <img src="/uploads/perfil/{{$user->foto}}" style="width: 150px; height: 150px;  float: left; border-radius: 50%; margin-right: 25px;" alt="">

                        <h2>{{$user->name}}</h2>
                        <span> {{$user->email}}</span>
                    </div>
                    @if(count($errors) > 0)

                    @foreach($errors as $error)
                    <p>{{$error}}</p>
                    @endforeach

                    @endif
                    <br>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                            Cambiar foto
                    </button>
                   
                </div>
            </div>
        </div>
        <div class="col-md-6">
             <div class="card pl-2 pr-2 py-2">
                <div class="card-header">
                <h4>Cambiar credenciales</h4>
                </div>
                <form action="{{route('user.changePassword')}}" method="POST">
                @csrf
                        <label for="contraseña_actual">Contraseña Actual</label>
                            <input class="form-control{{ $errors->has('contraseña_actual') ? ' is-invalid' : '' }}" type="password" name="contraseña_actual" id="contraseña_actual" value="" />
                            @if ($errors->has('contraseña_actual'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('contraseña_actual') }}</strong>
                                        </span>
                            @endif

                            <label for="nueva_contraseña">Nueva Contraseña</label>
                            <input class="form-control{{ $errors->has('nueva_contraseña') ? ' is-invalid' : ''  }}" type="password" name="nueva_contraseña" id="nueva_contraseña" value="" />
                            @if ($errors->has('nueva_contraseña'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('nueva_contraseña') }}</strong>
                                        </span>
                            @endif

                            <label for="confirmacion_contraseña">Confirmar </label>
                            <input class="form-control{{ $errors->has('confirmacion_contraseña') ? ' is-invalid' : ''  }}" type="password" name="confirmacion_contraseña" id="confirmacion_contraseña" value="" />
                            @if ($errors->has('confirmacion_contraseña'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('confirmacion_contraseña') }}</strong>
                                        </span>
                            @endif
                            <br> 
                            <div style="text-align: right;">
                                <button type="submit" class="btn btn-primary">Actualizar</button>
                            </div>
                </form>
                <br>
            </div>
        </div>

    </div>
</div>
{{--Fotografia de perfil--}}
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Actualizar foto de perfil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{route('perfil')}}" enctype="multipart/form-data" id="upload-profile">
          @csrf
                       
        <input type="file" id="input-file-now" class="dropify"  name="foto"  data-allowed-file-extensions = "jpg png" />
       
        <br>
         </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="upload-photo" >Guardar</button>
      </div>
    </div>
  </div>
</div>

@stop
@section('lowerScripts')
<script src="{{asset('vendors/dropify/dist/js/dropify.min.js')}}"></script>
<script>
$(document).ready(function () { 
    //manejo de archivos con dropify
    $('#input-file-now').dropify({
        messages: {
            'default': 'Arrastra y suelta una fotografia aquí o haz clic',
            'replace': 'Arrastra y suelta o haz clic para reemplazar',
            'remove': 'Remover',
            'error': 'Ooops, sucedió algo malo.'
        }
    });
});
$('#upload-photo').click(()=>{
    $('#upload-profile').submit();
});

</script>
@endsection

