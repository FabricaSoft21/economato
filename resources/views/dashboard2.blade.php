@extends('layouts.app')

@section('content')

@section('title')
Dashborad
@parent
@stop

    <!-- Container -->
    <div class="container-fluid mt-xl-50 mt-sm-30 mt-15">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <div class="hk-row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="card card-sm text-center">
                            <div class="card-body">
                                <span class="d-block font-11 font-weight-500 text-dark text-uppercase mb-10"><i data-feather="users"></i> Usuarios registrados</span>
                                <div class="d-flex align-items-center justify-content-between position-relative">
                                    <div class="w-100">
                                        <span class="d-block text-center display-5 font-weight-400 text-dark">{{$nUsers}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card card-sm text-center">
                            <div class="card-body">
                                <span class="d-block font-11 font-weight-500 text-dark text-uppercase mb-10"><i data-feather="file-text"></i> Contratos realizados</span>
                                <div class="d-flex align-items-center justify-content-between position-relative">
                                    <div class="w-100">
                                        <span class="d-block">
                                            <span class="display-5 text-center font-weight-400 text-dark"><span class="counter-anim">{{$nContracts}}</span></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card card-sm text-center">
                            <div class="card-body">
                                <span class="d-block font-11 font-weight-500 text-dark text-uppercase mb-10"><i data-feather="droplet"></i> Recetas registradas</span>
                                <div class="d-flex align-items-end justify-content-between">
                                    <div class="w-100">
                                        <span class="d-block">
                                            <span class="display-5 text-center font-weight-400 text-dark">{{$nRecipes}}</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card card-sm text-center">
                            <div class="card-body">
                                <span class="d-block font-11 font-weight-500 text-dark text-uppercase mb-10"><i data-feather="home"></i>Complejos</span>
                                <div class="d-flex align-items-end justify-content-between">
                                    <div class="w-100">
                                        <span class="d-block">
                                            <span class="display-5 text-center font-weight-400 text-dark">{{$nComplexes}}</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="hk-row">
                        @role('Super-Admin')
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-header card-header-action">

                               @if($contractA!=null)
                                <h6>Contrato actual #<span>{{$contractA->number}}</span></h6>
                                <div class="d-flex align-items-center card-action-wrap">
                                    <div class="d-flex align-items-center card-action-wrap">
                                        <a target="_blank" href="/uploads/contracts/{{$contractA->contracts_url}}" class="inline-block mr-15">
                                            <i class="ion ion-md-arrow-down"></i>
                                        </a>
                                        <a class="inline-block card-close" href="#" data-effect="fadeOut">
                                            <i class="ion ion-md-close"></i>
                                        </a>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="card-body">
                            @if($contractA!=null)
                                    <p>{{$contractA->provider->name}} {{$contractA->provider->last_name}}</p>
                                    <p> NIT:{{$contractA->provider->NIT}}</p>
                                    <p> Telefono: {{$contractA->provider->phone}}</p>
                                    <p> Email: {{$contractA->provider->email}}</p>
                                 
                            @else

                                <div class="alert alert-light" role="alert">
                                    No hay contrato actualmente!
                                </div>

                            @endif


                            </div>
                        </div>
                    </div>
                    @endrole
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-header card-header-action">
                                <h6>Presupuesto</h6>
                                <div class="d-flex align-items-center card-action-wrap">
                                    <div class="inline-block dropdown">
                                        <a class="dropdown-toggle no-caret" data-toggle="dropdown" href="#" aria-expanded="false" role="button"><i class="ion ion-ios-more"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="{{route('budgets')}}">Ver información de presupuestos.</a>
                                            @if($budget!=null)<a class="dropdown-item" href="{{route('budget', ['id' => $budget->id])}}">Ver presupuesto actual.</a>@endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="hk-legend-wrap mb-20">
                                    <div class="hk-legend">

                                    @if($budget!=null)
                                        <input type="hidden" value="{{$budget->id}}" id="budget_id"/>
                                        @foreach($budget->budgetsXcharacterizations as $budgetXcharacterization)
                                        <span class="d-10 bg-indigo-light-2 rounded-circle d-inline-block"></span><span>${{number_format($budgetXcharacterization->value)}} pesos para</span> <span> {{$budgetXcharacterization->characterization->name}} con representacion del: {{$budgetXcharacterization->porcentage}}%</span>
                                        <br>
                                        <hr>
                                        @endforeach

                                        <br>
                                        totalidad: <h5>${{number_format($budget->remaining)}}</h5>

                                        @else
                                            <div class="alert alert-light" role="alert">
                                                 No se ha registrado el presupuesto de este año
                                            </div>
                                    @endif
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-header card-header-action">
                                <h6>Representación del presupuesto</h6>
                                <div class="d-flex align-items-center card-action-wrap">
                                    <div class="inline-block dropdown">
                                        <a class="dropdown-toggle no-caret" data-toggle="dropdown" href="#" aria-expanded="false" role="button"><i class="ion ion-ios-more"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                      
                                            <div class="dropdown-divider"></div>
                                           @if($budget!=null) <a class="dropdown-item" href="{{route('budget', ['id' => $budget->id])}}">Ver presupuesto aportado</a> @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div id="e_chart_9" class="echart" style="height:291px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <br>
                <hr>
                <h1 class="text-center">Productos inhabilitados ({{count($products)}})</h1>
                <hr>
                <br>
                <div class="card">
                    <div class="card-body pa-0">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table class="table table-sm table-hover mb-0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th>Tipo de producto</th>
                                            <th>Presentación</th>
                                            <th>Unidad de medida</th>
                                            <th>Fecha de actualización</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $n = 1 @endphp
                                        @foreach($products as $product)
                                        <tr>
                                            <td><p class="text-monospace">{{$n++}}</p></td>
                                            <td>{{$product->name}}</td>
                                            <td>{{$product->typeProduct->name}}</td>
                                            <td>{{$product->presentationProduct->name}}</td>
                                            <td><span class="badge badge-secondary">{{$product->measureUnit->name}}</span></td>
                                            <td><span class="d-flex align-items-center"><i class="zmdi zmdi-time-restore font-25 mr-10 text-light-40"></i><span>{{$product->updated_at}}</span></span></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->

@stop

@section('lowerScripts')

<script src="/lowerScripts/budget_detail.js"></script>
<script>



$(document).ready(function () {

  $.ajax({
      url:'/contracts/vigente/Validate',
      success:(data)=>{
        //  se comenta 
        if (data.status) {

            if (data.data > 0) {
                toastr('info','Contrato Finalizado','El contrato vigente ha finalizado');
            }
        }
        //else{
        //      console.log(data);
        // }
      }
  })


});
function toastr(type, tittle, text) {
    $.toast({
        heading: '' + tittle + '',
        text: '' + text + '',
        position: 'top-right',
        loaderBg: '#000',
        class: 'jq-has-icon jq-toast-' + type,
        hideAfter: 3500,
        stack: 6,
        showHideTransition: 'fade'
    });
}
</script>
@endsection