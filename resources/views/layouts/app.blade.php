<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework" />
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @section('title')
        | Economato
        @show
    </title>
    <script>
        //se globaliza la variable para acceder a la ruta desde js
        var url = '{{env("APP_URL")}}';
    </script>


    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->

    <!-- Favicon
    <link rel="shortcut icon" href="icon/favicon.ico" />
    <link rel="icon" href="icon/favicon.ico" type="image/x-icon" /> -->

    <!-- Icono de la pestaña -->

    <link rel="apple-touch-icon" sizes="57x57" href="icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="icon/favicon-16x16.png">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->


    <!-- Morris Charts CSS -->
    <link href="{{asset('vendors/morris.js/morris.css')}}" rel="stylesheet" type="text/css" />

    <!-- Toggles CSS -->
    <link href="{{asset('vendors/jquery-toggles/css/toggles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('vendors/jquery-toggles/css/themes/toggles-light.css')}}" rel="stylesheet" type="text/css">

    <!-- Toastr CSS -->
    <link href="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link href="{{asset('dist/css/style.css')}}" rel="stylesheet" type="text/css">
    @yield('topScripts')
</head>


<body>
    <!-- Preloader -->
    <div class="preloader-it">
        <div class="loader-pendulums"></div>
    </div>
    <!-- /Preloader -->

    <!-- HK Wrapper -->
    <div class="hk-wrapper hk-vertical-nav">

        <!-- Top Navbar -->
        <nav class="navbar navbar-expand-xl navbar-light fixed-top hk-navbar">
            @auth
            <a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);"><span class="feather-icon"><i data-feather="menu"></i></span></a>
            @endauth
            <a class="navbar-brand" href="/home">
                <img class="brand-img d-inline-block" src="{{asset('dist/img/logo-light.png')}}" alt="brand" style="width: 80px; height: 50px;" />
            </a>

            <ul class="navbar-nav hk-navbar-content">
                @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
                @endif

                @else

                <li class="nav-item">
                    <!-- <a id="navbar_search_btn" class="nav-link nav-link-hover" href="javascript:void(0);"><span class="feather-icon"><i data-feather="search"></i></span></a> -->
                </li>

                <li class="nav-item">
                    <a id="settings_toggle_btn" class="nav-link nav-link-hover" href="{{route('perfil')}}"><span class="feather-icon"><i data-feather="settings"></i></span></a>
                </li>

                <!-- Notificaciones -->
                <li class="nav-item dropdown dropdown-notifications">
                    <a onclick="deleteNotify()" class="nav-link dropdown-toggle no-caret" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="feather-icon"><i data-feather="bell"></i></span><span class="badge-wrap"><span class="badge badge-primary badge-indicator badge-indicator-sm badge-pill pulse" id="notify"></span></span></a>
                    <div class="dropdown-menu dropdown-menu-right" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                        <h6 class="dropdown-header">Notifications <a href="javascript:void(0);" class="" onclick="viewAllNotifications()">View all</a></h6>
                        <div class="notifications-nicescroll-bar" id="notifications">
                            <a href="javascript:void(0);" class="dropdown-item">
                                <div class="media">
                                    <div class="media-body">
                                        <div>
                                            <div class="notifications-text"><span class="text-dark text-capitalize">Evie Ono</span> accepted your invitation to join the team</div>
                                            <div class="notifications-time">12m</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="javascript:void(0);" class="dropdown-item">
                                <div class="media">
                                    <div class="media-body">
                                        <div>
                                            <div class="notifications-text">New message received from <span class="text-dark text-capitalize">Misuko Heid</span></div>
                                            <div class="notifications-time">1h</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="javascript:void(0);" class="dropdown-item">
                                <div class="media">
                                    <div class="media-body">
                                        <div>
                                            <div class="notifications-text">You have a follow up with<span class="text-dark text-capitalize"> Mintos head</span> on <span class="text-dark text-capitalize">friday, dec 19</span> at <span class="text-dark">10.00 am</span></div>
                                            <div class="notifications-time">2d</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="javascript:void(0);" class="dropdown-item">
                                <div class="media">
                                    <div class="media-body">
                                        <div>
                                            <div class="notifications-text">Application of <span class="text-dark text-capitalize">Sarah Williams</span> is waiting for your approval</div>
                                            <div class="notifications-time">1w</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="javascript:void(0);" class="dropdown-item">
                                <div class="media">
                                    <div class="media-body">
                                        <div>
                                            <div class="notifications-text">Last 2 days left for the project</div>
                                            <div class="notifications-time">15d</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </li>

                <!-- Navbar Horizontal Opciones de sesión -->
                <li class="nav-item dropdown dropdown-authentication">
                    <a class="nav-link dropdown-toggle no-caret" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media">
                            <div class="media-img-wrap">
                                <div class="avatar">
                                    <img src="/uploads/perfil/{{auth::user()->foto}}" alt="user" class="avatar-img rounded-circle">
                                </div>
                                <span class="badge badge-success badge-indicator"></span>
                            </div>
                            <div class="media-body">
                                <span>{{auth::user()->name}}<i class="zmdi zmdi-chevron-down"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                        <a class="dropdown-item" href="{{route('perfil')}}"><i class="dropdown-icon zmdi zmdi-account"></i><span>Mi Perfil</span></a>

                        <div class="dropdown-divider"></div>

                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="dropdown-icon zmdi zmdi-power"></i>
                            <span>{{ __('Logout') }}</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>

            </ul>
        </nav>

        <!-- Buscador -->
        <form role="search" class="navbar-search">
            <div class="position-relative">
                <a href="javascript:void(0);" class="navbar-search-icon"><span class="feather-icon"><i data-feather="search"></i></span></a>
                <input type="text" name="example-input1-group2" class="form-control" placeholder="Type here to Search">
                <a id="navbar_search_close" class="navbar-search-close" href="#"><span class="feather-icon"><i data-feather="x"></i></span></a>
            </div>
        </form>
        <!-- /Top Navbar -->




        <!-- navbar Vertical cuando se está logueado -->
        <nav class="hk-nav hk-nav-light">
            <a href="javascript:void(0);" id="hk_nav_close" class="hk-nav-close"><span class="feather-icon"><i data-feather="x"></i></span></a>
            <div class="nicescroll-bar">
                <div class="navbar-nav-wrap">

                    <div class="nav-header">
                        <span>Menú de navegación</span>
                        <span>ECM</span>
                    </div>
                    <ul class="navbar-nav flex-column">
                        @role('Super-Admin|Presupuestal|Bodega|Coordinación')
                        <li class="nav-item">
                            <a class="{{request()->is('home') ? 'nav-link activate' : 'nav-link'}}" href="{{route('home')}}">
                                <span class="feather-icon"><i data-feather="home"></i></span>
                                <span class="nav-link-text">Inicio</span>
                            </a>
                        </li>
                        @endrole
                        @role('Super-Admin|Presupuestal|Bodega')
                        <li class="nav-item">
                            <a class="{{request()->is('budget') ? 'nav-link activate' : 'nav-link'}}" href="{{route('budgets')}}">
                                <span class="feather-icon"><i data-feather="dollar-sign"></i></span>
                                <span class="nav-link-text">Presupuesto Actual</span>
                            </a>
                        </li>
                        @endrole

                        @role('Super-Admin|Presupuestal')
                        <li class="nav-item">
                            <a class="{{request()->is('budget') ? 'nav-link activate' : 'nav-link'}}" href="{{route('budgets')}}">
                                <span class="feather-icon"><i data-feather="clock"></i></span>
                                <span class="nav-link-text">Presupuestos pasados</span>
                            </a>
                        </li>
                        @endrole
                        @role('Super-Admin|Bodega')
                        <li class="nav-item">
                            <a class="{{request()->is('contracts') ? 'nav-link activate' : 'nav-link'}}" href="{{route('contracts')}}">
                                <span class="feather-icon"><i data-feather="file-text"></i></span>
                                <span class="nav-link-text">Contratos</span>
                            </a>
                        </li>
                        @endrole
                        @role('Super-Admin|Administrador|Bodega')
                        <li class="nav-item">
                            <a class="{{request()->is('provider') ? 'nav-link activate' : 'nav-link'}}" href="{{route('provider.index')}}">
                                <span class="feather-icon"><i data-feather="users"></i></span>
                                <span class="nav-link-text">Proveedores</span>
                            </a>
                        </li>
                        @endrole
                        @role('Super-Admin|Coordinación|Instructores|Bodega')
                        <li class="nav-item">
                            <a class="{{request()->is('request') ? 'nav-link activate' : 'nav-link'}}" href="{{url('request')}}">
                                <span class="feather-icon"><i data-feather="list"></i></span>
                                <span class="nav-link-text">Pedidos</span>
                            </a>
                        </li>
                        @endrole
                        @role('Lider-Produccion-centro|Coordinación')
                        <li class="nav-item">
                            <a class="{{request()->is('produccioncenter') ? 'nav-link activate' : 'nav-link'}}" href="{{url('produccioncenter')}}">
                                <span class="feather-icon"><i data-feather="list"></i></span>
                                <span class="nav-link-text">Produccion de centro</span>
                            </a>
                        </li>
                        @endrole
                        @role('Super-Admin|Administrador|Bodega|Coordinación|Instructores|Lider-Produccion-centro')
                        <li class="nav-item">
                            <a class="{{request()->is('product') ? 'nav-link activate' : 'nav-link'}}" href="{{url('product')}}">
                                <span class="feather-icon"><i data-feather="tag"></i></span>
                                <span class="nav-link-text">Productos</span>
                            </a>
                        </li>
                        @endrole
                        @role('Super-Admin|Administrador')
                        <li class="nav-item">
                            <a class="{{request()->is('region') ? 'nav-link activate' : 'nav-link'}}" href="{{route('region')}}">
                                <span class="feather-icon"><i data-feather="map"></i></span>
                                <span class="nav-link-text">Regionales</span>
                            </a>
                        </li>
                        @endrole
                        @role('Super-Admin|Administrador')
                        <li class="nav-item">
                            <a class="{{request()->is('complex') ? 'nav-link activate' : 'nav-link'}}" href="{{route('complex')}}">
                                <span class="feather-icon"><i data-feather="map-pin"></i></span>
                                <span class="nav-link-text">Complejos</span>
                            </a>
                        </li>
                        @endrole
                        @role('Super-Admin|Administrador')
                        <li class="nav-item">
                            <a class="{{request()->is('center') ? 'nav-link activate' : 'nav-link'}}" href="{{route('center.index')}}">
                                <span class="feather-icon"><i data-feather="shield"></i></span>
                                <span class="nav-link-text">Centro</span>
                            </a>
                        </li>
                        @endrole
                        @role('Super-Admin|Administrador')
                        <li class="nav-item">
                            <a class="{{request()->is('program') ? 'nav-link activate' : 'nav-link'}}" href="{{route('program.index')}}">
                                <span class="feather-icon"><i data-feather="layers"></i></span>
                                <span class="nav-link-text">Programas de formación</span>
                            </a>
                        </li>
                        @endrole
                        @role('Super-Admin|Administrador')
                        <li class="nav-item">
                            <a class="{{request()->is('teacher') ? 'nav-link activate' : 'nav-link'}}" href="{{route('teacher')}}">
                                <span class="feather-icon"><i data-feather="user"></i></span>
                                <span class="nav-link-text">Instructores</span>
                            </a>
                        </li>
                        @endrole
                        @role('Super-Admin|Administrador')
                        <li class="nav-item">
                            <a class="{{request()->is('file') ? 'nav-link activate' : 'nav-link'}}" href="{{route('file.index')}}">
                                <span class="feather-icon"><i data-feather="file"></i></span>
                                <span class="nav-link-text">Fichas</span>
                            </a>
                        </li>
                        @endrole
                        @role('Super-Admin|Instructores')
                        <li class="nav-item">
                            <a class="{{request()->is('recipe') ? 'nav-link activate' : 'nav-link'}}" href="{{route('recipes.index')}}">
                                <span class="feather-icon"><i data-feather="folder"></i></span>
                                <span class="nav-link-text">Recetas</span>
                            </a>
                        </li>
                        @endrole

                        @role('Super-Admin')
                        <li class="nav-item">
                            <a class="{{request()->is('characterization') ? 'nav-link activate' : 'nav-link'}}" href="{{route('characterization')}}">
                                <span class="feather-icon"><i data-feather="percent"></i></span>
                                <span class="nav-link-text">Caracterizaciones </span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="{{request()->is('users/manage') ? 'nav-link activate' : 'nav-link'}}" href="{{route('users.manage')}}">
                                <span class="feather-icon"><i data-feather="user-plus"></i></span>
                                <span class="nav-link-text">Usuarios</span>
                            </a>
                        </li>


                        @endrole

                        @role('Coordinación')
                        <li class="nav-item">
                            <a class="{{request()->is('produccioncenter/approved') ? 'nav-link activate' : 'nav-link'}}" href="{{route('produccioncenter.approved')}}">
                                <span class="feather-icon"><i data-feather="clock"></i></span>
                                <span class="nav-link-text">Historial de pedidos</span>
                            </a>
                        </li>
                        @endrole

                        <!-- super-Admin  -->

                    </ul>
                    @endguest
                </div>
            </div>
        </nav>



        <div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>
        <!-- /Vertical Nav -->
        <div id="app">
            <main class="py-4">
                <!-- Main Content -->
                <div class="hk-pg-wrapper">
                    <!-- Container -->
                    <div class="container-fluid mt-xl-50 mt-sm-30 mt-15">
                        <!-- Contenido -->
                        @yield('content')
                        <!-- /Contenido -->
                    </div>
                    <!-- /Container -->

                    <!-- Footer -->
                    <div class="hk-footer-wrap container-fluid">
                        <footer class="footer">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <p>SENA - CESGE<a href="http://fabricadesoftware.co/" class="text-dark" target="_blank">Fabrica de Software</a> ©2019</p>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <p class="d-inline-block">Redes sociales</p>
                                    <a href="http://fabricadesoftware.co/" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                                    <a href="https://twitter.com/FS_CESGE/" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                                    <a href="https://www.instagram.com/fabricadesoftwaremedellin/?igshid=17vrvbig3j8tl" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-instagram"></i></span></a>
                                </div>
                            </div>
                        </footer>
                    </div>
                    <!-- /Footer -->
                </div>
                <!-- /Main Content -->
            </main>
        </div>




    </div>
    <!-- /HK Wrapper -->



    <!-- scripts plantilla -->
    <!-- jQuery -->
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('vendors/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- Slimscroll JavaScript -->
    <script src="{{asset('dist/js/jquery.slimscroll.js')}}"></script>

    <!-- Fancy Dropdown JS -->
    <script src="{{asset('dist/js/dropdown-bootstrap-extended.js')}}"></script>

    <!-- FeatherIcons JavaScript -->
    <script src="{{asset('dist/js/feather.min.js')}}"></script>

    <!-- Toggles JavaScript -->
    <script src="{{asset('vendors/jquery-toggles/toggles.min.js')}}"></script>
    <script src="{{asset('dist/js/toggle-data.js')}}"></script>

    <!-- Toastr JS -->
    <script src="{{asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>

    <!-- Counter Animation JavaScript -->
    <script src="{{asset('vendors/waypoints/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('vendors/jquery.counterup/jquery.counterup.min.js')}}"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{asset('vendors/raphael/raphael.min.js')}}"></script>
    <script src="{{asset('vendors/morris.js/morris.min.js')}}"></script>

    <!-- Easy pie chart JS -->
    <script src="{{asset('vendors/easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>

    <!-- Flot Charts JavaScript -->
    <script src="{{asset('vendors/flot/excanvas.min.js')}}"></script>
    <script src="{{asset('vendors/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('vendors/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('vendors/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('vendors/flot/jquery.flot.time.js')}}"></script>
    <script src="{{asset('vendors/flot/jquery.flot.stack.js')}}"></script>
    <script src="{{asset('vendors/flot/jquery.flot.crosshair.js')}}"></script>
    <script src="{{asset('vendors/jquery.flot.tooltip/js/jquery.flot.tooltip.min.js')}}"></script>

    <!-- EChartJS JavaScript -->
    <script src="{{asset('vendors/echarts/dist/echarts-en.min.js')}}"></script>

    <!-- Init JavaScript -->
    <script src="{{asset('dist/js/init.js')}}"></script>
    <script src="{{asset('dist/js/dashboard2-data.js')}}"></script>

    <script src="{{asset('vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('vendors/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('lowerScripts/main.js')}}"></script>
    @yield('lowerScripts')

</body>

</html>