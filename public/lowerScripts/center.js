var table = false;
var arrayPrograms = [];

$(document).ready(function () {
    ListCenters();
    $("#show").on("hidden.bs.modal", function () {
        cleanInputsUpdate();
    });
    $("#register").on("hidden.bs.modal", function () {
        cleanInputs();
    });
});

function applyDataTable() {
    $("#CenterTable").DataTable({
        responsive: true,
        autoWidth: false,
        language: {
            search: "",
            searchPlaceholder: "Buscar",
            sLengthMenu: "_MENU_items"
        }
    });

    table = true;
}

function ListCenters() {
    $("#tableCenterBody").html("");
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/center/data",
        type: "GET",
        datatype: "json",
        success: function (response) {
            if (response.ok == false) {
                toastr("info", "Atención", response.message);
            } else {
                response.data.forEach(function (e, i) {
                    if (e.status == 0) {
                        var status =
                            '<button type="button" onclick="change_state(' +
                            e.id +
                            ')" class="btn btn-success btn-rounded btn-xs">Activo</button>';
                    } else {
                        var status =
                            '<button type="button" onclick="change_state(' +
                            e.id +
                            ')" class="btn btn-danger btn-rounded btn-xs">Inactivo</button>';
                    }
                    $("#tableCenterBody").append(
                        "<tr>" +
                        "<th>" + (i + 1) + "</th>" +
                        "<td>" + e.name_center + "</td>" +
                        "<td>" + e.name_complex + ' - ' + e.name_region + "</td>" +
                        "<td>" + e.address + "</td>" +
                        "<td>" + status + "</td>" +
                        "<td>" +
                        '<button type="button" onclick="edit(' +e.id +
                        ')" class="btn btn-outline-success btn-icon btn-icon-circle btn-success btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-pencil"></i></span></button>' +
                        '<button type="button" onclick="show(' +e.id +
                        ')" class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-eye"></i></span></button>' +
                        "</td>" +
                        "</tr>"
                    );
                });

                if (table == false) {
                    applyDataTable();
                }
            }
        }
    });
}
function change_state(id) {
    console.log(id);
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/center/" + id,
        type: "DELETE",
        datatype: "json",
        success: function (response) {
            if (response.ok == true) {
                ListCenters();
                toastr("success", "Atención", response.message);
            }
        }
    });
}
function register() {
    var name = $("#name").val();
    var complex = $("#complex_id").val();
    var address = $("#address").val()

    if (arrayPrograms.length==0) {
        toastr("danger", "Atención", 'Debes agregar como mínimo un programa de formación');
    } else {
        $.ajax({
            url: "/center",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            type: "POST",
            data: {
                'name': name,
                'complex_id': complex,
                'address': address,
                'programs':arrayPrograms,
            },
            dataType: "json",
            success: function (response) {
                if (response.ok == true) {
                    toastr("success", "Atención", response.message);
                    $("#register").modal("toggle");
                    cleanInputs();
                    ListCenters();
                } else if (response.ok == false) {
                    if (response.message) {
                        listAlerts(response.message);
                    } else {
                        toastr("danger", "Atención", response.error);
                    }
                }
            },
        });
    }
}
function cleanInputs() {
    $("#name").val("");
    $("#name").removeClass("is-invalid");
    $("#address").val("");
    $("#address").removeClass("is-invalid");
    $("#search").val("");
    $("#results").html("");
    arrayPrograms=[];
    listPrograms();
}
function cleanInputsUpdate() {
    $("#name").val("");
    $("#name").removeClass("is-invalid");
    $("#nameShow").val("");
    $("#nameShow").removeClass("is-invalid");
    $("#address").val("");
    $("#address").removeClass("is-invalid");
    $("#addressShow").val("");
    $("#addressShow").removeClass("is-invalid");
    $("#searchShow").val("");
    $("#resultsShow").html("");
    arrayPrograms=[];
    listProgramsUpdate();
}
function listAlerts(e) {
    if (e.name) {
        toastr("danger", "Error", e.name[0]);
        $("#name").addClass("is-invalid");
    }
    if (e.address) {
        toastr("danger", "Error", e.address[0]);
        $("#address").addClass("is-invalid");
    }
}
function searchPrograms(value) {
    $("#results").html('');
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: '/search/program/' + value,
        type: 'get',
        datatype: 'json',
        success: function (r) {
            if (r.ok == false) {
                $("#results").append(`<li class="list-group-item list-group-item-danger">${r.message}</li>`)
            } else if (r.ok == true) {
                r.data.forEach(function (e, i) {
                    $("#results").append(`  <li class="list-group-item d-flex justify-content-between align-items-center">
                                                ${e.name}
                                                <span><button class="btn btn-success  btn-xs" onclick="addProgram(${e.id},'${e.name}')"><i class="glyphicon glyphicon-plus"></i></button></span>
                                            </li>`)
                });
            }
        }
    });
}
function searchProgramsUpdate(value) {
    $("#resultsShow").html('');
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: '/search/program/' + value,
        type: 'get',
        datatype: 'json',
        success: function (r) {
            if (r.ok == false) {
                $("#resultsShow").append(`<li class="list-group-item list-group-item-danger">${r.message}</li>`)
            } else if (r.ok == true) {
                r.data.forEach(function (e, i) {
                    let program = JSON.stringify(e);
                    console.log(program)
                    $("#resultsShow").append(`  <li class="list-group-item d-flex justify-content-between align-items-center">
                                                ${e.name}
                                                <span><button class="btn btn-success  btn-xs" onclick='addProgramUpdate(${program})'><i class="glyphicon glyphicon-plus"></i></button></span>
                                            </li>`)
                });
            }
        }
    });
}
function addProgram(id, name) {

    let ok = true

    arrayPrograms.forEach(function(e,i){
        if (e.name == name) {
            toastr("danger", "Atención", `${name} ya ha sido agregada`);
            ok = false
        } 
    });    
    
    if(ok==true){
        let program = new Object();

        program.id = id;
        program.name = name;
    
        arrayPrograms.push(program);
    
        listPrograms(); 
    }
}
function addProgramUpdate(program) {
    let ok = true

    arrayPrograms.forEach(function(e,i){
        if (e.id == program.id) {
            toastr("danger", "Atención", `${program.name} ya ha sido agregada`);
            ok = false
        } 
    });    
    
    if(ok==true){
    
        arrayPrograms.push(program);
    
        listProgramsUpdate(); 
    }
}
function listPrograms() {
    $("#bodyPrograms").html("");

    arrayPrograms.forEach(function (e, i) {
        $("#bodyPrograms").append(`<tr>
                                        <th scope="row">${(i + 1)}</th>
                                        <td>${e.name}</td>
                                        <td>
                                            <button class="btn btn-danger btn-sm" onclick="removeProgram(${i})"><i class="pe-7s-trash"></i></button>
                                        </td>
                                    </tr>`);
    });
}
function removeProgram(i) {
    arrayPrograms.splice(i, 1);
    listPrograms();
}
function listProgramsUpdate() {
    $("#bodyProgramsShow").html("");

    arrayPrograms.forEach(function (e, i) {
        $("#bodyProgramsShow").append(`<tr>
                                        <th scope="row">${(i + 1)}</th>
                                        <td>${e.name}</td>
                                        <td>
                                            <button class="btn btn-danger btn-sm" onclick="removeProgramUpdate(${i})"><i class="pe-7s-trash"></i></button>
                                        </td>
                                    </tr>`);
    });
}
function removeProgramUpdate(i) {
    arrayPrograms.splice(i, 1);
    listProgramsUpdate();
}
function show(id) {
    $.ajax({
        url:'/center/'+id,
        type:'get',
        datatype:'json',
        success:function(r){
            if(r.ok==true){
                // r.complex.forEach(function(e){
                //     if(e.id!=r.data.complex_id){
                //         $("#complex_idShow").append(`<option value="${e.id}">${e.name} -  ${e.region.name}</option>`);
                //     }else if(e.id==r.data.complex_id){
                //         $("#complex_idShow").prepend(`<option value="${e.id}">${e.name} -  ${e.region.name}</option>`);
                //     }
                // });
                $("#complexShow").show();
                $("#complex_idShow").hide();
                
                $("#show").modal('toggle');
                $("#nameView").html(r.data.name);
                $("#nameShow").val(r.data.name);
                $("#nameShow").prop('readonly',true);
                $("#complexShow").val(r.data.complex.name);
                $("#addressShow").val(r.data.address);
                $("#addressShow").prop('readonly',true);
                $("#bodyProgramsShow").html("");
                r.programs.forEach(function (e, i) {
                    $("#bodyProgramsShow").append(`<tr>
                                                    <th scope="row">${(i + 1)}</th>
                                                    <td>${e.program.name}</td>
                                                </tr>`);
                });
            }else if(r.ok==false){
                toastr("danger", "Error", e.message);
            }
        }
    });
}
function edit(id) {
    $.ajax({
        url:'/center/'+id,
        type:'get',
        datatype:'json',
        success:function(r){
            if(r.ok==true){
                $("#complexShow").hide();
                $("#complex_idShow").show();

                $("#complex_idShow").html('');
                r.complex.forEach(function(e){
                        $("#complex_idShow").append(`<option value="${e.id}" ${e.id==r.data.complex_id?'selected':''}>${e.name} -  ${e.region.name}</option>`);
                });
                $("#show").modal('toggle');
                $("#nameView").html(r.data.name);
                $("#nameShow").val(r.data.name);
                $("#nameShow").prop('readonly',false);
                $("#complexShow").val(r.data.complex.name);
                $("#addressShow").val(r.data.address);
                $("#addressShow").prop('readonly',false);
                $("#bodyProgramsShow").html("");
                $("#btnUpdate").attr('onclick',`update(${id})`)
                // arrayPrograms=r.programs;
                r.programs.forEach(function(e,i){
                    arrayPrograms.push(e.program);
                })
                listProgramsUpdate();
            }else if(r.ok==false){
                toastr("danger", "Error", e.message);
            }
        }
    });
}
function update(id) {
    var name = $("#nameShow").val();
    var complex = $("#complex_idShow").val();
    var address = $("#addressShow").val()

    if (arrayPrograms.length==0) {
        toastr("danger", "Atención", 'Debes agregar como mínimo un programa de formación');
    } else {
        $.ajax({
            url: "/center/"+id,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            type: "PATCH",
            data: {
                'name': name,
                'complex_id': complex,
                'address': address,
                'programs':arrayPrograms,
            },
            dataType: "json",
            success: function (response) {
                if (response.ok == true) {
                    toastr("success", "Atención", response.message);
                    $("#show").modal("toggle");
                    cleanInputsUpdate();
                    ListCenters();
                } else if (response.ok == false) {
                    if (response.message) {
                        listAlerts(response.message);
                    } else {
                        toastr("danger", "Atención", response.error);
                    }
                }
            },
        });
    }
}
function toastr(type, tittle, text) {
    $.toast({
        heading: "" + tittle + "",
        text: "" + text + "",
        position: "top-right",
        loaderBg: "#000",
        class: "jq-has-icon jq-toast-" + type,
        hideAfter: 3500,
        stack: 6,
        showHideTransition: "fade",
    });
}




