var table = false;
$(document).ready(function () {

    $('#contracts').DataTable({
        responsive: true,
        autoWidth: false,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });


});
function viewProducts(id) {
    $('#products-contract').html(`
    <table id="viewProductsTable" class="table table-hover ">
                  <thead >
                      <tr>
                          <th>Producto</th>
                          <th>Categoria</th>
                          <th>Presentación</th>
                          <th>Unidad de medida</th>
                          <th>IVA %</th>
                          <th>Precio unitario</th>
                          <th>Valor IVA</th>
                          <th>Total</th>
                      </tr>
                  </thead>
            </table>`);

    $('#viewProductsTable').DataTable({
        responsive: true,
        autoWidth: false,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        "ajax": "/contracts/products/view/" + id,
        "columns": [
            { "data": "product_name" },
            { "data": "category" },
            { "data": "presentation" },
            { "data": "measure" },
            { "data": "IVA" },
            { "data": "unit_price" },
            { "data": "tax_value" },
            { "data": "total_with_tax" }
        ]
    });

    setTimeout(() => {
        $('#modalProducts').modal('show');
    }, 100)

}


function viewPDF(pdfName) {
    window.open(url + "/uploads/contracts/" + pdfName, '_blank', 'fullscreen=yes');
}







