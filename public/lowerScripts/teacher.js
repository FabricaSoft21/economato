var table = false;

$(document).ready(function () {
    listTeachers();
});

function applyDataTable() {
    $("#teacherTable").DataTable({
        responsive: true,
        autoWidth: false,
        language: {
            search: "",
            searchPlaceholder: "Buscar",
            sLengthMenu: "_MENU_items"
        }
    });

    table = true;
}

function cleanInputs() {

    $("#name").val("");
    $("#name").removeClass("is-invalid");

    $("#lastname").val("");
    $("#lastname").removeClass("is-invalid");

    $("#identification").val("");
    $("#identification").removeClass("is-invalid");

    $("#phone").val("");
    $("#phone").removeClass("is-invalid");

    $("#email").val("");
    $("#email").removeClass("is-invalid");

    $("#password").val("");
    $("#password").removeClass("is-invalid");



}

function listTeachers() {
    $("#tableTeacherBody").html("");
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/teacher/data",
        type: "GET",
        datatype: "json",
        success: function (response) {
            console.log(response);
            if (response.ok == false) {
                toastr("info", "Atención", response.message);
            } else {
                response.data.forEach(function (e, i) {
                    if (e.status == 0) {
                        var state =
                            '<button type="button" onclick="change_state(' +
                            e.id +
                            ')" class="btn btn-success btn-rounded btn-xs" style="width: 67px;">Activo</button>';
                    } else {
                        var state =
                            '<button type="button" onclick="change_state(' +
                            e.id +
                            ')" class="btn btn-danger btn-rounded btn-xs" style="width: 67px;">Inactivo</button>';
                    }
                    $("#tableTeacherBody").append(
                        "<tr>" +
                        "<th>" +
                        (i + 1) +
                        "</th>" +
                        "<td>" +
                        e.name + ' ' + e.lastname +
                        "</td>" +
                        "<td>" +
                        e.identification +
                        "</td>" +
                        "<td>" +
                        e.phone +
                        "</td>" +
                        "<td>" +
                        e.email +
                        "</td>" +
                        "<td>" +
                        state +
                        "</td>" +
                        "<td>" +
                        '<button onclick="edit_teacher(' +
                        e.id +
                        ')" data-toggle="modal" data-target="#see_teacher"  class="btn btn-outline-success btn-icon btn-icon-circle btn-success btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-pencil"></i></span></button>' +

                        '<button onclick="see_teacher(' +
                        e.id +
                        ')" data-toggle="modal" data-target="#see_teacher"  class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-eye"></i></span></button>' +

                        "</tr>"
                    );
                });

                if (table == false) {
                    applyDataTable();
                }
            }
        }
    });
}

function change_state(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/teacher/delete",
        type: "GET",
        datatype: "json",
        data: {
            id: id
        },
        success: function (response) {
            console.log(response);
            listTeachers();
        }
    });
}

function register() {

    var nombre = $("#name").val();
    var apellidos = $("#lastname").val();
    var identificacion = $("#identification").val();
    var telefono = $("#phone").val();
    var correo = $("#email").val();
    var contraseña = $("#identification").val();


    $.ajax({
        url: "/teacher",

        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "POST",
        data: {

            name: nombre,
            lastname: apellidos,
            identification: identificacion,
            phone: telefono,
            email: correo,
            password: contraseña,

        },
        dataType: "json",
        success: function (response) {
            if (response.ok == true) {

                listTeachers();
                toastr("success", "Atención", response.message);
                $("#register").modal("toggle");
                cleanInputs();

            } else if (response.ok == false) {
                if (response.message) {
                    listAlerts(response.message);
                } else {
                    toastr("danger", "Atención", response.error);
                }
            }
        }
    });
}

function see_teacher(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/teacher/" + id,
        type: "GET",
        dataType: "json",
        success: function (res) {

            if (res.ok == true) {
                res = res.data;


                $("#names").val(res.name);
                $("#names").prop("disabled", true);

                $("#lastnames").val(res.lastname);
                $("#lastnames").prop("disabled", true);

                $("#identifications").val(res.identification);
                $("#identifications").prop("disabled", true);

                $("#phones").val(res.phone);
                $("#phones").prop("disabled", true);

                $("#emails").val(res.email);
                $("#emails").prop("disabled", true);

                $("#buttonEdit").hide();
                $("#see_teacher").modal("toggle");


            } else if (res.ok == false) {
                toastr("danger", "Error", res.message);
            }
        }
    });
}

function edit_teacher(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },

        url: "/teacher/" + id,

        type: "GET",
        dataType: "json",
        success: function (res) {
            if (res.ok == true) {
                res = res.data;


                $("#names").val(res.name);
                $("#names").prop("disabled", false);

                $("#lastnames").val(res.lastname);
                $("#lastnames").prop("disabled", false);

                $("#identifications").val(res.identification);
                $("#identifications").prop("disabled", false);

                $("#phones").val(res.phone);
                $("#phones").prop("disabled", false);

                $("#emails").val(res.email);
                $("#emails").prop("disabled", false);


                $("#see_teacher").modal("toggle");
                $("#buttonEdit").show();

                $("#buttonEdit").attr("onclick", `update_teacher(${id})`);

            } else if (res.ok == false) {
                toastr("danger", "Error", res.message);
            }
        }
    });
}

function update_teacher(id) {
    var nombre = $("#names").val();
    var apellidos = $("#lastnames").val();
    var identificacion = $("#identifications").val();
    var telefono = $("#phones").val();
    var correo = $("#emails").val();
    var contraseña = $("#identifications").val();

    if (nombre == "") {
        toastr("danger", "Error", "Debes de ingresar el nombre del instructor.");
        return false;
    }

    if (apellidos == "") {
        toastr(
            "danger",
            "Error",
            "Debes de ingresar los apellidos del instructor."
        );
        return false;
    }

    if (identificacion == "") {
        toastr(
            "danger",
            "Error",
            "Debes de ingresar el número de identificación del instructor."

        );
        return false;
    }


    if (telefono == "") {
        toastr(
            "danger",
            "Error",
            "Debes de ingresar el telefono del instructor."

        );
        return false;
    }


    if (correo == "") {
        toastr(
            "danger",
            "Error",
            "Debes de ingresar el correo electrónico del instructor."

        );
        return false;
    }


    if (contraseña == "") {
        toastr(
            "danger",
            "Error",
            "Es necesario que ingrese el número de identificación."

        );
        return false;
    }






    bander_upd = true;

    if (bander_upd == true) {
        $.ajax({

            url: "/teacher/update",

            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            type: "POST",
            dataType: "json",
            data: {
                id: id,

                name: nombre,
                lastname: apellidos,
                identification: identificacion,
                password: contraseña,
                email: correo,
                phone: telefono,

            },
            success: function (response) {
                if (response.ok == true) {
                    toastr("success", "Hecho!", response.message);
                    listTeachers();
                    $("#see_teacher").modal("toggle");
                }
                if (response.ok == false) {
                    listAlertsUpdate(response.message);
                }
            }
        });
        return false;
    }
}

function listAlerts(e) {


    //register

    if (e.name) {
        toastr("danger", "Error", e.name[0]);
        $("#name").addClass("is-invalid");
    }

    if (e.lastname) {
        toastr("danger", "Error", e.lastname[0]);
        $("#lastname").addClass("is-invalid");
    }

    if (e.email) {
        toastr("danger", "Error", e.email[0]);
        $("#email").addClass("is-invalid");
    }


    if (e.phone) {
        toastr("danger", "Error", e.phone[0]);
        $("#phone").addClass("is-invalid");
    }
    if (e.identification) {
        toastr("danger", "Error", e.identification[0]);
        $("#identification").addClass("is-invalid");

    }



}

function listAlertsUpdate(e) {




    if (e.name) {
        toastr("danger", "Error", e.name[0]);
        $("#names").addClass("is-invalid");
    }

    if (e.lastname) {
        toastr("danger", "Error", e.lastname[0]);
        $("#lastnames").addClass("is-invalid");
    }

    if (e.email) {
        toastr("danger", "Error", e.email[0]);
        $("#emails").addClass("is-invalid");
    }


    if (e.phone) {
        toastr("danger", "Error", e.phone[0]);
        $("#phones").addClass("is-invalid");
    }
    if (e.identification) {
        toastr("danger", "Error", e.identification[0]);
        $("#identifications").addClass("is-invalid");

    }



}

function toastr(type, tittle, text) {
    $.toast({
        heading: "" + tittle + "",
        text: "" + text + "",
        position: "top-right",
        loaderBg: "#000",
        class: "jq-has-icon jq-toast-" + type,
        hideAfter: 3500,
        stack: 6,
        showHideTransition: "fade"
    });
}

