var table = false;

$(document).ready(function () {
    listComplex();
});

function applyDataTable() {
    $("#complexTable").DataTable({
        responsive: true,
        autoWidth: false,
        language: {
            search: "",
            searchPlaceholder: "Buscar",
            sLengthMenu: "_MENU_items"
        }
    });

    table = true;
}

function cleanInputs() {

    $("#name").val("");
    $("#name").removeClass("is-invalid");

    $("#regional_id").val("");
    $("#regional_id").removeClass("is-invalid");

    $("#names").val("");
    $("#names").removeClass("is-invalid");

    $("#regional_ids").val("");
    $("#regional_ids").removeClass("is-invalid");

}

function listComplex() {
    $("#tableComplexBody").html("");
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/complex/data",
        type: "GET",
        datatype: "json",
        success: function (response) {
            console.log(response);
            if (response.ok == false) {
                toastr("info", "Atención", response.message);
            } else {
                response.data.forEach(function (e, i) {
                    if (e.status == 0) {
                        var state =
                            '<button type="button" onclick="change_state(' +
                            e.id +
                            ')" class="btn btn-success btn-rounded btn-xs" style="width: 67px;">Activo</button>';
                    } else {
                        var state =
                            '<button type="button" onclick="change_state(' +
                            e.id +
                            ')" class="btn btn-danger btn-rounded btn-xs" style="width: 67px;">Inactivo</button>';
                    }
                    $("#tableComplexBody").append(
                        "<tr>" +
                        "<th>" +
                        (i + 1) +
                        "</th>" +
                        "<td>" +
                        e.name +
                        "</td>" +
                        "<td>" +
                        e.region.name +
                        "</td>" +
                        "<td>" +
                        '<button onclick="edit_complex(' +
                        e.id +
                        ')" data-toggle="modal" data-target="#edit_complex"  class="btn btn-outline-success btn-icon btn-icon-circle btn-success btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-pencil"></i></span></button>' +
                        '<button onclick="see_complex(' +
                        e.id +
                        ')" data-toggle="modal" data-target="#see_complex"  class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-eye"></i></span></button>' +

                        "</tr>"
                    );
                });

                if (table == false) {
                    applyDataTable();
                }
            }
        }
    });
}

function change_state(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/complex/delete",
        type: "GET",
        datatype: "json",
        data: {
            id: id
        },
        success: function (response) {
            console.log(response);
            listComplex();
        }
    });
}

function register() {
    var nombreComplejo = $("#name").val();
    var regional = $("#regional_id").val();


    $.ajax({
        url: "/complex",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "POST",
        data: {
            name: nombreComplejo,
            region_id: regional,

        },
        dataType: "json",

        success: function (response) {
            if (response.ok == true) {
                listComplex();
                toastr("success", "Atención", response.message);
                $("#register").modal("toggle");
                cleanInputs();
            } else if (response.ok == false) {
                if (response.message) {
                    listAlerts(response.message);
                } else if (response.exist) {
                    toastr("danger", "Atención", response.exist);
                } else {
                    toastr("danger", "Atención", response.error);
                }
            }
        }
    });
}

function see_complex(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/complex/" + id,
        type: "GET",
        dataType: "json",
        success: function (res) {
            if (res.ok == true) {
                res = res.data;
                $("#names").val(res.name);
                $("#names").prop("disabled", true);

                $("#regional_ids").val(res.region_id);
                $("#regional_ids").prop("disabled", true);

                $("#buttonEdit").hide();
                $("#see_complex").modal("toggle");
                cleanInputs();
            } else if (res.ok == false) {
                toastr("danger", "Error", res.message);
            }
        }
    });
}

function edit_complex(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/complex/" + id,
        type: "GET",
        dataType: "json",
        success: function (res) {
            console.log(res)
            if (res.ok == true) {
                res = res.data;
                $("#names").val(res.name);
                $("#names").prop("disabled", false);

                $("#regional_ids").val(res.region_id);
                $("#regional_ids").prop("disabled", false);

                $("#see_complex").modal("toggle");
                $("#buttonEdit").show();

                $("#buttonEdit").attr("onclick", `update_complex(${id})`);
            } else if (res.ok == false) {
                toastr("danger", "Error", res.message);
            }
        }
    });
}

function update_complex(id) {
    var nombreComplejo = $("#names").val();
    var regional = $("#regional_ids").val();
    var bander_upd = false;

    if (nombreComplejo == "") {
        toastr("danger", "Error", "Debes de ingresar el nombre del complejo.");
        return false;
    }

    if (regional == "") {
        toastr(
            "danger",
            "Error",
            "Debes de seleccionar la regional."
        );
        return false;
    }

    bander_upd = true;

    if (bander_upd == true) {
        $.ajax({
            url: "/complex/update",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            type: "POST",
            dataType: "json",
            data: {
                id: id,
                name: nombreComplejo,
                region_id: regional,
            },
            success: function (response) {
                if (response.ok == true) {
                    toastr("success", "Hecho!", response.message);
                    listComplex();
                    $("#see_complex").modal("toggle");
                }
                if (response.ok == false) {
                    toastr("danger", "Error", response.message);
                }
            }
        });
        return false;
    }
}

function listAlerts(e) {

    if (e.name) {
        toastr("danger", "Error", e.name[0]);
        $("#name").addClass("is-invalid");
    }

    if (e.region_id) {
        toastr("danger", "Error", e.region_id[0]);
        $("#regional_id").addClass("is-invalid");
    }

    if (e.name) {
        toastr("danger", "Error", e.name[0]);
        $("#names").addClass("is-invalid");
    }

    if (e.region_id) {
        toastr("danger", "Error", e.region_id[0]);
        $("#regional_ids").addClass("is-invalid");
    }


}

function toastr(type, tittle, text) {
    $.toast({
        heading: "" + tittle + "",
        text: "" + text + "",
        position: "top-right",
        loaderBg: "#000",
        class: "jq-has-icon jq-toast-" + type,
        hideAfter: 3500,
        stack: 6,
        showHideTransition: "fade"
    });
}




