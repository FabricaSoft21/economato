var table = false;

$(document).ready(function () {
    listUsers();
});

function applyDataTable() {
    $("#UsersTable").DataTable({
        responsive: true,
        autoWidth: false,
        language: {
            search: "",
            searchPlaceholder: "Buscar",
            sLengthMenu: "_MENU_items"
        }
    });

    table = true;
}

function cleanInputs() {

    $("#name").val("");
    $("#name").removeClass("is-invalid");

    $("#lastname").val("");
    $("#lastname").removeClass("is-invalid");

    $("#identification").val("");
    $("#identification").removeClass("is-invalid");

    $("#phone").val("");
    $("#phone").removeClass("is-invalid");

    $("#email").val("");
    $("#email").removeClass("is-invalid");

    $("#password").val("");
    $("#password").removeClass("is-invalid");



}



function listUsers() {

    $("#tableUsersBody").html("");
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/user/data",
        type: "GET",
        datatype: "json",
        success: function (response) {

            if (response.ok == false) {
                toastr("info", "Atención", response.message);
            } else {
                response.data.forEach(function (e, i) {

                    if (!(e.role_name == 'Super-Admin')) {

                        if (e.status == 0) {
                            var state =
                                '<button type="button" onclick="change_state(' +
                                e.id +
                                ')" class="btn btn-success btn-rounded btn-xs" style="width: 67px;">Activo</button>';
                        } else {
                            var state =
                                '<button type="button" onclick="change_state(' +
                                e.id +
                                ')" class="btn btn-danger btn-rounded btn-xs" style="width: 67px;">Inactivo</button>';
                        }

                    } else {
                        var state =
                            '<button type="button" class="btn btn-success btn-rounded btn-xs" style="width: 67px;" disabled>Activo</button>';
                    }
                    $("#tableUsersBody").append(
                        "<tr>" +
                        "<th>" +
                        (i + 1) +
                        "</th>" +
                        "<td>" +
                        e.name + ' ' + e.lastname +
                        "</td>" +
                        "<td>" +
                        e.role_name +
                        "</td>" +
                        "<td>" +
                        e.email +
                        "</td>" +
                        "<td>" +
                        state +
                        "</td>" +

                        "</tr>"
                    );
                });

                if (table == false) {
                    applyDataTable();
                }
            }
        }
    });
}

function change_state(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/user/delete",
        type: "POST",
        datatype: "json",
        data: {
            id: id
        },
        success: function (response) {
            console.log(response);
            listUsers();
        }
    });
}

function register() {

    var nombre = $("#name").val();
    var apellidos = $("#lastname").val();
    var identificacion = $("#identification").val();
    var telefono = $("#phone").val();
    var correo = $("#email").val();
    var contraseña = $("#identification").val();
    var role = $("#role_id").val();


    $.ajax({
        url: "/user/save",

        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "POST",
        data: {

            name: nombre,
            lastname: apellidos,
            identification: identificacion,
            phone: telefono,
            email: correo,
            password: contraseña,
            rol_id: role,

        },
        dataType: "json",
        success: function (response) {
            if (response.ok == true) {

                listUsers();
                toastr("success", "Atención", response.message);
                cleanInputs();
                $("#register").modal("toggle");


            } else if (response.ok == false) {
                if (response.message) {
                    listAlerts(response.message);
                } else {
                    toastr("danger", "Atención", response.error);
                }
            }
        }
    });
}


function listAlerts(e) {


    //register

    if (e.name) {
        toastr("danger", "Error", e.name[0]);
        $("#name").addClass("is-invalid");
    }

    if (e.lastname) {
        toastr("danger", "Error", e.lastname[0]);
        $("#lastname").addClass("is-invalid");
    }

    if (e.email) {
        toastr("danger", "Error", e.email[0]);
        $("#email").addClass("is-invalid");
    }


    if (e.phone) {
        toastr("danger", "Error", e.phone[0]);
        $("#phone").addClass("is-invalid");
    }
    if (e.identification) {
        toastr("danger", "Error", e.identification[0]);
        $("#identification").addClass("is-invalid");

    }



}

function toastr(type, tittle, text) {
    $.toast({
        heading: "" + tittle + "",
        text: "" + text + "",
        position: "top-right",
        loaderBg: "#000",
        class: "jq-has-icon jq-toast-" + type,
        hideAfter: 3500,
        stack: 6,
        showHideTransition: "fade"
    });
}

