$(document).ready(function() {
    listNotifications()
})
setInterval(listNotifications, 10000);
function listNotifications() {
    $.get('/get/notifications', function(r) {
        if (r.ok) {
            $("#notifications").html('');
            r.data.forEach(e => {
                $("#notifications").prepend(`<a href="javascript:void(0);" class="dropdown-item">
                                                <div class="media">
                                                    <div class="media-body">
                                                        <div>
                                                            <div class="notifications-text"><span class="text-dark text-capitalize">${e.title}:</span> ${e.description}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                            <div class="dropdown-divider"></div>`)

            });
            $("#notify").show()

        }else{
            $("#notifications").html(`<a href="javascript:void(0);" class="dropdown-item">
            <div class="media">
                <div class="media-body">
                    <div>
                        <div class="notifications-text">No tienes notificaciones</div>
                    </div>
                </div>
            </div>
        </a>
        <div class="dropdown-divider"></div>`);
            $("#notify").hide()
        }
    });
}
function viewAllNotifications() {
    $.get('/view/all/notifications',function() {
        listNotifications()
    })
}
function deleteNotify() {
    $("#notify").hide()    
}