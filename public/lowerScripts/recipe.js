// import { type } from "os";

window.addEventListener("load", function () {
    //BUSCAR RECETA
    
    listProducts();

    $('#searchRecipe').submit(function (e) {
        e.preventDefault();

        // $(this).attr('action', url+'/recipe/search/'+$('#searchRecipe #search').val());

        var search = {
            search: $('input[name="search"]').val(),
        }

        $.get("/recipe/search/" + search.search, function (response) {
           
            $("#card_recipes").html("");

            response.data.forEach((element, index) => {

                    var date = new Date(element.created_at);

                $("#card_recipes").append(`
                <div class="col-6 my-2">
                    <div class="ft-recipe"> 
                    <div class="ft-recipe__thumb"><span id="close-modal"><i class="ion ion-md-eye"></i></span>
                    <h3></h3><img src="/uploads/recipes/${element.image}" alt="Strawberry Waffle"/>
                    </div>
                    <div class="ft-recipe__content">
                    <header class="content__header">
                        <div class="row-wrapper">
                        <h2 class="recipe-title">${element.recipe_name}</h2>
                        <div class="user-rating"></div>
                        </div>
                        <ul class="recipe-details">
                        <li class="recipe-details-item time"><i class="ion ion-ios-clock-outline"></i><span class="value">${date.getDay()}/${date.getMonth()}/${date.getUTCFullYear()}</span><span class="title">Registro</span></li>
                        <li class="recipe-details-item ingredients"><i class="ion ion-ios-book-outline"></i><span class="value">${element.recipes_xproducts.length}</span><span class="title">Ingredients</span></li>
                        </ul>
                    </header>
                    <p class="description">
                        ${element.description}
                    </p>
                    <footer class="content__footer"><a href="/recipe/edit-recipe/${element.id}">Ver receta</a></footer>
                    </div>
                </div></div>
                `
                );
            });

        });

        return false;

    });



    $('#searchProduct').submit(function () {
        $(this).attr('action', url + '/recipe/edit-recipe/' + $('#searchProduct #recipe_id').val() + '/' + $('#searchProduct #search').val());
    });


    $('#searchProduct').submit(function (e) {
        e.preventDefault();



        var search = {
            search: $('input[name="search"]').val(),
        }

        dataProducts(search.search);

        return false;
    });


    dataProducts();

});



function dataProducts(search) {

    if (search == undefined) {
        search = "";
    }

    $.get("/recipe/product/" + search, function (response) {


        $('#products').html("");

        response.data.forEach((element, index) => {
            var myobj = JSON.stringify(element);

            $('#products').append(`
                    <hr>
                    <p><button class="btn btn-primary" style="border-radius:50%;" onclick='infoProduct(${myobj})'><i class="glyphicon-plus"></i></button> ${element.name}</p> 
            `);

        });

    });

}

function infoProduct(element) {


    $('#ModalProductBody').html(` 
        Descripcion del producto
        <hr>
        <b>Nombre: ${element.name}</b>
        <p>Tipo: ${element.type_product.name}</p>
        <p>Presentacion: ${element.presentation_product.name}</p>
        <p>Unidad de medida: ${element.measure_unit.name}</p>
        <hr>
    `);


    $('#idProduct').val(element.id);
    $('#quantityMU').text(` ${element.measure_unit.name}`);
    $("#ModalProduct").modal('toggle');

}

function addProduct() {

    var datos = {
        idProduct: $('input[name="idProduct"]').val(),
        idRecipe: $('input[name="idRecipe"]').val(),
        product_quantity: $('input[name="product_quantity"]').val(),
        _token: $('meta[name="csrf-token"]').attr("content")
    }



    $.post("/recipe/addproduct/", datos, function (response) {

        $("#ModalProduct").modal('toggle');
        toastr("success", "Hecho!", response.message);

    });

    listProducts();

}
function listProducts() {


    idRecipe = $('input[name="recipe"]').val();

    if(idRecipe!=null){


        $.get("/products-of-recipe/" + idRecipe, function (response) {

            console.log(response);

            $('#listProducts').html("");

            response.data.forEach((element, index) => {

                var myobj = JSON.stringify(element);

                $('#listProducts').append(`
                    <hr>
                    <p><button class="btn btn-warning" style="border-radius:50%;" onclick='removeProduct(${myobj})'><i class="glyphicon-minus"></i></button> ${element.product.name} <span>${element.quantity}</span></p>
                    `);

            });

        });

    }

}

function removeProduct(element) {


    $('#infoProductRemove').html(` 
        <b>Nombre: ${element.product.name}</b>
        <p>Unidad de medida: <span style="color:lightseagreen"> ${element.product.measure_unit.name}</span><p>
        <p>Cantidad seleccionada para la receta: ${element.quantity}</p>
        <hr>
    `);


    $('#product_quantity_remove').val(element.quantity);
    $('#idRecipeXProduct').val(element.id);

    $("#modalRemoveProduct").modal('toggle');


}

function subtractProduct() {

    var datos = {
        product_quantity_remove: $('input[name="product_quantity_remove"]').val(),
        idRecipeXProduct: $('input[name="idRecipeXProduct"]').val(),
        _token: $('meta[name="csrf-token"]').attr("content")
    }

    $.post("/recipe/removeproduct/", datos, function (response) {

        toastr("success", "Hecho!", response.message);

    });

    $("#modalRemoveProduct").modal('toggle');


    listProducts();
}


function toastr(type, tittle, text) {
    $.toast({
        heading: "" + tittle + "",
        text: "" + text + "",
        position: "top-right",
        loaderBg: "#000",
        class: "jq-has-icon jq-toast-" + type,
        hideAfter: 3500,
        stack: 6,
        showHideTransition: "fade"
    });
}

