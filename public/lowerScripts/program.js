var table = false;

$(document).ready(function() {
	listPrograms();
});

function applyDataTable() {
    $('#tablePrograms').DataTable({
		responsive: true,
		autoWidth: false,
		language: { search: "",
		searchPlaceholder: "Buscar",
		sLengthMenu: "_MENU_items"
		
		}
    });	

	table = true;
}
function validateData(){
	let name = $("#name").val();
	let version = $("#version").val();
	let description = $("#description").val();

	$.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/program',
        type: 'POST',
        data:{
			'name':name,
			'version':version,
			'description':description,
        },
        datatype: 'json',
        success: function (response) {
            if(response.ok==true){
				listPrograms();
				toastr('success','Atención',response.message);
				$('#register').modal('toggle');
				cleanInputs();
            }else if(response.ok==false){
                if (response.message) {
                    listAlerts(response.message);
                }else{
					toastr('danger','Atención',response.error);
				}
            }
        }
    });
}

function listPrograms() {

    $("#tableProgramsBody").html('');
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/program/data',
        type: 'GET',
        datatype: 'json',
        success: function (response) {
            if (response.ok == false) {
                toastr('info','Atención',response.message);
            } else {
                
                response.data.forEach(function (e, i) {
					if(e.status==0){
						var state = `<button type="button" class="btn btn-success btn-rounded btn-xs" onclick="setState(${e.id})">Activo</button>`;
					}else{
						var state = `<button type="button" class="btn btn-danger btn-rounded btn-xs" onclick="setState(${e.id})">Inactivo</button>`;
                    }
                    
                    $("#tableProgramsBody").append(`<tr>
                                                        <th>${(i+1)}</th>
                                                        <td>${e.name}</td>
                                                        <td>${e.version}</td>
                                                        <td>${e.description.substr(0,50)}${e.description.length>50?`...`:`` }</td>
                                                        <td>
                                                            ${state}
                                                        </td>
                                                        <td>
                                                            <button onclick="edit(${e.id})" class="btn btn-outline-success btn-icon btn-icon-circle btn-success btn-icon-style-2"><span class="btn-icon-wrap" ><i class="fa fa-pencil"></i></span></button>
                                                            <button onclick="show(${e.id})"class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap" ><i class="fa fa-eye"></i></span></button>
                                                        </td>
                                                    </tr>`);
				});
				
                if (table == false) {
                    applyDataTable();
                }

            }

        }



    });

}
function show(id){
	$.ajax({
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url:'/program/'+id,
		type:'GET',
		datatype:'json',
		success: function (r) {
			if(r.ok==true){
				$("#nameShow").val(r.data.name);
				$("#nameShow").prop('disabled',true);
				$("#versionShow").val(r.data.version);
				$("#versionShow").prop('disabled',true);
				$("#descriptionShow").val(r.data.description);
				$("#descriptionShow").prop('disabled',true);

				$("#buttonEdit").hide();
				$("#show").modal('toggle');


			}else if(r.ok==false){
				toastr('danger','Error',r.message)
			}
		}
	});
}
function edit(id){
	$.ajax({
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url:'/program/'+id,
		type:'GET',
		datatype:'json',
		success: function (r) {
			if(r.ok==true){
                $("#nameShow").val(r.data.name);
				$("#nameShow").prop('disabled',false);
				$("#versionShow").val(r.data.version);
				$("#versionShow").prop('disabled',false);
				$("#descriptionShow").val(r.data.description);
                $("#descriptionShow").prop('disabled',false);
                
                $("#buttonEdit").show();
                $("#buttonEdit").attr(`onclick`,`update(${id})`);
				$("#show").modal('toggle');

			}else if(r.ok==false){
				toastr('danger','Error',r.message)
			}
		}
	});
}
function update(id){
    let name = $("#nameShow").val();
	let version = $("#versionShow").val();
	let description = $("#descriptionShow").val();

	$.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/program/'+id,
        type: 'PUT',
        data:{
			'name':name,
			'version':version,
			'description':description,
        },
        datatype: 'json',
        success: function (response) {
            if(response.ok==true){
				listPrograms();
				toastr('success','Atención',response.message);
				$('#show').modal('toggle');
				cleanInputsUpdate();
            }else if(response.ok==false){
                if (response.message) {
                    listAlertsUpdate(response.message);
                }else{
					toastr('danger','Atención',response.error);
				}
            }
        }
    });
}
function cleanInputs(){
	$("#name").val('');
	$("#name").removeClass('is-invalid');
	$("#version").val('');
	$("#version").removeClass('is-invalid');
	$("#description").val('');
	$("#description").removeClass('is-invalid');
}
function cleanInputsUpdate(){
	$("#nameShow").val('');
	$("#nameShow").removeClass('is-invalid');
	$("#versionShow").val('');
	$("#versionShow").removeClass('is-invalid');
	$("#descriptionShow").val('');
	$("#descriptionShow").removeClass('is-invalid');
}
function listAlerts(e){
	if (e.name) {
		toastr('danger', 'Error',e.name[0]);
		$("#name").addClass('is-invalid');
	}
	if (e.version) {
		toastr('danger', 'Error',e.version[0]);
		$("#version").addClass('is-invalid');
	}
	if (e.description) {
		toastr('danger', 'Error',e.description[0]);
		$("#description").addClass('is-invalid');
	}
}
function listAlertsUpdate(e){
	if (e.name) {
		toastr('danger', 'Error',e.name[0]);
		$("#nameShow").addClass('is-invalid');
	}
	if (e.version) {
		toastr('danger', 'Error',e.version[0]);
		$("#versionShow").addClass('is-invalid');
	}
	if (e.description) {
		toastr('danger', 'Error',e.description[0]);
		$("#descriptionShow").addClass('is-invalid');
	}
}
function toastr(type,tittle,text){
	$.toast({
		heading: ''+tittle+'',
		text: ''+text+'',
		position: 'top-right',
		loaderBg:'#000',
		class: 'jq-has-icon jq-toast-'+type,
		hideAfter: 3500, 
		stack: 6,
		showHideTransition: 'fade'
	});
}
function setState(id){
	$.ajax({
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url: '/program/'+id,
		type:'DELETE',
		datatype:'json',
		success:function(r){
			if (r.ok==true) {
				listPrograms();
				toastr('success', 'Atención',r.message);
			} else {
				if(r.message){
					toastr('danger', 'Error',r.message);
				}
			}
		}
	});
}