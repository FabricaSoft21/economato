var table = false;

$(document).ready(function () {
    listBudgets();
    chart();
});

function applyDataTable() {
    $("#budgetTable").DataTable({
        responsive: true,
        autoWidth: false,
        language: {
            search: "",
            searchPlaceholder: "Buscar",
            sLengthMenu: "_MENU_items"
        }
    });

    table = true;
}

function cleanInputs() {
    $("#initial_budget").val("");
    $("#initial_budget").removeClass("is-invalid");

    $("#budget_code").val("");
    $("#budget_code").removeClass("is-invalid");

    $("#budget_begin").val("");
    $("#budget_begin").removeClass("is-invalid");

    $("#budget_finish").val("");
    $("#budget_finish").removeClass("is-invalid");
}

function listBudgets() {
    $("#tableBudgetBody").html("");
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/budget/data",
        type: "GET",
        datatype: "json",
        success: function (response) {
            console.log(response);
            if (response.ok == false) {
                toastr("info", "Atención", response.message);
            } else {
                response.data.forEach(function (e, i) {
                    if (e.status == 0) {
                        var state =
                            '<button type="button" class="btn btn-success btn-rounded btn-xs" style="width: 67px;">Vigente</button>';
                    } else {
                        var state =
                            '<button type="button" class="btn btn-danger btn-rounded btn-xs" style="width: 67px;">Vencido</button>';
                    }
                    $("#tableBudgetBody").append(
                        "<tr>" +
                        "<th>" +
                        (i + 1) +
                        "</th>" +
                        "<td>" +
                        e.initial_budget +
                        "</td>" +
                        "<td>" +
                        e.budget_code +
                        "</td>" +
                        "<td>" +
                        e.budget_begin +
                        "</td>" +
                        "<td>" +
                        e.budget_finish +
                        "</td>" +
                        "<td>" +
                        state +
                        "</td>" +
                        "<td>" +
                        '<button onclick="see_budget(' +
                        e.id +
                        ')" data-toggle="modal" data-target="#see_budget" class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-eye"></i></span></button>'
                        +
                        '<a href="budget_detail/' + e.id + '" class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><i class="fa fa-bar-chart"></a>' +
                        "</tr>"
                    );
                });

                if (table == false) {
                    applyDataTable();
                }
            }
        }
    });
}

function change_state(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/budget/delete",
        type: "GET",
        datatype: "json",
        data: {
            id: id
        },
        success: function (response) {
            console.log(response);
            listBudgets();
        }
    });
}
var ArrayBudget = [];
function register() {
    var presupuestoInicial = $("#total_hiddden").val();
    var codigoPresupuesto = $("#budget_code").val();
    var inicioPresupuesto = $("#budget_begin").val();
    var finPresupuesto = $("#budget_finish").val();

    $.ajax({
        url: "/budget",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "POST",
        data: {
            initial_budget: presupuestoInicial,
            budget_code: codigoPresupuesto,
            budget_begin: inicioPresupuesto,
            budget_finish: finPresupuesto,
            budgetXcharact: ArrayBudget,
            remaining : presupuestoInicial
        },
        dataType: "json",
        success: function (response) {
            if (response.ok == true) {
                listBudgets();
                toastr("success", "Atención", response.message);
                $("#register").modal("toggle");
                cleanInputs();
                location.reload();
                removeClass();
            } else if (response.ok == false) {
                if (response.message) {
                    listAlerts(response.message);
                } else {
                    toastr("danger", "Atención", response.error);
                }
            }
        }
    });
}

function see_budget(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/budget/" + id,
        type: "GET",
        dataType: "json",
        success: function (res) {

            if (res.ok == true) {
                res = res.data;
                var char = res.budgets_xcharacterizations;
                console.log(char)
                $("#initial_budgets").val(res.initial_budget);
                $("#budget_codes").val(res.budget_code);
                $("#budget_begins").val(res.budget_begin);
                $("#budget_finishs").val(res.budget_finish);
                $("#buttonEdit").hide();
                $("#see_budget").modal("toggle");

                char.forEach(function (e, i) {
                    console.log(e);
                    $(`#see${e.characterization_id}`).val(e.porcentage);
                    $('#see_val' + e.characterization_id).val(e.value)
                });
            } else if (res.ok == false) {
                toastr("danger", "Error", res.message);
            }
        }
    });
}



function assing(id) {
    $.ajax({
        url: "/charact",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "GET",
        dataType: "json",
        success: function (data) {
            ArrayBudget = [];
            var total = 0;
            var por = 0;
            var porcentage = 0;
            data.forEach(function (e, i) {
                var valor_capturado = $(`#value${e.id}`).val();
                if (valor_capturado.length == 0) {
                    valor_capturado = 0;
                }
                total = total + parseInt(valor_capturado);
                $("#total").html('$' + new Intl.NumberFormat().format(total));
                $("#total_hiddden").val(total);
            });
            data.forEach(function (e, i) {
                por = total / 100
                porcentage_d = $(`#value${e.id}`).val() / por;
                var porcentage = porcentage_d.toFixed(2);
                $("#porcentage" + e.id).val(porcentage + ' %')

                var ObjectoBudget = new Object();
                ObjectoBudget.porcentage = porcentage;
                ObjectoBudget.value = $(`#value${e.id}`).val();
                ObjectoBudget.id = e.id
                ArrayBudget.push(ObjectoBudget);
            });
            console.log(ArrayBudget)
        }
    });
}


function edit_budget(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/budget/" + id,
        type: "GET",
        dataType: "json",
        success: function (res) {
            if (res.ok == true) {
                res = res.data;
                $("#initial_budgets").val(res.initial_budget);
                $("#initial_budgets").prop("disabled", false);

                $("#budget_codes").val(res.budget_code);
                $("#budget_codes").prop("disabled", false);

                $("#budget_begins").val(res.budget_begin);
                $("#budget_begins").prop("disabled", false);

                $("#budget_finishs").val(res.budget_finish);
                $("#budget_finishs").prop("disabled", false);

                $("#see_budget").modal("toggle");
                $("#buttonEdit").show();

                $("#buttonEdit").attr("onclick", `update_budget(${id})`);
            } else if (res.ok == false) {
                toastr("danger", "Error", res.message);
            }
        }
    });
}

function update_budget(id) {
    var presupuestoInicial = $("#initial_budgets").val();
    var codigoPresupuesto = $("#budget_codes").val();
    var inicioPresupuesto = $("#budget_begins").val();
    var finPresupuesto = $("#budget_finishs").val();
    var bander_upd = false;

    if (presupuestoInicial == "") {
        toastr("danger", "Error", "Debes de ingresar el presupuesto inicial.");
        return false;
    }

    if (codigoPresupuesto == "") {
        toastr(
            "danger",
            "Error",
            "Debes de ingresar el codigo del Presupuesto."
        );
        return false;
    }

    if (inicioPresupuesto == "") {
        toastr(
            "danger",
            "Error",
            "Debes de ingresar el inicio del Presupuesto."
        );
        return false;
    }

    if (finPresupuesto == "") {
        toastr(
            "danger",
            "Error",
            "Debes de ingresar el fin del Presupuesto inicial."
        );
        return false;
    }

    if (finPresupuesto < inicioPresupuesto) {
        toastr(
            "info",
            "Atención",
            "La fecha de presupuesto final no puede ser menor a la fecha inicial"
        );
        return false;
    }

    bander_upd = true;

    if (bander_upd == true) {
        $.ajax({
            url: "/budget/update",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            type: "POST",
            dataType: "json",
            data: {
                id: id,
                initial_budget: presupuestoInicial,
                budget_code: codigoPresupuesto,
                budget_begin: inicioPresupuesto,
                budget_finish: finPresupuesto
            },
            success: function (response) {
                if (response.ok == true) {
                    toastr("success", "Hecho!", response.message);
                    $("#see_budget").modal("toggle");
                }
                if (response.ok == false) {
                    toastr("danger", "Error", response.message);
                }
            }
        });
        return false;
    }
}

function listAlerts(e) {
    if (e.initial_budget) {
        toastr("danger", "Error", e.initial_budget[0]);
        $("#initial_budget").addClass("is-invalid");
    }

    if (e.budget_code) {
        toastr("danger", "Error", e.budget_code[0]);
        $("#budget_code").addClass("is-invalid");
    }

    if (e.budget_begin) {
        toastr("danger", "Error", e.budget_begin[0]);
        $("#budget_begin").addClass("is-invalid");
    }
    if (e.budget_finish) {
        toastr("danger", "Error", e.budget_finish[0]);
        $("#budget_finish").addClass("is-invalid");
    }
}

function toastr(type, tittle, text) {
    $.toast({
        heading: "" + tittle + "",
        text: "" + text + "",
        position: "top-right",
        loaderBg: "#000",
        class: "jq-has-icon jq-toast-" + type,
        hideAfter: 3500,
        stack: 6,
        showHideTransition: "fade"
    });
}

function drop_class() {
    $("#initial_budget").removeClass("is-invalid");
}


