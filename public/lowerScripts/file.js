var table = false;
var ArrayTeachers = [];

$(document).ready(function () {
    listFiles();

    $("#teacher_id").select2({
        dropdownParent: $("#register")
    });
});


function applyDataTable() {
    $("#fileTable").DataTable({
        responsive: true,
        autoWidth: false,
        language: {
            search: "",
            searchPlaceholder: "Buscar",
            sLengthMenu: "_MENU_items"
        }
    });

    table = true;
}

function listFiles() {
    $("#tableFilesBody").html("");
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/file/data",
        type: "GET",
        datatype: "json",

        success: function (response) {

            if (response.ok == false) {
                toastr("info", "Atención", response.message);

            } else {
                response.data.forEach(function (e, i) {
                    if (e.status == 0) {
                        var state =
                            '<button type="button" class="btn btn-success btn-rounded btn-xs">En curso</button>';
                    } else {
                        var state =
                            '<button type="button" class="btn btn-danger btn-rounded btn-xs">Finalizada</button>';
                    }
                    $("#tableFilesBody").append(
                        "<tr>" +
                        "<th>" + (i + 1) + "</th>" +
                        "<td>" + e.program + "</td>" +
                        "<td>" + e.charact + "</td>" +
                        "<td>" + e.number + "</td>" +
                        "<td>" + e.people + "</td>" +
                        "<td>" + state + "</td>" +
                        "<td>" +

                        '<button onclick="infoFile(' +
                        e.id +
                        ')" data-toggle="modal" data-target="#edit"  class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-eye"></i></span></button>' +
                        '<button onclick="editFile(' +
                        e.id +
                        ')" data-toggle="modal" data-target="#update"  class="btn btn-outline-success btn-icon btn-icon-circle btn-success btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-pencil"></i></span></button>' +

                        "</tr>"
                    );

                });

                if (table == false) {
                    applyDataTable();
                }
            }
        }
    });
}

function get_program() {
    var id = $("#center_id").val();
    if (id == "") {
        $("#program_id").html('')
    }
    $.ajax({
        url: `/file/get_pro/${id}`,
        type: "get",
        datatype: "json",
        success: function (data) {
            $("#program_id").html('');
            data.forEach(function (r, id, array) {
                $("#program_id").append(
                    `<option value="${r.id}">${r.name} ${r.version}</option>`
                );
            });
        }
    });
}


function getProgramUpdate() {
    var id = $("#center_idss").val();
    if (id == "") {
        $("#program_idss").html('')
    }
    $.ajax({
        url: `/file/get_pro/${id}`,
        type: "get",
        datatype: "json",
        success: function (data) {
            $("#program_idss").html('');
            data.forEach(function (r, id, array) {
                $("#program_idss").append(
                    `<option value="${r.id}">${r.name} ${r.version}</option>`
                );
            });
        }
    });
}

function register() {
    if (ArrayTeachers.length == 0) {
        toastr("danger", "Atención", "Debes seleccionar instructores");
    }
    if (ArrayTeachers.length > 0) {
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            url: "/file",
            type: "post",
            data: {
                number: $("#number").val(),
                apprentices: $("#apprentices").val(),
                start_date: $("#start_date").val(),
                finish_date: $("#finish_date").val(),
                status: 0,
                program_id: $("#program_id").val(),
                characterization_id: $("#charact_id").val(),
                center_id: $("#center_id").val(),
                teachers: ArrayTeachers,
            },
            datatype: "json",
            success: function (response) {
                if (response.ok == true) {
                    listFiles();
                    toastr("success", "Atención", response.message);
                    $("#register").modal("toggle");
                    cleanInputs();
                    removeClass();
                } else if (response.ok == false) {
                    if (response.message) {
                        listAlerts(response.message);
                    } else {
                        toastr("danger", "Atención", response.error);
                    }
                }
            }
        });
    }
}

function cleanInputs() {
    $("#number").val("");
    $("#apprentices").val("");
    $("#start_date").val("");
    $("#finish_data").val("");
    $("#program_id").val("");
    $("#charact_id").val("");
    $("#center_id").val("");

    $("#numbers").val("");
    $("#apprenticess").val("");
    $("#start_dates").val("");
    $("#finish_datas").val("");
    $("#charact_ids").html('');
    $("#center_ids").html('');
    $("#program_ids").html('');
}

function toastr(type, tittle, text) {
    $.toast({
        heading: "" + tittle + "",
        text: "" + text + "",
        position: "top-right",
        loaderBg: "#000",
        class: "jq-has-icon jq-toast-" + type,
        hideAfter: 3500,
        stack: 6,
        showHideTransition: "fade"
    });
}

function listAlerts(e) {
    if (e.number) {
        toastr("danger", "Error", e.number[0]);
        $("#number").addClass("is-invalid");
    }
    if (e.apprentices) {
        toastr("danger", "Error", e.apprentices[0]);
        $("#apprentices").addClass("is-invalid");
    }
    if (e.start_date) {
        toastr("danger", "Error", e.start_date[0]);
        $("#start_date").addClass("is-invalid");
    }
    if (e.finish_date) {
        toastr("danger", "Error", e.finish_date[0]);
        $("#finish_date").addClass("is-invalid");
    }
    if (e.program_id) {
        toastr("danger", "Error", e.program_id[0]);
        $("#program_id").addClass("is-invalid");
    }
    if (e.characterization_id) {
        toastr("danger", "Error", e.characterization_id[0]);
        $("#charact_id").addClass("is-invalid");
    }
    center_id;
    if (e.center_id) {
        toastr("danger", "Error", e.center_id[0]);
        $("#center_id").addClass("is-invalid");
    }
}

function removeClass() {
    $("#number").removeClass("is-invalid");
    $("#apprentices").removeClass("is-invalid");
    $("#start_date").removeClass("is-invalid");
    $("#finish_date").removeClass("is-invalid");
    $("#program_id").removeClass("is-invalid");
    $("#charact_id").removeClass("is-invalid");
    $("#center_id").removeClass("is-invalid");

    $("#numbers").removeClass("is-invalid");
    $("#apprenticess").removeClass("is-invalid");
    $("#start_dates").removeClass("is-invalid");
    $("#finish_dates").removeClass("is-invalid");
    $("#program_ids").removeClass("is-invalid");
    $("#charact_ids").removeClass("is-invalid");
    $("#center_ids").removeClass("is-invalid");
}

function list() {
    $("#tbl_teachers").html('');
    ArrayTeachers.forEach(function (e, i) {
        $("#tbl_teachers").append(`<tr>
                                   <th>${i + 1}</th>
                                   <td>${e.name}</td>
                                   <td>
                                    
                                   </td>
                                  </tr>`);
    });
}

function add_table(value) {
    var val = JSON.parse(value);

    var bander = true;
    ArrayTeachers.forEach(function (e, i) {
        if (e.id == val.id) {
            toastr("danger", "este instructor ya fue seleccionado");
            bander = false;
        }
    });
    if (bander == true) {
        ObjectTeacher = new Object();
        ObjectTeacher.name = val.name;
        ObjectTeacher.id = val.id


        ArrayTeachers.push(ObjectTeacher);
        list();
    }
}

function infoFile(id) {

    $.get("/file/" + id, function (response) {

        console.log(response.data);
        $('#numbers').val(response.data.number);
        $("#numbers").prop("readonly", true);

        $('#apprenticess').val(response.data.apprentices);
        $("#apprenticess").prop("readonly", true);

        $('#start_dates').val(response.data.start_date);
        $("#start_dates").prop("readonly", true);

        $('#finish_dates').val(response.data.finish_date);
        $("#finish_dates").prop("readonly", true);

        $("#charact_ids").html('')
        $("#charact_ids").append(`<option value="${response.data.characterization.id}" >${response.data.characterization.name} </option>`);
        $("#charact_ids").prop("readonly", true);

        $("#program_ids").html('');
        $("#program_ids").append(`<option value="${response.data.program.id}" >${response.data.program.name} </option>`);
        $("#program_ids").prop("readonly", true);

        $("#center_ids").html('');
        $("#center_ids").append(`<option value="${response.data.center.id}" >${response.data.center.name} </option>`);
        $("#center_ids").prop("readonly", true);


       
        $("#showInfoFile").modal('toggle');
    });


}


function editFile(id) {

    $.get("/file/" + id, function (response) {

        if (response.ok == true) {


            $('#numberss').val(response.data.number);
            $("#numberss").prop("readonly", false);

            $('#apprenticesss').val(response.data.apprentices);
            $("#apprenticess").prop("readonly", false);

            $('#start_datess').val(response.data.start_date);
            $("#start_datess").prop("readonly", false);

            $('#finish_datess').val(response.data.finish_date);
            $("#finish_datess").prop("readonly", false);          
           

            $("#buttonEdit").attr("onclick", `updateFile(${id})`);

        } else if (response.ok == false) {
            toastr("danger", "Error", response.message);
        }


    });
}






function updateFile(id) {

}
