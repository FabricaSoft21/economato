
var table = false;
function applyDataTable() {
    $("#tableOrders").DataTable({
        responsive: true,
        autoWidth: false,
        language: {
            search: "",
            searchPlaceholder: "Buscar",
            sLengthMenu: "_MENU_items"
        }
    });

    table = true;
}
function show(id) {
    $.ajax({
        url:'/produccioncenter/'+id,
        type:'get',
        datatype:'json',
        success:function(r){
            if(r.ok){
                let h = r.data.head
                let p = r.data.products
                console.log(h)
                $("#dateShow").val(h.order_date)
                $("#titleShow").val(h.title)
                $("#quantity_peopleShow").val(h.quantity_people)
                $("#locationShow").val(h.center.name)
                $("#descripcionShow").val(h.descripcion)

                $("#bodyProductsShow").html('')
                let costShow = 0;
                p.forEach(function(e,i) {
                    $("#bodyProductsShow").append(` <tr>
                                                        <td>${(i+1)}</td>
                                                        <td>${e.name}</td>
                                                        <td>${e.unitPrice}</td>
                                                        <td>${e.quantity}</td>
                                                        <td>$ ${new Intl.NumberFormat().format(e.subtotal)}</td>
                                                    </tr>`)
                    costShow=costShow+e.subtotal
                })
                $("#costShow").html(new Intl.NumberFormat().format(costShow))
                $("#show").modal('toggle')
            }else{
                toastr('danger','Error',r.message)
            }
        }
    })
}
function toastr(type, tittle, text) {
    $.toast({
        heading: "" + tittle + "",
        text: "" + text + "",
        position: "top-right",
        loaderBg: "#000",
        class: "jq-has-icon jq-toast-" + type,
        hideAfter: 3500,
        stack: 6,
        showHideTransition: "fade",
    });
}
