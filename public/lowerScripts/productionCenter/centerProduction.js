var products = [];
var cost = 0;
$(document).ready(function () {
    listOrders();

    $('#location_id').select2({
        dropdownParent: $('#register')
    });
});
function listOrders() {

    $("#tableOrdersBody").html('');
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/produccioncenter/data',
        type: 'GET',
        datatype: 'json',
        success: function (response) {
            if (response.ok == false) {
                toastr('info', 'Atención', response.message);
            } else {

                response.data.forEach(function (e, i) {
                    if (e.status_id == 1) {
                        var state = `<button type="button" class="btn btn-info btn-rounded btn-xs">${e.statusName}</button>`;
                    } else if (e.status_id == 2) {
                        var state = `<button type="button" class="btn btn-success btn-rounded btn-xs">${e.statusName}</button>`;
                    }
                    else if (e.status_id == 3) {
                        var state = `<button type="button" class="btn btn-danger btn-rounded btn-xs">${e.statusName}</button>`;
                    }
                    $("#tableOrdersBody").append(`  <tr>
                                                        <th>${(i + 1)}</th>
                                                        <td>${e.title}</td>
                                                        <td>${e.order_date}</td>
                                                        <td>${e.quantity_people}</td>
                                                        <td><strong>$</strong> ${e.cost} </td>
                                                        <td>${e.centerName}</td>
                                                        <td>
                                                            ${state}
                                                        </td>
                                                        <td>
                                                            <button onclick="show(${e.id})"class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap" ><i class="fa fa-eye"></i></span></button>
                                                        </td>
                                                    </tr>`);
                });

                if (table == false) {
                    applyDataTable();
                }

            }

        }



    });

}
function register() {
        let date = $("#date").val();
        let quantity_people = $("#quantity_people").val();
        let title = $("#title").val();
        let descripcion = $("#descripcion").val();
        let location_id = $("#location_id").val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/produccioncenter',
            type: 'POST',
            data: {
                'order_date': date,
                'quantity_people': quantity_people,
                'title': title,
                'descripcion': descripcion,
                'cost': cost,
                'center_id': location_id,
                'products':products
            },
            datatype: 'json',
            success: function (response) {
                if (response.ok == true) {
                    listOrders();
                    toastr('success', 'Atención', response.message);
                    $('#register').modal('toggle');
                    cleanInputs();
                } else if (response.ok == false) {
                    if (response.message) {
                        listAlerts(response.message);
                    } else {
                        toastr('danger', 'Atención', response.error);
                    }
                }
            }
        });
}
function search(value) {
    $("#results").html('');
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: '/search/product/' + value,
        type: 'get',
        datatype: 'json',
        success: function (r) {
            if (r.ok == false) {
                $("#results").append(`<li class="list-group-item list-group-item-danger">${r.message}</li>`)
            } else if (r.ok == true) {
                r.data.forEach(function (e, i) {
                    let product = JSON.stringify(e);
                    $("#results").append(`  <li class="list-group-item d-flex justify-content-between align-items-center">
                                                ${e.name}
                                                <span><button class="btn btn-success  btn-xs" onclick='addQuantity(${product})'><i class="glyphicon glyphicon-plus"></i></button></span>
                                            </li>`)
                });
            }
        }
    });
}
function addQuantity(product) {
    let ok = true
    products.forEach(function (e, i) {
        if (product.product_id == e.product_id) {
            toastr('danger', 'Error', 'Este producto ya ha sido agregado')
            ok = false
        }
    })
    if (ok) {
        $.ajax({
            url:'/measureunit/'+product.measure_unit_id,
            type:'get',
            datatype:'json',
            success:function(r){
                $("#meUnit").html(r.data.name)
            }
        })
        $('#results').html('')
        $('#quantityDiv').toggle()
        $('#btnAddProduct').toggle()
        $('#search').prop('readonly', true)
        $('#search').val(`${product.name}`)
        product = JSON.stringify(product);
        $('#btnAddProduct').attr('onclick', `addProduct(${product})`)
    }

}
function addProduct(product) {
    let quantity = $('#quantity').val()
    if (parseInt(quantity) <= 0) {
        toastr('danger', 'Atención', 'La cantidad del producto no pueden ser valores negativos ni cero');
        return false;
    } else if (quantity == '') {
        toastr('danger', 'Atención', 'La cantidad del producto no pueden ir vacía');
        return false;
    } else {
        product.quantity = parseInt(quantity)
        product.subtotal= parseInt(quantity*product.total_with_tax)
        cost = cost + parseInt(product.subtotal)
        products.push(product)

        $('#quantityDiv').toggle()
        $('#btnAddProduct').toggle()
        $('#search').prop('readonly', false)
        $('#search').val('')
        $("#quantity").val('')

        listProducts()
    }


}
function listProducts() {
    $("#bodyProducts").html('')
    cost = 0

    products.forEach(function (e, i) {
        $("#bodyProducts").append(` <tr>
                                        <th>${(i + 1)}</th>
                                        <td>${e.name}</td>
                                        <td>$ ${e.total_with_tax}</td>
                                        <td>${e.quantity}</td>
                                        <td>$ ${new Intl.NumberFormat().format(e.subtotal)}</td>
                                        <td>
                                            <button class="btn btn-icon btn-icon-only btn-info btn-icon-style-4" onclick="remove(${i})"><i class="icon-trash"></i></button>
                                        </td>
                                    </tr>`)
        cost=cost+e.subtotal
    })
    $("#cost").html(new Intl.NumberFormat().format(cost))

}
function cleanInputs() {
    $("#date").val('');
    $("#date").removeClass('is-invalid');
    $("#quantity_people").val('');
    $("#quantity_people").removeClass('is-invalid');
    $("#title").val('');
    $("#title").removeClass('is-invalid');
    $("#descripcion").val('');
    $("#descripcion").removeClass('is-invalid');
    $("#cost").html('0');
    $("#location_id").val('');
    $("#location_id").removeClass('is-invalid');

    products = [];
    $("#search").val('');
    $("#results").html('');
    $("#bodyProducts").html('')
}
function listAlerts(e) {
    if (e.order_date) {
        toastr('danger', 'Error', e.order_date[0]);
        $("#date").addClass('is-invalid');
    }
    if (e.quantity_people) {
        toastr('danger', 'Error', e.quantity_people[0]);
        $("#quantity_people").addClass('is-invalid');
    }
    if (e.title) {
        toastr('danger', 'Error', e.title[0]);
        $("#title").addClass('is-invalid');
    }
    if (e.descripcion) {
        toastr('danger', 'Error', e.descripcion[0]);
        $("#descripcion").addClass('is-invalid');
    }
    if (e.cost) {
        toastr('danger', 'Error', e.cost[0]);
        $("#cost").addClass('is-invalid');
    }
    if (e.location_id) {
        toastr('danger', 'Error', e.location_id[0]);
        $("#location_id").addClass('is-invalid');
    }
    if (e.phone_contact) {
        toastr('danger', 'Error', e.phone_contact[0]);
        $("#phoneContact").addClass('is-invalid');
    }

}
function remove(i) {
    products.splice(i, 1)
    listProducts()
}