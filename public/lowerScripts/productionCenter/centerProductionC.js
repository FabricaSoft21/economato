
$(document).ready(function () {
    listOrders();
});
function listOrders() {

    $("#tableOrdersBody").html('');
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/produccioncenter/data',
        type: 'GET',
        datatype: 'json',
        success: function (response) {
            if (response.ok == false) {
                toastr('info', 'Atención', response.message);
            } else {

                response.data.forEach(function (e, i) {
                    if (e.status_id == 1) {
                        var state = `<button type="button" class="btn btn-info btn-rounded btn-xs">${e.statusName}</button>`;
                    } else if (e.status_id == 2) {
                        var state = `<button type="button" class="btn btn-success btn-rounded btn-xs">${e.statusName}</button>`;
                    }
                    else if (e.status_id == 3) {
                        var state = `<button type="button" class="btn btn-danger btn-rounded btn-xs">${e.statusName}</button>`;
                    }
                    $("#tableOrdersBody").append(`  <tr>
                                                        <th>${(i + 1)}</th>
                                                        <td>${e.title}</td>
                                                        <td>${e.order_date}</td>
                                                        <td>${e.quantity_people}</td>
                                                        <td><strong>$</strong> ${e.cost} </td>
                                                        <td>${e.centerName}</td>
                                                        <td style="text-align: center;">
                                                            <div id="loading_${e.id}" style="display:none;">
                                                                <img src="/gifs/loading.gif"   style="height: 20px;width: 20px;">
                                                            </div>
                                                            <div id="buttons_${e.id}">
                                                                <button type="button" class="btn btn-success btn-rounded btn-xs" onclick="changeState(${e.id}, 2)">Aprobar</button>
                                                                <button type="button" class="btn btn-danger btn-rounded btn-xs" onclick="changeState(${e.id}, 3)">Anular</button>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <button onclick="show(${e.id})"class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap" ><i class="fa fa-eye"></i></span></button>
                                                        </td>
                                                    </tr>`);
                });

                if (table == false) {
                    applyDataTable();
                }

            }

        }



    });

}
function changeState(id, newState) {

    $(`#loading_${id}`).show()
    $(`#buttons_${id}`).hide()

     $.ajax({
         headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
       },
       url: '/produccioncenter/setState/' + id,
        type: 'post',
        data: {
           'newState': newState
       },
        dataType: 'json',
        success: function (r) {
            if (r.ok) {
                toastr('success', 'Atención', r.message)
                listOrders()
          } else {
                console.log(r.error);
            }
        }
     }) 

}