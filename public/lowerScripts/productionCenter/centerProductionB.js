$(document).ready(function () {
    listOrders();
});
function listOrders() {

    $("#tableOrdersBody").html('');
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/produccioncenter/data',
        type: 'GET',
        datatype: 'json',
        success: function (response) {
            if (response.ok == false) {
                toastr('info', 'Atención', response.message);
            } else {

                response.data.forEach(function (e, i) {
                    if (e.status_id == 1) {
                        var state = `<button type="button" class="btn btn-info btn-rounded btn-xs">${e.status_order.name}</button>`;
                    } else if (e.status_id == 2) {
                        var state = `<button type="button" class="btn btn-success btn-rounded btn-xs">${e.status_order.name}</button>`;
                    }
                    else if (e.status_id == 3) {
                        var state = `<button type="button" class="btn btn-danger btn-rounded btn-xs">${e.status_order.name}</button>`;
                    }
                    $("#tableOrdersBody").append(`  <tr>
                                                        <th>${(i + 1)}</th>
                                                        <td>${e.title}</td>
                                                        <td>${e.order_date}</td>
                                                        <td>${e.quantity_people}</td>
                                                        <td><strong>$</strong> ${e.cost} </td>
                                                        <td>${e.center.name}</td>
                                                        <td>
                                                            ${state}
                                                        </td>
                                                        <td>
                                                            <button onclick="show(${e.id})"class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap" ><i class="fa fa-eye"></i></span></button>
                                                        </td>
                                                    </tr>`);
                });

                if (table == false) {
                    applyDataTable();
                }

            }

        }



    });

}
function changeState(id,newState) {
    $.ajax({
        headers:{
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url:'/produccioncenter/setState/'+id,
        type:'post',
        data:{
            'newState':newState
        },
        dataType:'json',
        success:function (r) {
            if (r.ok) {
                toastr('success','Atención',r.message)
                listRequest()
            } else {
                console.log(r.error);
            }
        }
    })
}