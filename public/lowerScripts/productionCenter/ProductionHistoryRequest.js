
$(document).ready(function () {
    listRequest();

});


function applyDatatable() {

    $("#orderCenter").DataTable({
        responsive: true,
        autoWidth: false,
        language: {
            search: "",
            searchPlaceholder: "Buscar",
            sLengthMenu: "_MENU_items"
        }
    });
}

function listRequest() {
    $("#tableHistoryBody").html("");
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/approved/data",
        type: "GET",
        datatype: "json",

        success: function (response) {

            if (response.ok == false) {
                toastr("info", "Atención", response.message);

            } else {
                response.data.forEach(function (e, i) {
                    if (e.status_id == 2) {
                        var state =
                            '<span  class="text-success">Aprobado</span>';
                    } else if (e.status_id) {
                        var state =
                            '<span  class="text-danger">Denegado</span>';
                    }
                    $("#tableHistoryBody").append(
                        "<tr>" +
                        "<td>" + (i + 1) + "</td>" +
                        "<td>" + e.title + "</td>" +
                        "<td>" + e.descripcion + "</td>" +
                        "<td>" + e.quantity_people + "</td>" +
                        "<td>" + '$ ' + e.cost + "</td>" +
                        "<td>" + state + "</td>" +
                        "<td>" +

                        '<button onclick="showDataHistory(' +
                        e.id +
                        ')" data-toggle="modal" data-target="#showHistoryOrders" class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-eye"></i></span></button>' +
                        "</td>" +
                        "</tr>"
                    );

                });

                applyDatatable();

            }
        }
    });
}


function showDataHistory(id) {

    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/approved/history/data/" + id,
        type: "GET",
        dataType: "json",
        success: function (res) {
            if (res.ok == true) {
                res = res.data;

                for (var i in res) {
                    $(`#${i}`).html('');
                    if (res.hasOwnProperty(i)) {

                        if (res[i] == res.status_id) {
                            if (res.status_id == 2) {
                                $(`#${i}`).html("Aprobada")
                            } else if (res.status_id == 3) {
                                $(`#${i}`).html("Denegada")

                            }
                        } else {
                            $(`#${i}`).html(res[i])
                        }
                    }
                }


                $('#btnProductsProductionCenter').attr('onclick', 'showProductionCenterDetail('+res.id+')');

            }
        }
    });

}


function showProductionCenterDetail(id) {
    $.ajax({
        url:'/produccioncenter/'+id,
        type:'get',
        datatype:'json',
        success:function(r){
            if(r.ok){
                let h = r.data.head
                let p = r.data.products
                console.log(h)
                $("#dateShow").val(h.order_date)
                $("#titleShow").val(h.title)
                $("#quantity_peopleShow").val(h.quantity_people)
                $("#locationShow").val(h.center.name)
                $("#descripcionShow").val(h.descripcion)

                $("#bodyProductsShow").html('')
                let costShow = 0;
                p.forEach(function(e,i) {
                    $("#bodyProductsShow").append(` <tr>
                                                        <td>${(i+1)}</td>
                                                        <td>${e.name}</td>
                                                        <td>${e.unitPrice}</td>
                                                        <td>${e.quantity}</td>
                                                        <td>$ ${new Intl.NumberFormat().format(e.subtotal)}</td>
                                                    </tr>`)
                    costShow=costShow+e.subtotal
                })
                $("#costShow").html(new Intl.NumberFormat().format(costShow))
                $("#show").modal('toggle')
            }else{
                toastr('danger','Error',r.message)
            }
        }
    })
}
