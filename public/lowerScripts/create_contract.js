let registerContract = false;
let proveedor = null;
let Products = new Array();
let numProducts = 0;
$(document).ready(function () {

    $("#wizard-contract").steps({

        labels: {
            finish: "Registrar contrato",
            next: "Siguiente",
            previous: "Anterior"
        },
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "fade",
        autoFocus: true,
        titleTemplate: "#title#",
        onStepChanging: function (event, currentIndex, newIndex) {
            if (proveedor == null) {
                toastr('error', 'Acción requerida', 'Seleccione un proveedor');
                return false;
            }

            if (newIndex == 2) {
                if (Products.length == 0) {
                    toastr('error', 'Acción requerida', 'Seleccione un producto');
                    return false;
                }
            }

            addProductsTable();

            if (newIndex == 3) {
                for (product of Products) {
                    if (!product.status) {
                        toastr('error', 'Acción requerida', 'Debe completar todos los productos');
                        return false;
                    }
                }
            }
            $('#resumen_provider').text(proveedor.name + " " + proveedor.last_name + " / " + proveedor.contact_name)
            $('#resumen_product').text(`${Products.length} Seleccionados de  ${numProducts} productos `);

            return true;

        },
        onFinished: function (event, currentIndex) {
            if (!registerContract) {

                if ($('#startDate').val() == "" || $('#endDate').val() == "" || $('#num_contract').val() == "") {
                    toastr('error', 'Acción requerida', 'Complete los campos ,por favor');

                } else {
                    if ($('#input-file-now').val() == "") {
                        toastr('warning', 'Acción requerida', 'Suba el comprobante del contrato');
                    } else {
                        registerContract = true;
                        var file_data = $('#input-file-now').prop('files')[0];
                        var form_data = new FormData();
                        form_data.append('file', file_data);
                        form_data.append('Products', JSON.stringify(Products));
                        form_data.append('Provider', JSON.stringify(proveedor));
                        form_data.append('startDate', $('#startDate').val());
                        form_data.append('endDate', $('#endDate').val());
                        form_data.append('num_contract', $('#num_contract').val());
                        $.ajax({
                            url: "/contracts/create",
                            type: "POST",
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: (data) => {
                                if (data.status) {
                                    toastr('success', 'Contrato', 'El contrato fue registrado exitosamente,espera un momento');
                                    setTimeout(() => {
                                        window.location.href = "/contracts";
                                    }, 3000);

                                } else {
                                    toastr('warning', 'Atención', 'Tenemos problemas para registrar el contrato');
                                    registerContract = false;
                                }
                            },
                            error: (e) => {
                                toastr('warning', 'Atención', 'Tenemos problemas ,recargue la página');
                            }
                        });

                    }
                }
            } else {
                toastr('warning', 'Atención', 'Lo sentimos , espera un momento para volver a intentarlo');
            }

        }
    });

    listProviders();
    listProducts();

    //fechas de inicio y finalidad de un contrato
    var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

    $('#startDate').datepicker({
        dateFormat: 'yyyy-mm-dd',
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        minDate: today,
        maxDate: function () {
            return $('#endDate').val();
        }
    });

    $('#endDate').datepicker({
        dateFormat: 'yyyy-mm-dd',
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        minDate: function () {
            return $('#startDate').val();
        }
    });

    //manejo de archivos con dropify
    $('.dropify').dropify({
        error: {
            ' fileSize ': ' El tamaño del archivo es demasiado grande ({{value}} max). ',
            ' imageFormat ': ' El formato no está permitido (solo {{value}}). '
        },
        messages: {
            'default': 'Arrastra y suelta un archivo aquí o haz clic',
            'replace': 'Arrastra y suelta o haz clic para reemplazar',
            'remove': 'Remover',
            'error': 'Ooops, sucedió algo malo.'
        }
    });

});
function listProviders() {
    $.ajax({
        url: '/contracts/providers/actives',
        type: 'GET',
        datatype: 'json',
        success: function (response) {
            if (response.ok == false) {
                toastr('info', 'Atención', response.message);
            } else {

                response.data.forEach(function (e, i) {
                    $("#tableProvidersBody").append(
                        `<tr>
                            <td>${e.NIT}</td>
                            <td>${e.name}</td>
                            <td>${e.contact_name + " " + e.last_name}</td>
                            <td><button type="button" class="btn btn-outline-success btn-icon btn-icon-circle btn-success add-proveedor" onclick="addProvider('${e.id}')"><span class="btn-icon-wrap" ><i class="fa fa-plus"></i></span></button> </td>
                        </tr>`);
                });

                $('#providers').DataTable({
                    responsive: true,
                    autoWidth: true,
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                    }
                });
            }

        }
    });

}
function toastr(type, tittle, text) {
    $.toast({
        heading: '' + tittle + '',
        text: '' + text + '',
        position: 'top-right',
        loaderBg: '#000',
        class: 'jq-has-icon jq-toast-' + type,
        hideAfter: 3500,
        stack: 6,
        showHideTransition: 'fade'
    });
}
function addProvider(id) {
    $.ajax({
        url: '/provider/' + id,
        type: 'GET',
        datatype: 'json',
        success: function (response) {
            if (response.ok) {
                proveedor = response.data;
                toastr('success', 'Agregado', 'Proveedor agregado correctamente');
            } else {
                toastr('warning', 'Atención', response.message);
            }


        }
    });
}
//añade un solo producto
function addProduct(product) {

    for (var i in Products) {
        if (Products[i].id === product.id) {
            Products.splice(i, 1);
            $('#products-' + product.id).html(`<label class="customcheck"><input type="checkbox" class="check-products" id="product-${product.id}"
     onclick='addProduct(${JSON.stringify(product)})' data-id="${product.id}"> <span class="checkmark"></span></label>`);
            toastr('success', 'Eliminado', 'Producto Eliminado correctamente');
            return;
        }
    }

    Products.push(product);
    $('#products-' + product.id).html(`<label class="customcheck"><input type="checkbox" class="check-products" id="product-${product.id}"
     onclick='addProduct(${JSON.stringify(product)})' data-id="${product.id}"
      checked="checked" > <span class="checkmark"></span></label>`);
    toastr('success', 'Agregado', 'Producto añadido correctamente');

}
//añade y selecciona todos los productos
function addProducts() {

    if (Products.length == 0 || Products.length > 0) {
        $.ajax({
            url: '/contracts/products/actives',
            type: 'GET',
            datatype: 'json',
            success: function (response) {
                if (response.ok) {
                    Products = new Array();
                    Products = response.data;
                    AllChecked();
                    $('#products-all').attr('onclick', 'deleteProducts()');
                    toastr('success', 'Agregados', 'todos los Productos fueron agregados');
                }

            }
        });
    }

}
//elimina todos los productos  en el vector y elimina todos los productos seleccionados 
function deleteProducts() {
    Products = new Array();
    AllClearChecked();
    $('#products-all').attr('onclick', 'addProducts()');
    toastr('success', 'Eliminados', 'Todos los producto Eliminados correctamente');
}
//seleccionar todos los productos que no han sido seleccionados
function AllChecked() {
    $('.check-products').each(function (i) {
        if (i != 0) {
            var id = $(this).data('id');
            if (!$(this).is(':checked')) {
                $('#product-' + id).attr('checked', 'checked');
            }
        }
    });
}

function AllClearChecked() {
    $('.check-products').each(function (i) {
        var id = $(this).data('id');
        if ($(this).is(':checked')) {
            $('#product-' + id).removeAttr('checked');
        }
    });
}

function listProducts() {
    $.ajax({
        url: '/contracts/products/actives',
        type: 'GET',
        datatype: 'json',
        success: function (response) {
            if (response.ok == false) {
                toastr('info', 'Atención', response.message);
            } else {

                response.data.forEach(function (e, i) {
                    e.status = false;
                    $("#tableProductsBody").append(
                        `<tr>
                            <td>${e.name}</td>
                            <td>${e.category}</td>
                            <td>${e.measure}</td>
                            <td>${e.presentation}</td>
                            <td>${e.iva} %</td>
                            <td style="padding:1em;" id="products-${e.id}"> <label class="customcheck"><input type="checkbox" class="check-products" id="product-${e.id}" onclick='addProduct(${JSON.stringify(e)})' data-id="${e.id}" > <span class="checkmark"></span></label></td>
                        </tr>`);
                });

                $('#products').DataTable({
                    "lengthMenu": [[-1], ["Todos"]],
                    responsive: true,
                    autoWidth: true,
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                    }
                });
                numProducts = response.data.length;
            }

        }
    });
}

function addProductsTable() {
    $('#detailsProducts').html(`
    <table id="productsDetails" class="table table-hover w-100 ">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Categoria</th>
                <th>Unidad medida</th>
                <th>Presentación</th>
                <th>Iva % </th>
                <th>Estado</th>
                <th>Editar</th>
            </tr>
        </thead>
        <tbody id="tableProductsDetailsBody"></tbody>
    </table>`);

    var template = ``;
    for (var i in Products) {
        template +=
            `<tr>
            <td>${Products[i].name}</td>
            <td>${Products[i].category}</td>
            <td>${Products[i].measure}</td>
            <td>${Products[i].presentation}</td>
            <td>${Products[i].iva} % </td>
            <td id="status-product-${Products[i].id}">${Products[i].status === true ? '<button type="button" class="btn btn-success btn-rounded btn-xs">Completado</button>' : '<button type="button" class="btn btn-danger btn-rounded btn-xs">Por completar</button>'}</td>
            <td ><button  type="button" onclick='editProdut(${JSON.stringify(Products[i])},${i})' class="btn btn-outline-success btn-icon btn-icon-circle btn-success btn-icon-style-2"><span class="btn-icon-wrap" ><i class="fa fa-pencil"></i></span></button></td>
        </tr>`
    }
    $('#tableProductsDetailsBody').html(template);

    $('#productsDetails').DataTable({
        responsive: true,
        autoWidth: false,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });


}

function editProdut(product, index) {
    //regresamos el color del input:Price_product
    $('#Price_product').css('border-color', '#e0e3e4');
    //mostramos la información del producto

    $('#editProductId').val(product.id);
    $('#Index').val(index);
    $('#name_product').val(product.name);
    $('#category_product').val(product.category);
    $('#measure_product').val(product.measure);
    $('#presentation_product').val(product.presentation);
    $('#iva_product').val(product.iva + " %");
    $('#iva_total').val("");
    $('#val_total').val("");

    if (product.status) {
        $('#Price_product').val(product.unit_price);
        GeneratePrices();
    } else {
        $('#Price_product').val("");
    }
    $('#editProducts').modal('show');
}

function GeneratePrices() {

    try {
        let price = $('#Price_product').val();
        let iva = $('#iva_product').val();

        if (iva != "" && price != "") {

            iva = parseInt(iva);
            price = parseInt(price);

            var TotalIva = Math.round(price * (iva / 100));
            var Total = Math.round((price * (iva / 100)) + price);

            $('#iva_total').val(TotalIva);
            $('#val_total').val(Total);

        } else {
            $('#iva_total').val(0);
            $('#val_total').val(0);
        }
    } catch (error) {
        toastr('error', 'Atención', 'Tenemos problemas recargue la pagína ,por favor.');
    }
}

function updateStatus() {
    try {

        if ($('#Index').val() != "" && $('#editProductId').val() != "" &&
            $('#iva_total').val() != "" && $('#val_total').val() != ""
            && $('#Price_product').val() != "") {

            var index = parseInt($('#Index').val());
            Products[index].totalIva = parseInt($('#iva_total').val());
            Products[index].unit_price = parseInt($('#Price_product').val());
            Products[index].status = true;
            toastr('success', 'Producto', 'Producto actualizado correctamente');
            $('#status-product-' + Products[index].id).html(`<button type="button" class="btn btn-success btn-rounded btn-xs">Completado</button>`);
            $('#editProducts').modal('toggle');
            $('#Price_product').css('border-color', '#e0e3e4');
        } else {
            toastr('error', 'Atención', 'Complete los campos ,por favor');
            $('#Price_product').css('border-color', 'red');
        }
    } catch (error) {
        toastr('error', 'Atención', 'Tenemos problemas recargue la pagína ,por favor.');
    }
}