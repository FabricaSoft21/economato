
$(document).ready(function(){
    budget();
});

var money = [];
var spent = [];
var color = [];
var charact = [];
function budget(){
    var id = $("#budget_id").val();

    if(id!=null){
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url : '/charts/'+id,
        dataType : 'json',
        success : function(r){
            r.moneyXchart.forEach(function(e, i){
                money.push(e.porcentage, e.name)
            });
            charact.push('Producción de centro')
            spent.push(r.production[0].produc_total);
            r.spent_charact.forEach(function(e, i){
                charact.push(e.characterization)
                spent.push(e.total)
            });

            console.log(charact);
            console.log(spent);
            if( $('#e_chart_9').length > 0 ){
                var eChart_9 = echarts.init(document.getElementById('e_chart_9'));
                
                var option8 = {
                    tooltip: {
                        show: true,
                        backgroundColor: '#fff',
                        borderRadius:6,
                        padding:6,
                        axisPointer:{
                            lineStyle:{
                                width:0,
                            }
                        },
                        textStyle: {
                            color: '#324148',
                            fontFamily: '"Poppins", sans-serif',
                            fontSize: 12
                        }	
                    },
                    series: [
                        {
                            name:'',
                            type:'pie',
                            radius : '60%',
                            center : ['50%', '50%'],
                            roseType : 'radius',
                            color: ['#0A7F78', '#61FFF4', '#14FFEF', '#228A83', '#10CCBF','#0A6778' ],
                            data: money,
                            label: {
                                normal: {
                                    formatter: '{b}\n{d}%'
                                },
                          
                            }
                        }
                    ]
                };
                eChart_9.setOption(option8);
                eChart_9.resize();
            }
           //Spent
           if( $('#e_chart_1').length > 0 ){
            var eChart_1 = echarts.init(document.getElementById('e_chart_1'));
            var option = {
                color: ['#0A7F78', '#61FFF4', '#14FFEF', '#228A83', '#10CCBF','#0A6778' ],
                tooltip: {
                    show: true,
                    trigger: 'axis',
                    backgroundColor: '#fff',
                    borderRadius:6,
                    padding:6,
                    axisPointer:{
                        lineStyle:{
                            width:0,
                        }
                    },
                    textStyle: {
                        color: '#324148',
                        fontFamily: '"Nunito", sans-serif',
                        fontSize: 12
                    }
                },
                
                xAxis: [{
                    type: 'category',
                    data: charact,
                    axisLine: {
                        show:false
                    },
                    axisTick: {
                        show:false
                    },
                    axisLabel: {
                        textStyle: {
                            color: '#5e7d8a'
                        }
                    }
                }],
                yAxis: {
                    type: 'value',
                    axisLine: {
                        show:false
                    },
                    axisTick: {
                        show:false
                    },
                    axisLabel: {
                        textStyle: {
                            color: '#5e7d8a'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: 'transparent',
                        }
                    }
                },
                grid: {
                    top: '3%',
                    left: '3%',
                    right: '3%',
                    bottom: '3%',
                    containLabel: true
                },
                series: [{
                    data: spent,
                    type: 'bar',
                    barMaxWidth: 30,
                    itemStyle: {
                        normal: {
                            barBorderRadius: [6, 6, 0, 0] ,
                        }
                    },
                }]
            };
            eChart_1.setOption(option);
            eChart_1.resize();
        }
        }
    });
}
}



