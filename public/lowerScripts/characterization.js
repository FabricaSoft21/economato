var table = false;

$(document).ready(function () {
    listCharact();
});

function applyDataTable() {
    $("#charactTable").DataTable({
        responsive: true,
        autoWidth: false,
        language: {
            search: "",
            searchPlaceholder: "Buscar",
            sLengthMenu: "_MENU_items"
        }
    });

    table = true;
}

function cleanInputs() {

    $("#name").val("");
    $("#name").removeClass("is-invalid");

    $("#names").val("");
    $("#names").removeClass("is-invalid");


}

function listCharact() {
    $("#tableCharactBody").html("");
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/characterization/data",
        type: "GET",
        datatype: "json",
        success: function (response) {
            console.log(response);
            if (response.ok == false) {
                toastr("info", "Atención", response.message);
            } else {
                response.data.forEach(function (e, i) {
                    if (e.status == 0) {
                        var state =
                            '<button type="button" onclick="change_state(' +
                            e.id +
                            ')" class="btn btn-success btn-rounded btn-xs" style="width: 67px;">Activo</button>';
                    } else {
                        var state =
                            '<button type="button" onclick="change_state(' +
                            e.id +
                            ')" class="btn btn-danger btn-rounded btn-xs" style="width: 67px;">Inactivo</button>';
                    }
                    $("#tableCharactBody").append(
                        "<tr>" +
                        "<th>" +
                        (i + 1) +
                        "</th>" +
                        "<td>" +
                        e.name +
                        "</td>" +
                        "<td>" +
                        state +
                        "</td>" +
                        "<td>" +
                        '<button onclick="edit_charact(' +
                        e.id +
                        ')" data-toggle="modal" data-target="#see_charact"  class="btn btn-outline-success btn-icon btn-icon-circle btn-success btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-pencil"></i></span></button>' +

                        '<button onclick="see_charact(' +
                        e.id +
                        ')" data-toggle="modal" data-target="#see_charact"  class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-eye"></i></span></button>' +
                        "</td>" +
                        "</tr>"
                    );
                });

                if (table == false) {
                    applyDataTable();
                }
            }
        }
    });
}

function change_state(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/characterization/delete",
        type: "GET",
        datatype: "json",
        data: {
            id: id
        },
        success: function (response) {
            listCharact();
        }
    });
}

function register() {

    var nombre = $("#name").val();


    $.ajax({
        url: "/characterization",

        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "POST",
        data: {

            name: nombre,

        },
        dataType: "json",
        success: function (response) {
            if (response.ok == true) {

                listCharact();
                toastr("success", "Atención", response.message);
                $("#register").modal("toggle");
                cleanInputs();

            } else if (response.ok == false) {
                if (response.message) {
                    listAlerts(response.message);
                } else {
                    toastr("danger", "Atención", response.error);
                }
            }
        }
    });
}

function see_charact(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/characterization/" + id,
        type: "GET",
        dataType: "json",
        success: function (res) {

            if (res.ok == true) {
                res = res.data;


                $("#names").val(res.name);
                $("#names").prop("disabled", true);


                $("#buttonEdit").hide();
                $("#see_charact").modal("toggle");


            } else if (res.ok == false) {
                toastr("danger", "Error", res.message);
            }
        }
    });
}

function edit_charact(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },

        url: "/characterization/" + id,

        type: "GET",
        dataType: "json",
        success: function (res) {
            if (res.ok == true) {
                res = res.data;


                $("#names").val(res.name);
                $("#names").prop("disabled", false);

                $("#see_charact").modal("toggle");
                $("#buttonEdit").show();

                $("#buttonEdit").attr("onclick", `update_charact(${id})`);

            } else if (res.ok == false) {
                toastr("danger", "Error", res.message);
            }
        }
    });
}

function update_charact(id) {
    var nombre = $("#names").val();


    if (nombre == "") {
        toastr("danger", "Error", "Debes de ingresar el nombre de la caracterización.");
        return false;
    }

    bander_upd = true;

    if (bander_upd == true) {
        $.ajax({

            url: "/characterization/update",

            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            type: "POST",
            dataType: "json",
            data: {
                id: id,

                name: nombre,


            },
            success: function (response) {
                if (response.ok == true) {
                    toastr("success", "Hecho!", response.message);
                    listCharact();
                    $("#see_charact").modal("toggle");
                }
                if (response.ok == false) {
                    listAlertsUpdate(response.message);
                }
            }
        });
        return false;
    }
}

function listAlerts(e) {


    //register

    if (e.name) {
        toastr("danger", "Error", e.name[0]);
        $("#name").addClass("is-invalid");
    }




}

function listAlertsUpdate(e) {

    if (e.name) {
        toastr("danger", "Error", e.name[0]);
        $("#names").addClass("is-invalid");
    }


}

function toastr(type, tittle, text) {
    $.toast({
        heading: "" + tittle + "",
        text: "" + text + "",
        position: "top-right",
        loaderBg: "#000",
        class: "jq-has-icon jq-toast-" + type,
        hideAfter: 3500,
        stack: 6,
        showHideTransition: "fade"
    });
}

