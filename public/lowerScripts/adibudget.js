var table = false;

$(document).ready(function () {
    list_aditional_budget();
});

function applyDataTable() {
    $("#adiBudgetTable").DataTable({
        responsive: true,
        autoWidth: false,
        language: {
            search: "",
            searchPlaceholder: "Buscar",
            sLengthMenu: "_MENU_items"
        }
    });

    table = true;
}

function cleanInputs() {
    $("#budget_id-r").val("");
    $("#budget_id-r").removeClass("is-invalid");

    $("#aditional_budget-r").val("");
    $("#aditional_budget-r").removeClass("is-invalid");

    $("#aditional_budget_code-r").val("");
    $("#aditional_budget_code-r").removeClass("is-invalid");

    $("#aditional_begin_date-r").val("");
    $("#aditional_begin_date-r").removeClass("is-invalid");

    $("#aditional_finish_date-r").val("");
    $("#aditional_finish_date-r").removeClass("is-invalid");
}

function list_aditional_budget() {
    $("#table_aditional_budget_body").html("");
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/adibudget/data",
        type: "GET",
        datatype: "json",
        success: function (response) {
            console.log(response);
            if (response.ok == false) {
                toastr("info", "Atención", response.message);
            } else {
                response.data.forEach(function (e, i) {
                    if (e.status == 0) {
                        var state =
                            '<button type="button" onclick="change_state(' +
                            e.id +
                            ')" class="btn btn-success btn-rounded btn-xs" style="width: 67px;">Activo</button>';
                    } else {
                        var state =
                            '<button type="button" onclick="change_state(' +
                            e.id +
                            ')" class="btn btn-danger btn-rounded btn-xs" style="width: 67px;">Inactivo</button>';
                    }
                    $("#table_aditional_budget_body").append(
                        "<tr>" +
                        "<th>" +
                        (i + 1) +
                        "</th>" +
                        "<td>" +
                        e.budgetss +
                        "</td>" +
                        "<td>" +
                        e.aditional_budget +
                        "</td>" +
                        "<td>" +
                        e.aditional_budget_code +
                        "</td>" +
                        "<td>" +
                        e.aditional_begin_date +
                        "</td>" +
                        "<td>" +
                        e.aditional_finish_date +
                        "</td>" +
                        "<td>" +
                        state +
                        "</td>" +
                        "<td>" +
                        '<button onclick="see_aditional_budget(' +
                        e.id +
                        ')" data-toggle="modal" data-target="#see_aditional_budget"  class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-eye"></i></span></button>' +

                        "</tr>"
                    );
                    // $("#table_aditional_budget_body").append(`a es igual: ${a}, b es igual: ${b}`);
                });

                if (table == false) {
                    applyDataTable();
                }
            }
        }
    });
}

function change_state(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/adibudget/delete",
        type: "GET",
        datatype: "json",
        data: {
            id: id
        },
        success: function (response) {
            console.log(response);
            list_aditional_budget();
        }
    });
}

function save_aditional_budget() {
    var presupuesto = $("#budget_id-r").val();
    var presupuestoAdicional = $("#aditional_budget-r").val();
    var codigo = $("#aditional_budget_code-r").val();
    var inicioPresupuesto = $("#aditional_begin_date-r").val();
    var finPresupuesto = $("#aditional_finish_date-r").val();

    var bander_save = false;

    console.log(presupuesto);
    if (presupuesto == "") {
        toastr("danger", "Error", "Debes seleccionar el código del presupuesto.");
        return false;
    }

    if (presupuestoAdicional == "") {
        toastr("danger", "Error", "Debes de ingresar el presupuesto adicional.");
        return false;
    }

    if (codigo == "") {
        toastr(
            "danger",
            "Error",
            "Debes de ingresar el codigo del Presupuesto adicional."
        );
        return false;
    }

    if (inicioPresupuesto == "") {
        toastr(
            "danger",
            "Error",
            "Debes de ingresar el inicio del Presupuesto."
        );
        return false;
    }

    if (finPresupuesto == "") {
        toastr(
            "danger",
            "Error",
            "Debes de ingresar el fin del Presupuesto."
        );
        return false;
    }

    if (finPresupuesto < inicioPresupuesto) {
        toastr(
            "info",
            "Atención",
            "La fecha de presupuesto final no puede ser menor a la fecha inicial"
        );
        return false;
    }

    bander_save = true;

    if (bander_save == true) {
        $.ajax({
            url: "/adibudget",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            type: "POST",
            data: {
                budget_id: presupuesto,
                aditional_budget: presupuestoAdicional,
                aditional_budget_code: codigo,
                aditional_begin_date: inicioPresupuesto,
                aditional_finish_date: finPresupuesto,
            },
            dataType: "json",

            success: function (response) {
                if (response.ok == true) {
                    list_aditional_budget();
                    toastr("success", "Atención", response.message);
                    cleanInputs();
                    $("#register").modal("toggle");
                } else if (response.ok == false) {
                    if (response.message) {
                        listAlerts(response.message);
                    } else {
                        toastr("danger", "Atención", response.error);
                    }
                }
            }
        });
    }
}

function see_aditional_budget(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/adibudget/" + id,
        type: "GET",
        dataType: "json",
        success: function (res) {
            if (res.ok == true) {
                res = res.data;


                $("#budget_id").val(res.budget.budget_code);
                $("#budget_id").prop("disabled", true);

                $("#aditional_budget").val(res.aditional_budget);
                $("#aditional_budget").prop("disabled", true);

                $("#aditional_budget_code").val(res.aditional_budget_code);
                $("#aditional_budget_code").prop("disabled", true);

                $("#aditional_begin_date").val(res.aditional_begin_date);
                $("#aditional_begin_date").prop("disabled", true);

                $("#aditional_finish_date").val(res.aditional_finish_date);
                $("#aditional_finish_date").prop("disabled", true);

                $("#see_aditional_budget").modal("toggle");
            } else if (res.ok == false) {
                toastr("danger", "Error", res.message);
            }
        }
    });
}

function listAlerts(e) {
    if (e.budget_code) {
        toastr("danger", "Error", e.budget_code[0]);
        $("#initial_budget").addClass("is-invalid");
    }

    if (e.aditional_budget) {
        toastr("danger", "Error", e.aditional_budget[0]);
        $("#aditional_budget").addClass("is-invalid");
    }


    if (e.aditional_budget_code) {
        toastr("danger", "Error", e.aditional_budget_code[0]);
        $("#budget_code").addClass("is-invalid");
    }

    if (e.aditional_begin_date) {
        toastr("danger", "Error", e.aditional_begin_date[0]);
        $("#budget_begin").addClass("is-invalid");
    }
    if (e.aditional_finish_date) {
        toastr("danger", "Error", e.aditional_finish_date[0]);
        $("#budget_finish").addClass("is-invalid");
    }
}

function toastr(type, tittle, text) {
    $.toast({
        heading: "" + tittle + "",
        text: "" + text + "",
        position: "top-right",
        loaderBg: "#000",
        class: "jq-has-icon jq-toast-" + type,
        hideAfter: 3500,
        stack: 6,
        showHideTransition: "fade"
    });
}