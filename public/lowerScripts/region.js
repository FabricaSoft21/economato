var table = false;

$(document).ready(function () {
    listRegions();
});


function applyDataTable() {
    $("#regionTable").DataTable({
        responsive: true,
        autoWidth: false,
        language: {
            search: "",
            searchPlaceholder: "Buscar",
            sLengthMenu: "_MENU_items"
        }
    });

    table = true;
}


function listRegions() {
    $("#tableRegionBody").html("");
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/region/data",
        type: "GET",
        datatype: "json",
        success: function (response) {
            console.log(response);
            if (response.ok == false) {
                toastr("info", "Atención", response.message);
            } else {
                response.data.forEach(function (e, i) {
                    if (e.status == 0) {
                        var state =
                            '<button type="button" onclick="change_state(' +
                            e.id +
                            ')" class="btn btn-success btn-rounded btn-xs" style="width: 67px;">Activo</button>';
                    } else {
                        var state =
                            '<button type="button" onclick="change_state(' +
                            e.id +
                            ')" class="btn btn-danger btn-rounded btn-xs" style="width: 67px;">Inactivo</button>';
                    }
                    $("#tableRegionBody").append(
                        "<tr>" +
                        "<th>" +
                        (i + 1) +
                        "</th>" +
                        "<td>" +
                        e.name +
                        "</td>" +
                        "<td>" +
                        '<button onclick="see_region(' +
                        e.id +
                        ')" data-toggle="modal" data-target="#see_region"  class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-eye"></i></span></button>' +
                        "</td>" +
                        "</tr>"
                    );
                });

                if (table == false) {
                    applyDataTable();
                }
            }
        }
    });
}


function cleanInputs() {
    $("#name").val("");
    $("#name").removeClass("is-invalid");


}


function register() {
    var nombreRegion = $("#name").val();

    $.ajax({
        url: "/region",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "POST",
        data: {
            name: nombreRegion,

        },
        dataType: "json",

        success: function (response) {
            if (response.ok == true) {
                listRegions();
                toastr("success", "Atención", response.message);
                $("#register").modal("toggle");
                cleanInputs();
                removeClass();
            } else if (response.ok == false) {
                if (response.message) {
                    listAlerts(response.message);
                } else {
                    toastr("danger", "Atención", response.error);
                }
            }
        }
    });
}


function see_region(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/region/" + id,
        type: "GET",
        dataType: "json",
        success: function (res) {
            if (res.ok == true) {
                res = res.data;
                console.log(res)
                $("#names").val(res.name);
                $("#names").prop("disabled", true);

                $("#buttonEdit").hide();
                $("#see_region").modal("toggle");
            } else if (res.ok == false) {
                toastr("danger", "Error", res.message);
            }
        }
    });
}


function listAlerts(e) {
    if (e.nombreRegion) {
        toastr("danger", "Error", e.nombreRegion[0]);
        $("#name").addClass("is-invalid");
    }

  
}

function toastr(type, tittle, text) {
    $.toast({
        heading: "" + tittle + "",
        text: "" + text + "",
        position: "top-right",
        loaderBg: "#000",
        class: "jq-has-icon jq-toast-" + type,
        hideAfter: 3500,
        stack: 6,
        showHideTransition: "fade"
    });
}

