var table = false; 

function applyDataTable() {
    $("#requestTable").DataTable({
        responsive: true,
        autoWidth: false,
        language: {
            search: "",
            searchPlaceholder: "Buscar",
            sLengthMenu: "_MENU_items"
        }
    });
    table = true;
}
function toastr(type, tittle, text) {
    $.toast({
        heading: "" + tittle + "",
        text: "" + text + "",
        position: "top-right",
        loaderBg: "#000",
        class: "jq-has-icon jq-toast-" + type,
        hideAfter: 3500,
        stack: 6,
        showHideTransition: "fade",
    });
}

function loadInformation(idRequest){
    $.get("/requestInformation/" + idRequest, function (response) {
        showRequest(response);
    });
}

/*
function loadProduct(idProduct){

    $.get("/product/objProduct/" + idProduct, await function (response) {
         product = response.data;
    });

}
*/

function showRequest(response){

    console.log(response);

    $("#showFile").text(response.data['file'].number);
    $("#nApprentices").text(response.data['file'].apprentices);
    $("#characterization").text(response.data['file'].characterization.name);

    $("#responsible").text(response.data.request.user.name);
    $("#identity").text(response.data.request.user.identification);

    if(response.data['file'].characterization.status==0){
        $("#status").text("En ejecucion.");
    }else{
        $("#status").text("Finalizada.");
    }
    $("#ShowDescription").val(response.data['request'].description);
  
    
    $("#listProductsShow").html('')
    cost = 0

    response.data.orderXProducts.forEach(function (e, i) {


        $("#listProductsShow").append(` <tr>
                                        <th>${(i + 1)}</th>
                                        <td>${e.product_xcontract.product.name}</td>
                                        <td>${e.product_xcontract.unit_price}</td>
                                        <td>${e.quantity}</td>
                                        <td>$ ${new Intl.NumberFormat().format(e.subtotal)}</td>
                                        <td>
                                            
                                        </td>
                                    </tr>`)
        cost=cost+e.subtotal

    })
    $("#showCost").html(new Intl.NumberFormat().format(cost))







    $("#div-edit").hide()
    $("#buttonEdit").hide()
    $("#showInformation").modal('toggle');

}