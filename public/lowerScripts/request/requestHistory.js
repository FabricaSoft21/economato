
$(document).ready(function () {
    listHistoryRequest();

});



function applyDatatableRequest() {
    $("#requestTable").DataTable({
        responsive: true,
        autoWidth: false,
        language: {
            search: "",
            searchPlaceholder: "Buscar",
            sLengthMenu: "_MENU_items"
        }
    });
}

function loadInformationProducts(idRequest){
    $.get("/requestInformation/" + idRequest, function (response) {
        showRequestDetail(response);
    });
}

function showRequestDetail(response){

    console.log(response);

    $("#showFile").text(response.data['file'].number);
    $("#nApprentices").text(response.data['file'].apprentices);
    $("#characterization").text(response.data['file'].characterization.name);

    $("#responsible").text(response.data.request.user.name);
    $("#identity").text(response.data.request.user.identification);

    if(response.data['file'].characterization.status==0){
        $("#status").text("En ejecucion.");
    }else{
        $("#status").text("Finalizada.");
    }
    $("#ShowDescription").val(response.data['request'].description);
  
    
    $("#listProductsShow").html('')
    cost = 0

    response.data.orderXProducts.forEach(function (e, i) {


        $("#listProductsShow").append(` <tr>
                                        <th>${(i + 1)}</th>
                                        <td>${e.product_xcontract.product.name}</td>
                                        <td>${e.product_xcontract.unit_price}</td>
                                        <td>${e.quantity}</td>
                                        <td>$ ${new Intl.NumberFormat().format(e.subtotal)}</td>
                                        <td>
                                            
                                        </td>
                                    </tr>`)
        cost=cost+e.subtotal

    })
    $("#showCost").html(new Intl.NumberFormat().format(cost))







    $("#div-edit").hide()
    $("#buttonEdit").hide()
    $("#showInformation").modal('toggle');

}



function listHistoryRequest() {
    $("#tableRequestHistoryBody").html("");
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/request/data/history",
        type: "GET",
        datatype: "json",

        success: function (response) {

            if (response.ok == false) {
                toastr("info", "Atención", response.message);

            } else {

                response.data.forEach(function (e, i) {
                    if (e.status_id == 2) {
                        var state =
                            '<span  class="text-success">Aprobado</span>';
                    } if (e.status_id == 3) {
                        var state =
                            '<span  class="text-danger">Denegado</span>';
                    }
                    if (e.status_id == 1) {
                        var state =
                            '<span  class="text-primary">Por aprobar</span>';
                    }
                    if (e.status_id == 4) {
                        var state =
                            '<span  class="text-success">Entregado</span>';
                    }
                    $("#tableRequestHistoryBody").append(
                        "<tr>" +
                        "<td>" + (i + 1) + "</td>" +
                        "<td>" + e.user.name + "</td>" +
                        "<td>" + e.file.number + "</td>" +
                        "<td>" + e.cost + "</td>" +
                        "<td>" + state + "</td>" +
                        "<td>" + e.description + "</td>" +
                        "<td>" +

                        '<button onclick="showRequestHistory(' +
                        e.id +
                        ')" data-toggle="modal" data-target="#showHistoryRequest" class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-eye"></i></span></button>' +
                        "</td>" +
                        "</tr>"
                    );


                });
                applyDatatableRequest();

            }
        }
    });
}


function showRequestHistory(id) {

    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/request//history/" + id,
        type: "GET",
        dataType: "json",
        success: function (res) {
            if (res.ok == true) {
                res = res.data;
                console.log(res)

                for (var i in res) {
                    $(`#${i}`).html('');
                    if (res.hasOwnProperty(i)) {

                        if (res[i] == res.status_ids) {
                            if (res.status_ids == 2) {
                                $(`#${i}`).html("Aprobada")
                            } else if (res.status_ids == 3) {
                                $(`#${i}`).html("Denegada")

                            } else if (res.status_ids == 1) {
                                $(`#${i}`).html("Por aprobar")
                            } else if (res.status_ids == 4) {
                                $(`#${i}`).html("Entregado")
                            }
                        } else {
                            $(`#${i}`).html(res[i])
                        }
                    }
                }


                $('#btnProducts').attr('onclick', 'loadInformationProducts('+res.id+')');

            }
        }
    });

}



