$(document).ready(function() {
    listRequest()
});
function listRequest() {
    $("#tableRequestBody").html("");
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/request/data",
        type: "GET",
        datatype: "json",
        success: function(response) {
            if (response.ok == false) {
                toastr("info", "Atención", response.message);
            } else {
                n=1
                response.data.forEach(function(e, i) {
                    if (e.status_id==1) {
                        $("#tableRequestBody").append(
                            `<tr>
                                <td>${n}</td>
                                <td>${e.userName}</td>
                                <td>${e.date}</td>
                                <td>${e.file}</td>
                                <td style="text-align: center;">
                                    <div id="loading_${e.id}" style="display:none;">
                                        <img src="/gifs/loading.gif"   style="height: 20px;width: 20px;">
                                    </div>
                                    <div id="buttons_${e.id}">
                                        <button type="button" onclick="changeState(${e.id},2)" class="btn btn-success btn-rounded btn-xs">Aprobar</button>
                                        <button type="button" onclick="changeState(${e.id},3)" class="btn btn-danger btn-rounded btn-xs">Anular</button>
                                    </div>
                                </td>
                                <td> 
                                    <button onclick="loadInformation(${e.id})"class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap" ><i class="fa fa-eye"></i></span></button>                            
                                </td>
                            </tr>`)
                            n++
                    }
                });

                if (table == false) {
                    applyDataTable();
                }
            }
        }
    });
}
function changeState(id,newState) {

    $(`#loading_${id}`).show()
    $(`#buttons_${id}`).hide()

    $.ajax({
        headers:{
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url:'/request/setState/'+id,
        type:'post',
        data:{
            'newState':newState
        },
        dataType:'json',
        success:function (r) {
            if (r.ok) {
                toastr('success','Atención',r.message)
                listRequest()
            } else {
                console.log(r.error);
            }
        }
    })
}