var products = [];
var cost = 0;
$(document).ready(function() {
    $("#hInstr").remove()
    listRequest();


    $('#searchRecipe').submit(function (e) {
        e.preventDefault();

        // $(this).attr('action', url+'/recipe/search/'+$('#searchRecipe #search').val());

        var search = {
            search: $('input[name="search"]').val(),
        }


        $.get("/recipe/search/" + search.search, function (response) {

            $('#infRecipe').html("");

            response.data.forEach((element, index) => {
                var myobj = JSON.stringify(element);


                $('#infRecipe').append(`
                <div class="shadow p-3 mb-5 bg-white rounded"><button class="btn btn-primary" style="border-radius:50%;" onclick='addQuantityPerson(${myobj})'><i class="glyphicon-plus"></i></button> ${element.recipe_name}</div>
                <br>
                <img id="img_${element.id}" style="display:none;" src="/uploads/recipes/${element.image}" class="card-img-top" width="70%" height="70%" alt="..."/>
                <br>
                 `);


            });

            if(response.data[0]==null){
                $('#infRecipe').append(`
                
                <div class="alert alert-warning" role="alert">
                    Receta no encontrada!
                </div>

                `);
            }

            console.log(response);
            


        });

        return false;
    });


});

function listRequest() {
    $("#tableRequestBody").html("");
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/request/data",
        type: "GET",
        datatype: "json",
        success: function(response) {
            if (response.ok == false) {
                toastr("info", "Atención", response.message);
            } else {
                response.data.forEach(function(e, i) {
                    if (e.status_id == 1) {
                        var state =
                            '<button type="button" (' +
                            e.id +
                            ')" class="btn btn-info btn-rounded btn-xs">Por aprobar</button>';
                    } else if(e.status_id==2) {
                        var state =
                            '<button type="button" (' +
                            e.id +
                            ')" class="btn btn-success btn-rounded btn-xs">Aprobado</button>';
                    }else if(e.status_id==3){
                        var state =
                        '<button type="button" (' +
                        e.id +
                        ')" class="btn btn-danger btn-rounded btn-xs">Anulado</button>';
                    }
                    $("#tableRequestBody").append(
                        `<tr>
                            <td>${i + 1}</td>
                            <td>${e.date}</td>
                            <td>${e.file}</td>
                            <td>${state}</td>
                            <td>
                                <button onclick="loadInformation(${e.id})"class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap" ><i class="fa fa-eye"></i></span></button>                            
                            </td>
                        </tr>`
                    );
                });

                if (table == false) {
                    applyDataTable();
                }
            }
        }
    });
}
function register()
{
    let description = $("#descripcion").val()
    console.log(description)
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/request',
        type: 'POST',
        data: {
           'file_id': $("#file_id").val(),
           'description' : description,
           'cost' : cost,
           'products':products,
        },
        datatype: 'json',
        success: function (response) {
            if (response.ok == true) {
                listRequest();
                toastr('success', 'Atención', response.message);
                $('#register').modal('toggle');
                cleanInputs();
            } else if (response.ok == false) {
                if (response.message) {
                    listAlerts(response.message);
                } else {
                    toastr('danger', 'Atención', response.error);
                }
            }
        }
    });
}
function search(value) {
    $("#results").html('');
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: '/search/product/' + value,
        type: 'get',
        datatype: 'json',
        success: function (r) {
            if (r.ok == false) {
                $("#results").append(`<li class="list-group-item list-group-item-danger">${r.message}</li>`)
            } else if (r.ok == true) {
                r.data.forEach(function (e, i) {
                    let product = JSON.stringify(e);
                    console.log(product);
                    $("#results").append(`  <li class="list-group-item d-flex justify-content-between align-items-center">
                                                ${e.name}
                                                <span><button class="btn btn-success  btn-xs" onclick='addQuantity(${product})'><i class="glyphicon glyphicon-plus"></i></button></span>
                                            </li>`)
                });
            }
        }
    });
}

function addQuantityPerson(recipe){

    $('#infRecipe').html('');

    var myobj = JSON.stringify(recipe);

    $('#infRecipe').append(`
    <div class="shadow p-3 mb-5 bg-white rounded"><button class="btn btn-primary" style="border-radius:50%;" onclick='addQuantityPerson(${myobj})'><i class="glyphicon-plus"></i></button> ${recipe.recipe_name}</div>
    <br>
    <img id="img_${recipe.id}" style="display:none;" src="/uploads/recipes/${recipe.image}" class="card-img-top" width="70%" height="70%" alt="..."/>
    <br>
     `);



    $('#quantityDivPerson').toggle();
    $('#btnAddRecipe').toggle();
    $(`#img_${recipe.id}`).toggle();

        var myobj = JSON.stringify(recipe);

    $('#btnAddRecipe').attr('onclick', `addRecipe(${myobj})`);

}

function addQuantity(product) {
    let ok = true
    products.forEach(function (e, i) {
        if (product.product_id == e.product_id) {
            toastr('danger', 'Error', 'Este producto ya ha sido agregado')
            ok = false
        }
    })
    if (ok) {
        $.ajax({
            url:'/measureunit/'+product.measure_unit_id,
            type:'get',
            datatype:'json',
            success:function(r){
                $("#meUnit").html(r.data.name)
            }
        })
        $('#results').html('')
        $('#quantityDiv').toggle()
        $('#btnAddProduct').toggle()
        $('#search').prop('readonly', true)
        $('#search').val(`${product.name}`)
        product = JSON.stringify(product);
        $('#btnAddProduct').attr('onclick', `addProduct(${product})`)
    }

}
function addProduct(product) {
    let quantity = $('#quantity').val()
    if (parseInt(quantity) <= 0) {
        toastr('danger', 'Atención', 'La cantidad del producto no pueden ser valores negativos ni cero');
        return false;
    } else if (quantity == '') {
        toastr('danger', 'Atención', 'La cantidad del producto no pueden ir vacía');
        return false;
    } else {
        product.quantity = parseInt(quantity)
        product.subtotal= parseInt(quantity*product.total_with_tax)
        cost = cost + parseInt(product.subtotal)
        products.push(product)

        $('#quantityDiv').toggle()
        $('#btnAddProduct').toggle()
        $('#search').prop('readonly', false)
        $('#search').val('')
        $("#quantity").val('')

        listProducts()
    }
}
function listProducts() {
    $("#bodyProducts").html('')
    cost = 0

    products.forEach(function (e, i) {
        $("#bodyProducts").append(` <tr>
                                        <th>${(i + 1)}</th>
                                        <td>${e.name}</td>
                                        <td>$ ${e.total_with_tax}</td>
                                        <td>${e.quantity}</td>
                                        <td>$ ${new Intl.NumberFormat().format(e.subtotal)}</td>
                                        <td>
                                            <button class="btn btn-icon btn-icon-only btn-info btn-icon-style-4" onclick="remove(${i})"><i class="icon-trash"></i></button>
                                        </td>
                                    </tr>`)
        cost=cost+e.subtotal
    })
    $("#cost").html(new Intl.NumberFormat().format(cost))

}
async function addRecipe(recipe){

    var quantityPerson = $('#quantityPerson').val();
    var productsOut = 0;
    
    $('#excludedProducts').html(``);

        if(quantityPerson<=0){
            toastr('danger', 'Atención', "Ingrese la cantidad de personas, o platillos de la receta.");
        }else{
            
               await $.get("/contracted_products_of_recipe/" + recipe.id, function (response) {
                    console.log(response);

                    if(response.data[0]==null){
                        toastr('warning', 'Atención', "La receta seleccionada no tiene productos asociados.");
                    }

                    response.data.forEach(function (e, i) {
            
                        if(e.product_contract!=null){
            
                            let product = {"name":String(e.product_contract.product.name), "product_id":e.product_contract.id, "total_with_tax":e.product_contract.total_with_tax, "measure_unit_id":e.product_contract.measure_unit_id};
            
                            product.quantity = parseInt(e.quantity*quantityPerson);
                            product.subtotal= parseInt(e.quantity*product.total_with_tax);
            
                            cost = cost + parseInt(product.subtotal);
            
                            products.push(product);
            
                            
                            console.log(product);
                            console.log(response.data);

                            $('#quantityDivPerson').hide();
                            $('#btnAddRecipe').hide();
                            $('#quantityPerson').val('');

                            listProducts();
                        }else{

                            productsOut =  productsOut+1;
                            

                            $('#excludedProducts').append(`
                        
                                 <p style="color:red">-${e.product.name} no esta en el contrato!</p>


                             `);


                        }
            
                    });
            
            
                });


                if(productsOut==0){

                    $('#excludedProducts').prepend(`         
                   
                    `);  

                }else{

                    $('#excludedProducts').prepend(`         
                    <p style="color:red">Atencion! ${productsOut} productos estan fuera de los contratos.</p>
                    <p style="color:red">----------------------------------------------------------------------------------</p>
                    `);

                }

        }

}
function remove(i) {
    products.splice(i,1)
    listProducts()
}







































































