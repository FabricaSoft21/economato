var products = [];
var cost = 0;

$(document).ready(function () {
    listRequest()
});
function listRequest() {
    $("#tableRequestBody").html("");
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/request/data",
        type: "GET",
        datatype: "json",
        success: function (response) {
            if (response.ok == false) {
                toastr("info", "Atención", response.message);
            } else {
                n = 1
                response.data.forEach(function (e, i) {
                    if (e.status_id == 2) {
                        $("#tableRequestBody").append(
                            `<tr>
                                <td>${n}</td>
                                <td>${e.userName}</td>
                                <td>${e.date}</td>
                                <td>${e.file}</td>
                                <td style="text-align:center;"> 
                                    <button type="button" onclick="changeState(${e.id},4)" class="btn btn-success btn-rounded btn-xs">Entregado</button>
                                    <button type="button" onclick="changeState(${e.id},3)" class="btn btn-danger btn-rounded btn-xs">Anular</button>                            
                                </td>
                                <td> 
                                    <button onclick="loadInformation(${e.id})"class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap" ><i class="fa fa-eye"></i></span></button>                            
                                    <button onclick="edit(${e.id})"class="btn btn-outline-success btn-icon btn-icon-circle btn-success btn-icon-style-2"><span class="btn-icon-wrap" ><i class="fa fa-edit"></i></span></button>                            
                                </td>
                            </tr>`)
                        n++
                    }
                });

                if (table == false) {
                    applyDataTable();
                }
            }
        }
    });
}
function update(id) {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: `/request/${id}`,
        type: 'PUT',
        data: {
            'cost': cost,
            'products': products,
        },
        datatype: 'json',
        success: function (response) {
            if (response.ok == true) {
                listRequest();
                toastr('success', 'Atención', response.message);
                $('#showInformation').modal('toggle');
                cleanInputs();
            } else if (response.ok == false) {
                toastr('danger', 'Atención', response.error);
            }
        }
    });
}
function edit(id) {
    $.get("/requestInformation/" + id, function (r) {
        console.log(r);
        $("#showFile").text(r.data['file'].number);
        $("#nApprentices").text(r.data['file'].apprentices);
        $("#characterization").text(r.data['file'].characterization.name);

        $("#responsible").text(r.data.request.user.name);
        $("#identity").text(r.data.request.user.identification);

        if (r.data['file'].characterization.status == 0) {
            $("#status").text("En ejecucion.");
        } else {
            $("#status").text("Finalizada.");
        }
        $("#ShowDescription").val(r.data['request'].description);
        products=[]
        r.data.orderXProducts.forEach(function (e, i) {

            let product = {
                'measure_unit_id': e.product_xcontract.product.measure_unit_id,
                'name': e.product_xcontract.product.name,
                'product_id': e.product_id,
                'quantity': e.quantity,
                'subtotal': e.subtotal,
                'total_with_tax': e.product_xcontract.total_with_tax,
            }

            products.push(product)
            cost = cost + e.subtotal

        })
        listProducts()
        $("#showCost").html(new Intl.NumberFormat().format(cost))

        $("#div-edit").show()
        $("#buttonEdit").show()
        $("#buttonEdit").attr('onclick', `update(${id})`)

        $("#showInformation").modal('toggle');

    });
}
function addQuantity(product) {
    let ok = true
    products.forEach(function (e, i) {
        if (product.product_id == e.product_id) {
            toastr('danger', 'Error', 'Este producto ya ha sido agregado')
            ok = false
        }
    })
    if (ok) {
        $.ajax({
            url: '/measureunit/' + product.measure_unit_id,
            type: 'get',
            datatype: 'json',
            success: function (r) {
                $("#meUnit").html(r.data.name)
            }
        })
        $('#resultsEdit').html('')
        $('#quantityDivEdit').toggle()
        $('#btnAddProductEdit').toggle()
        $('#searchEdit').prop('readonly', true)
        $('#searchEdit').val(`${product.name}`)
        product = JSON.stringify(product);
        $('#btnAddProductEdit').attr('onclick', `addProduct(${product})`)
    }

}
function addProduct(product) {
    let quantity = $('#quantityEdit').val()
    if (parseInt(quantity) <= 0) {
        toastr('danger', 'Atención', 'La cantidad del producto no pueden ser valores negativos ni cero');
        return false;
    } else if (quantity == '') {
        toastr('danger', 'Atención', 'La cantidad del producto no pueden ir vacía');
        return false;
    } else {
        product.quantity = parseInt(quantity)
        product.subtotal = parseInt(quantity * product.total_with_tax)
        cost = cost + parseInt(product.subtotal)
        products.push(product)

        $('#quantityDivEdit').toggle()
        $('#btnAddProductEdit').toggle()
        $('#searchEdit').prop('readonly', false)
        $('#searchEdit').val('')
        $("#quantityEdit").val('')

        listProducts()
    }
}
function listProducts() {
    $("#listProductsShow").html('')
    cost = 0

    products.forEach(function (e, i) {
        $("#listProductsShow").append(` <tr>
                                        <th>${(i + 1)}</th>
                                        <td>${e.name}</td>
                                        <td>$ ${e.total_with_tax}</td>
                                        <td>${e.quantity}</td>
                                        <td>$ ${new Intl.NumberFormat().format(e.subtotal)}</td>
                                        <td>
                                            <button class="btn btn-icon btn-icon-only btn-info btn-icon-style-4" onclick="remove(${i})"><i class="icon-trash"></i></button>
                                        </td>
                                    </tr>`)
        cost = cost + e.subtotal
    })
    $("#showCost").html(new Intl.NumberFormat().format(cost))

}
function search(value) {
    $("#resultsEdit").html('');
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: '/search/product/' + value,
        type: 'get',
        datatype: 'json',
        success: function (r) {
            if (r.ok == false) {
                $("#resultsEdit").append(`<li class="list-group-item list-group-item-danger">${r.message}</li>`)
            } else if (r.ok == true) {
                r.data.forEach(function (e, i) {
                    let product = JSON.stringify(e);
                    console.log(product);
                    $("#resultsEdit").append(`  <li class="list-group-item d-flex justify-content-between align-items-center">
                                                ${e.name}
                                                <span><button class="btn btn-success  btn-xs" onclick='addQuantity(${product})'><i class="glyphicon glyphicon-plus"></i></button></span>
                                            </li>`)
                });
            }
        }
    });
}
function remove(i) {
    products.splice(i, 1)
    listProducts()
}
function changeState(id,newState) {
    $.ajax({
        headers:{
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url:'/request/setState/'+id,
        type:'post',
        data:{
            'newState':newState
        },
        dataType:'json',
        success:function (r) {
            if (r.ok) {
                toastr('success','Atención',r.message)
                listRequest()
            } else {
                console.log(r.error);
            }
        }
    })
}