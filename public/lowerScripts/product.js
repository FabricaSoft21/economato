var table = false;

$(document).ready(function() {
    listProducts();
});

function applyDataTable() {
    $("#productTable").DataTable({
        responsive: true,
        autoWidth: false,
        language: {
            search: "",
            searchPlaceholder: "Buscar",
            sLengthMenu: "_MENU_items"
        }
    });

    table = true;
}

function listProducts() {
    $("#tableProductsBody").html("");
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/product/data",
        type: "GET",
        datatype: "json",
        success: function(response) {
            console.log(response.data);
            if (response.ok == false) {
                toast("info", "Atención", response.message);
            } else {
                response.data.forEach(function(e, i) {
                    if (e.status == 0) {
                        var state =
                            '<button type="button" onclick="change_state(' +
                            e.id +
                            ')" class="btn btn-success btn-rounded btn-xs">Activo</button>';
                    } else {
                        var state =
                            '<button type="button" onclick="change_state(' +
                            e.id +
                            ')" class="btn btn-danger btn-rounded btn-xs">Inactivo</button>';
                    }
                    $("#tableProductsBody").append(
                        "<tr>" +
                            "<th>" +(i + 1) +"</th>" +
                            "<td>" +e.name +"</td>" +
                            "<td>" +e.type_product_name +"</td>" +
                            "<td>" +e.presentation_name +"</td>"+
                            "<td>" +  e.measure_name +"</td>" +
                            "<td>" +  e.tax+ "%" +"</td>" +
                            "<td>"+state +"</td>" +
                            "<td>" +
                            '<button data-toggle="modal" data-target="#update" onclick="edit_product(' +
                            e.id +
                            ')" class="btn btn-outline-success btn-icon btn-icon-circle btn-success btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-pencil"></i></span></button>' +
                            '<button data-toggle="modal" data-target="#show" onclick="show(' +
                            e.id +
                            ')" class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-eye"></i></span></button>' +
                            "</td>" +
                            "</tr>"
                    );
                });

                if (table == false) {
                    applyDataTable();
                }
            }
        }
    });
}
function change_state(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/product/delete",
        type: "GET",
        datatype: "json",
        data: {
            id: id
        },
        success: function(response) {
            if (response.ok == true) {
                listProducts();
                toastr("success", "Atención", response.message);
                cleanInputs();
                removeClass();
            }
        }
    });
}

function show(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/product/see",
        type: "GET",
        datatype: "json",
        data: {
            id: id
        },
        success: function(data) {
            console.log(data);
            $("#name_show").val(data.name);
            $("#type_product_id_show").val(data.type_product_id);
            $("#presentation_id_show").val(data.presentation_id);
            $("#measure_unit_id_show").val(data.measure_unit_id);
            $("#tax_id_show").val(data.tax_id)
        }
    });
}

function edit_product(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: "/product/see",
        type: "GET",
        datatype: "json",
        data: {
            id: id
        },
        success: function(data) {
            console.log(data);
            $("#id_upd").val(data.id);
            $("#name_upd").val(data.name);
            $("#type_product_id_upd").val(data.type_product_id);
            $("#presentation_id_upd").val(data.presentation_id);
            $("#measure_unit_id_upd").val(data.measure_unit_id);
            $("#tax_id_upd").val(data.tax_id);
        }
    });
}
// Registrar productos
function register() {
   
    $.ajax({
        url: "/product",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "POST",
        dataType: "json",
        data: {
            name: $("#name").val(),
            type_product_id: $("#type_product_id").val(),
            presentation_id: $("#presentation_id").val(),
            measure_unit_id: $("#measure_unit_id").val(),
            tax_id : $("#tax_id").val(),
        },
        success: function(response) {
            if(response.ok==true){
				listProducts();
				toastr('success','Atención',response.message);
				$('#register').modal('toggle');
				cleanInputs();
				removeClass();
            }else if(response.ok==false){
                if (response.message) {
                    listAlerts(response.message);
                }else{
					toastr('danger','Atención',response.error);
				}
            }
        }
    });
}

function cleanInputs(){
	$("#name").val('');
	$("#name").removeClass('is-invalid');
	$("#type_product_id").val('');
	$("#type_product_id").removeClass('is-invalid');
	$("#presentation_id").val('');
	$("#presentation_id").removeClass('is-invalid');
	$("#measure_unit_id").val('');
    $("#measure_unit_id").removeClass('is-invalid');
    $("#tax_id").val('');
	$("#tax_id").removeClass('is-invalid');
}


// Editar producto
function update() {

    $.ajax({
        url: "/product/update",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "POST",
        dataType: "json",
        data: {
            id:  $("#id_upd").val(),
            name: $("#name_upd").val(),
            type_product_id: $("#type_product_id_upd").val(),
            presentation_id: $("#presentation_id_upd").val(),
            measure_unit_id: $("#measure_unit_id_upd").val(),
            tax_id : $("#tax_id_upd").val(),
        },
        success: function(response) {
            if(response.ok==true){
				listProducts();
				toastr('success','Atención',response.message);
				$('#update').modal('toggle');
				cleanInputs();
				removeClass();
            }else if(response.ok==false){
                if (response.message) {
                    listAlerts(response.message);
                }else{
					toastr('danger','Atención',response.error);
				}
            }
        }
    });
}

function listAlerts(e) {
    if (e.type_product_id) {
        toastr("danger", "Error", e.type_product_id[0]);
        $("#type_product_id").addClass("is-invalid");
    }
    if (e.name) {
        toastr("danger", "Error", e.name[0]);
        $("#name").addClass("is-invalid");
    }
    if (e.measure_unit_id) {
        toastr("danger", "Error", e.measure_unit_id[0]);
        $("#measure_unit_id").addClass("is-invalid");
    }
    if (e.presentation_id) {
        toastr("danger", "Error", e.presentation_id[0]);
        $("#presentation_id").addClass("is-invalid");
    }
    if(e.tax_id){
        toastr("danger", "Error", e.tax_id[0]);
        $("#tax_id").addClass("is-invalid");
    }
    
}
function toastr(type, tittle, text) {
    $.toast({
        heading: "" + tittle + "",
        text: "" + text + "",
        position: "top-right",
        loaderBg: "#000",
        class: "jq-has-icon jq-toast-" + type,
        hideAfter: 3500,
        stack: 6,
        showHideTransition: "fade"
    });
}
