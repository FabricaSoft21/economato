var table = false;

$(document).ready(function() {
	listProviders();
});

function applyDataTable() {
    $('#tableProviders').DataTable({
		responsive: true,
		autoWidth: false,
		language: { search: "",
		searchPlaceholder: "Buscar",
		sLengthMenu: "_MENU_items"
		
		}
    });	

	table = true;
}
function listProviders() {

    $("#tableProvidersBody").html('');
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/provider/data',
        type: 'GET',
        datatype: 'json',
        success: function (response) {
            if (response.ok == false) {
                toastr('info','Atención',response.message);
            } else {
                
                response.data.forEach(function (e, i) {
					if(e.status==0){
						var state = '<button type="button" class="btn btn-success btn-rounded btn-xs">Activo</button>';
					}else{
						var state = '<button type="button" class="btn btn-danger btn-rounded btn-xs">Inactivo</button>';
					}
                    $("#tableProvidersBody").append('<tr>'+
													'<th>'+(i+1)+'</th>'+
													'<td>'+e.NIT+'</td>'+
													'<td>'+e.name+'</td>'+
													'<td>'+e.phone+'</td>'+
													'<td>'+e.email+'</td>'+
													'<td>'+e.contact_name+' '+e.last_name+'</td>'+
													'<td>'+e.phone_contact+'</td>'+
													'<td>'+
														state+
													'</td>'+
													'<td>'+
														'<button onclick="edit('+e.id+')" class="btn btn-outline-success btn-icon btn-icon-circle btn-success btn-icon-style-2"><span class="btn-icon-wrap" ><i class="fa fa-pencil"></i></span></button>'+
														'<button onclick="show('+e.id+')"class="btn btn-outline-info btn-icon btn-icon-circle btn-info btn-icon-style-2"><span class="btn-icon-wrap" ><i class="fa fa-eye"></i></span></button>'+
													'</td>'+
												'</tr>');
				});
				
                if (table == false) {
                    applyDataTable();
                }

            }

        }



    });

}
function validateData(){
	let Nit = $("#Nit").val();
	let name = $("#name").val();
	let phone = $("#phone").val();
	let email = $("#email").val();
	let concatName = $("#concatName").val();
	let contactLastName = $("#contactLastName").val();
	let phoneContact = $("#phoneContact").val();

	$.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/provider',
        type: 'POST',
        data:{
			'NIT':Nit,
			'name':name,
			'phone':phone,
			'email':email,
			'contact_name':concatName,
			'last_name':contactLastName,
			'phone_contact':phoneContact
        },
        datatype: 'json',
        success: function (response) {
            if(response.ok==true){
				listProviders();
				toastr('success','Atención',response.message);
				$('#register').modal('toggle');
				cleanInputs();
            }else if(response.ok==false){
                if (response.message) {
                    listAlerts(response.message);
                }else{
					toastr('danger','Atención',response.error);
				}
            }
        }
    });
}
function show(id){
	$.ajax({
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url:'/provider/'+id,
		type:'GET',
		datatype:'json',
		success: function (r) {
			if(r.ok==true){
				$("#NitShow").val(r.data.NIT);
				$("#NitShow").prop('readonly',true);
				$("#nameShow").val(r.data.name);
				$("#nameShow").prop('readonly',true);
				$("#phoneShow").val(r.data.phone);
				$("#phoneShow").prop('readonly',true);
				$("#emailShow").val(r.data.email);
				$("#emailShow").prop('readonly',true);
				$("#concatNameShow").val(r.data.contact_name);
				$("#concatNameShow").prop('readonly',true);
				$("#contactLastNameShow").val(r.data.last_name);
				$("#contactLastNameShow").prop('readonly',true);
				$("#phoneContactShow").val(r.data.phone_contact);
				$("#phoneContactShow").prop('readonly',true);

				$("#buttonEdit").hide();
				$("#show").modal('toggle');


			}else if(r.ok==false){
				toastr('danger','Error',r.message)
			}
		}
	});
}
function edit(id){
	$.ajax({
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url:'/provider/'+id,
		type:'GET',
		datatype:'json',
		success: function (r) {
			if(r.ok==true){
				$("#NitShow").val(r.data.NIT);
				$("#NitShow").prop('readonly',false);
				$("#nameShow").val(r.data.name);
				$("#nameShow").prop('readonly',false);
				$("#phoneShow").val(r.data.phone);
				$("#phoneShow").prop('readonly',false);
				$("#emailShow").val(r.data.email);
				$("#emailShow").prop('readonly',false);
				$("#concatNameShow").val(r.data.contact_name);
				$("#concatNameShow").prop('readonly',false);
				$("#contactLastNameShow").val(r.data.last_name);
				$("#contactLastNameShow").prop('readonly',false);
				$("#phoneContactShow").val(r.data.phone_contact);
				$("#phoneContactShow").prop('readonly',false);
				$("#buttonEdit").show();
				$("#show").modal('toggle');

			}else if(r.ok==false){
				toastr('danger','Error',r.message)
			}
		}
	});
}
function cleanInputs(){
	$("#Nit").val('');
	$("#Nit").removeClass('is-invalid');
	$("#name").val('');
	$("#name").removeClass('is-invalid');
	$("#phone").val('');
	$("#phone").removeClass('is-invalid');
	$("#email").val('');
	$("#email").removeClass('is-invalid');
	$("#concatName").val('');
	$("#concatName").removeClass('is-invalid');
	$("#contactLastName").val('');
	$("#contactLastName").removeClass('is-invalid');
	$("#phoneContact").val('');
	$("#phoneContact").removeClass('is-invalid');
}
function listAlerts(e) {
    if (e.NIT) {
		toastr('danger', 'Error',e.NIT[0]);
		$("#Nit").addClass('is-invalid');
	}
	if (e.name) {
		toastr('danger', 'Error',e.name[0]);
		$("#name").addClass('is-invalid');
	}
	if (e.phone) {
		toastr('danger', 'Error',e.phone[0]);
		$("#phone").addClass('is-invalid');
	}
	if (e.email) {
		toastr('danger', 'Error',e.email[0]);
		$("#email").addClass('is-invalid');
	}
	if (e.contact_name) {
		toastr('danger', 'Error',e.contact_name[0]);
		$("#concatName").addClass('is-invalid');
	}
	if (e.last_name) {
		toastr('danger', 'Error',e.last_name[0]);
		$("#contactLastName").addClass('is-invalid');
	}
	if (e.phone_contact) {
		toastr('danger', 'Error',e.phone_contact[0]);
		$("#phoneContact").addClass('is-invalid');
    }

}
function toastr(type,tittle,text){
	$.toast({
		heading: ''+tittle+'',
		text: ''+text+'',
		position: 'top-right',
		loaderBg:'#000',
		class: 'jq-has-icon jq-toast-'+type,
		hideAfter: 3500, 
		stack: 6,
		showHideTransition: 'fade'
	});
}